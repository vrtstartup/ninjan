<?php



// Stuff you probably want to edit, maybe.
$modules_directory = "modules/"; // Don't change this unless you are sure.
$require_login = true; // turn logins on or off. Sometimes you want an open system.


$default_module = "texts"; // The default module that a user should come when logging in.
                           // This can be overridden at user level in logins.conf.php 
$default_action = "index"; // The default action that a user should do when logging in.
                           // This can be overridden at user level in the logins.conf.php



// All logins that have this level or higher can use the modules.
// that are not defined/overriden below.
$default_module_level = 0;

$skin = "default";

// Blank array for holder module levels.
$module_levels = array(); // used if require login.

// Module Levels
$module_levels['texts'] = 10; // client and higher.
$module_levels['uploads'] = 0; // admin and higher.



$noshows = array();
$noshows[] = "tools";
$noshows[] = "uploads";



///////////////// END PROBABLE EDITING /////////////////////////




include_once '../inc/db.php';
include_once '../inc/languages.php';

$modules = array();

$files = glob("modules/*");
foreach ($files as $file) {
  if ( is_dir( $file ) ) {
    $module = basename($file);
    $modules[] = $module;
    if( ! isset( $module_levels[$module] ) ) {
      $module_levels[$module] = $default_module_level;
    }
  }
}

if( $require_login ) {
  
  // gonna need that session
  session_start();

  if( ! isset( $_SESSION['admin_user'] ) && basename( $_SERVER['PHP_SELF'] ) != "login.php" ) {
    header( "Location: login.php" );
    exit( 0 );
  }
}


// Set the module safely.
if ( isset( $_GET['module'] ) ) {
  if ( ! is_dir( $modules_directory . $_GET['module'] ) ) {
      if( isset( $_SESSION['admin_user_default_module'] ) ) {
        $_GET['module'] = $_SESSION['admin_user_default_module'];
      } else {
        $_GET['module'] = $default_module;
      }
  }
} else {
  if( isset( $_SESSION['admin_user_default_module'] ) ) {
    $_GET['module'] = $_SESSION['admin_user_default_module'];
  } else {
    $_GET['module'] = $default_module;
  }
}

$MODULE = $_GET['module'];
$table = $MODULE;


// Now we need to check if this user can use this module.

if( isset( $_SESSION['admin_user_level'] ) && $_SESSION['admin_user_level'] > $module_levels[$MODULE] ) {
  // No they may not.

  // This should never ever happen.
  // If it does we send them to the logout.
  header( "Location: logout.php" );
  exit( 0 );
}

// Normally the default action should be index.
if ( isset( $_GET['action'] ) ) {
  if ( ! is_file( $modules_directory . $MODULE . "/" . $_GET['action'] . ".php" ) ) {
      $_GET['action'] = $default_action;
  }
} else {
  $_GET['action'] = $default_action;
}

$ACTION = $_GET['action'];


// Get helper functions.
$files = glob("../inc/functions/function.*");
foreach ($files as $file) {
  include_once($file);
}
