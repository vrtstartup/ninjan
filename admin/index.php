<?php

date_default_timezone_set( "Europe/Brussels" );
define( "ADMIN" , TRUE );

include_once 'inc.php';

if ( is_file( $modules_directory . $MODULE . "/global.php" ) ) {
    include_once( $modules_directory . $MODULE . "/global.php" );
}

if ( is_dir( $modules_directory . $MODULE . "/functions" ) ) {
    $files = glob( $modules_directory . $MODULE . "/functions/function.*.php" );
    foreach ($files as $file) {
      include_once($file);
    }

}

if ( is_file( $modules_directory . $MODULE . "/pre-" . $ACTION . ".php" ) ) {
    include_once( $modules_directory .  $MODULE . "/pre-" . $ACTION . ".php" );
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  
  <title>ADMIN - <?php echo $_SERVER['SERVER_NAME']; ?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">


  <script src="assets/js/jquery.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>

  
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/sticky-footer.css" rel="stylesheet">
  
  
  <?php if( file_exists( "skins/" . $skin . "/custom.css" ) ) { ?>
    <link href="skins/<?php echo $skin; ?>/custom.css" rel="stylesheet">
  <?php } ?>
  
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.js"></script>
    <script src="assets/js/respond.min.js"></script>
  <![endif]-->

  

  <?php if( file_exists( "skins/" . $skin . "/favicon.ico" ) ) { ?>
    <link href="skins/<?php echo $skin; ?>/favicon.ico" rel="shortcut icon">
  <?php } else { ?>
    <link href="skins/default/favicon.ico" rel="shortcut icon">
  <?php } ?>

  <?php
  if ( is_file( $modules_directory . $MODULE . "/head.php" ) ) {
      include_once( $modules_directory . $MODULE . "/head.php" );
  }
  ?>

  <style>
      body {
        padding-top: 50px;
      }    
  </style>

</head>
<body>
  <div id="wrap">
    
    <?php ( file_exists( "skins/" . $skin . "/header.php" ) ) ? include( "skins/" . $skin . "/header.php" ) : include( "skins/default/header.php" ); ?>

    <div class="container main">
      <h1><?php echo ucwords( $MODULE ); ?></h1>
      <?php if ( is_file( $modules_directory . $MODULE . "/" . $ACTION . ".php" ) ) { ?>
        <?php include_once( $modules_directory . $MODULE . "/" . $ACTION . ".php" ); ?>
      <?php } ?>
    </div>
  </div>

  

  <?php ( file_exists( "skins/" . $skin . "/footer.php" ) ) ? include( "skins/" . $skin . "/footer.php" ) : include( "skins/default/footer.php" ); ?>

  </body>
</html>
