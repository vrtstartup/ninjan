<?php
  include_once 'inc.php';


  if( count( $_POST ) ) {
    if( isset( $_POST['username'] ) && isset( $_POST['password'] ) ) {
      include_once( "logins.conf.php" );
      if( isset( $logins[$_POST['username']] ) ) {
        if( $logins[$_POST['username']]['password'] == $_POST['password'] ) {
          $_SESSION['admin_user'] = $_POST['username'];
          $_SESSION['admin_user_level'] = $logins[$_POST['username']]['level'];

          $query = "";
          if( isset( $logins[$_POST['username']]['default_module'] ) ) {
            $_SESSION['admin_user_default_module'] = $logins[$_POST['username']]['default_module'];
          }

          header( "Location: ./"  );
          exit( 0 );
        }
      }
    }
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>ADMIN LOGIN</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <script src="assets/js/jquery.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>

  

  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/sticky-footer.css" rel="stylesheet">
  
  <?php if( file_exists( "skins/" . $skin . "/custom.css" ) ) { ?>
    <link href="skins/<?php echo $skin; ?>/custom.css" rel="stylesheet">
  <?php } ?>
  
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.js"></script>
    <script src="assets/js/respond.min.js"></script>
  <![endif]-->

  <?php if( file_exists( "skins/" . $skin . "/favicon.ico" ) ) { ?>
    <link href="skins/<?php echo $skin; ?>/favicon.ico" rel="shortcut icon">
  <?php } else { ?>
    <link href="skins/default/favicon.ico" rel="shortcut icon">
  <?php } ?>
  
  <style>
      body {
        padding-top: 50px;
      }    
  </style>

</head>
<body>

  <div id="wrap">

    <?php ( file_exists( "skins/" . $skin . "/header.php" ) ) ? include( "skins/" . $skin . "/header.php" ) : include( "skins/default/header.php" ); ?>

     
    <div class="container main" id="login">
      <h3>Login to gain access to the admin.</h3>


      <form action="" method="POST" role="form">
        
      
        <div class="form-group">
          <label class="control-label" for="username">User Name</label>
          <input type="text" class="form-control" id="username" name="username" value="<?php echo pvar( "username" ); ?>">
        </div>


        <div class="form-group">
          <label class="control-label" for="password">Password</label>
          <input type="password" class="form-control" id="password" name="password" value="<?php echo pvar( "password" ); ?>">
        </div>
      
        
      
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>

    </div>

  </div>

  <?php ( file_exists( "skins/" . $skin . "/footer.php" ) ) ? include( "skins/" . $skin . "/footer.php" ) : include( "skins/default/footer.php" ); ?>

    

  </body>
</html>
