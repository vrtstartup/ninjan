<?php

// In this file define your logins.
//
//  

// Admin User
$logins['admin']['password'] = 'admin';
$logins['admin']['level'] = 0;


// Client User
$logins['client']['password'] = 'client';
$logins['client']['level'] = 10;
$logins['client']['default_module'] = 'texts'; // Give a default module if needed.


// To prevent some users from using some modules:
/*
  First set a weight for the user like '10'.
  Then look in: inc.php.

  Set the weight of modules that you don't want the user to use to
  something lower than 10. Any module that is 10 or more will be
  availeable to the user.
*/