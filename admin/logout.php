<?php

  session_start();

  unset( $_SESSION['admin_user'] );
  unset( $_SESSION['admin_user_level'] );
  unset( $_SESSION['admin_user_default_module'] );

  header( "Location: login.php" );
  exit( 0 );