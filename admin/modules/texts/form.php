<script src="assets/js/ace/src-min-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
<script>
var editors = [];
</script>


<form method="post" role="form">

<input type="hidden" name="text_key" value="<?php echo $_POST['text_key']; ?>" />


<ul class="nav nav-tabs">
    <?php foreach ($languages as $k => $language) { ?>
    <?php
        $active = ( $_GET['l'] == $k ) ? "active" : "";
        $error = ( errors( 'text_'.$k ) ) ? "*" : "";
    ?> 
        <li class="<?php echo $active; ?>"><a href="#<?php echo $k; ?>" data-toggle="tab"><?php echo $language['name']; ?><?php echo $error; ?></a></li>
    <?php } ?>
</ul>

<div class="tab-content">

<?php foreach ($languages as $k => $language) { ?>

<?php
    $active = ( $_GET['l'] == $k ) ? "active" : "";
?> 

<div class="tab-pane <?php echo $active; ?>" id="<?php echo $k; ?>">

    <div class="form-group">
      
      <div style="position: relative; height: 650px;;" id="text_<?php echo $k; ?>"><?php echo htmlspecialchars(pvar( 'text_'.$k )); ?></div>
      
    
    </div>

    <input type="hidden" name="text_<?php echo $k; ?>" value="" />

    <div class="form-group">
    <button type="submit" class="btn btn-primary">Save</button>
    </div>

    <script>
        editors['<?php echo $k; ?>'] = ace.edit("text_<?php echo $k; ?>");
        editors['<?php echo $k; ?>'].setTheme("ace/theme/monokai");
        editors['<?php echo $k; ?>'].getSession().setMode("ace/mode/html");
        editors['<?php echo $k; ?>'].getSession().setTabSize(2);
        editors['<?php echo $k; ?>'].getSession().setWrapLimitRange( 120 , 120 );
        editors['<?php echo $k; ?>'].getSession().setUseSoftTabs(true);
        editors['<?php echo $k; ?>'].getSession().setUseWrapMode(true);
    </script>

</div>

<?php } ?>


</div>
</form>

<script>
    $(document).ready(function(){
        $('form').submit(function(e){
            //e.preventDefault();

            for( k in editors ) {
                //console.log( k , editors[k].getValue() );

                $( "input[name='text_"+k+"']" ).val( editors[k].getValue() );

            }
            return true;

        });
    });
</script>



