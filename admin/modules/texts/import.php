<h4>Import</h4>

<p>
  Import a texts.serial file that you imported from some where else.
</p>

<p>
  This will take the texts from an export and bring them into this site.
  Ideally you would export from production and import into staging.
</p>

<h4>!!! CAUTION !!!</h4>

<p>
  The site that you are importing from and the site you are importing to
  should have the same languages.php configuration. If this is not the case
  Then you will probably get some database errors if the site you are importing
  from has more languages or different languages configured.
</p>

<p>
  Maybe some day when it is really needed and there is time we can program up
  better erorr checking on the import export. For now this should make you really
  happy compared to other systems.
</p>

<form method="post" enctype="multipart/form-data">

  <h4>Replace Translations</h4>

  <label class="radio">
  <input type="radio" name="override" id="onlynewer" value="onlynewer" checked /> Only Newer Updated
  </label>

  <label class="radio">
  <input type="radio" name="override" id="all" value="all" /> All ( HULK SMASH!)
  </label>

  <h4>texts.serial File</h4>

  <p>
    <input type="file" name="import_file" enctype="multipart/form-data">
  </p>

  <p>
    <button type="submit" onClick="return confirm('Are 105% certain this is what you want?')" class="btn btn-primary">Import</button>
    <a href="?module=texts&action=index&l=<?php gvar('l') ?>" class="btn btn-primary">Return</a>
  </p>

</form>
