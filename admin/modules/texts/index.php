<?php


require_once '../inc/languages.php';

$table = "texts";

$l = $default_language;
if ( isset( $_GET['l'] ) && isset( $languages[$_GET['l']] ) ) {
  $l = $_GET['l'];
}

$q = "SELECT text_id,text_key, text_".$l." FROM ".$table." ORDER BY text_id DESC";

if ( isset( $_GET['search'] ) && strlen($_GET['search']) ) {
  $search = dbEscapeString( "%" . $_GET['search'] . "%" );
  $q = "SELECT text_id,text_key, text_".$l." FROM ".$table." WHERE text_key LIKE ".$search." OR text_".$l." LIKE ".$search." ORDER BY text_id DESC";
}

$qr = dbQuery( $q );

$llinks = array();
foreach ($languages as $k => $language) {
  $link = '<a href="?module=texts&l='.$k.'">'.$k.'</a>';
  if ($l == $k) {
    $link = "<b>" . $k . "</b>";
  }
  $llinks[] = $link;
}
?>

<p class="lang-select">
  Select a language to view:
  <?php echo join( " | " , $llinks ) ?>
</p>

<?php

$tlinks = array();
$tlinks[] = "<a class='btn btn-info' href='?module=texts&action=export'>Export</a>";
$tlinks[] = "<a class='btn btn-info' href='?module=texts&action=import&l=".$l."'>Import</a>";

if ( ! isset( $_GET['search'] ) ) {
  $_GET['search'] = ""; // Stupid System Admins that turn on Notices...
}

?>

<div class="pull-right">
  <?php echo join(" " , $tlinks) ?>
</div>


<form method="get" role="form" class="form-search">
  
  <input type="hidden" name="module" value="texts" />
  <input type="hidden" name="l" value="<?php echo $l; ?>" />

  <div class="form-group">
    <div class="input-append">
      <input type="text" id="search" name="search" value="<?php echo $_GET['search']; ?>" class="span2 search-query" />
      <span class="icon-search" id="input-icon-search"></span>
      <button type="submit" value="search" class="btn btn-primary">Search</button>
    </div>
  </div>
</form>


<br />


<table class="table table-striped table-bordered">

<thead>
<tr>
<th>Text Key</th>
<th>Text <?php echo $languages[$l]['name']; ?></th>
<th>Delete</th>
</tr>
</thead>

<tbody>
<?php while ( $qrow = dbFetch($qr) ) { ?>
<tr>
<td><a href="?module=texts&action=update&text_id=<?php echo $qrow['text_id']; ?>&l=<?php echo $l; ?>"><?php echo $qrow['text_key']; ?></a></td>
<td><?php echo substr( strip_tags( $qrow['text_'.$l] ) , 0 , 80 ); ?><?php echo ( strlen( strip_tags( $qrow['text_'.$l] ) ) > 80 ) ? "..." : "" ?></td>
<td><a href="?module=texts&action=delete&text_id=<?php echo $qrow['text_id']; ?>&l=<?php echo $l; ?>" onClick="return confirm( 'Are you 100% sure that you want to delete the text: <?php echo $qrow['text_key']; ?>' );">delete</a></td>
</tr>
<?php } ?>
</tbody>

</table>
