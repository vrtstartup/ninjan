<?php

$table = "texts";
$q = "SELECT * FROM ".$table;
$qr = dbQuery($q);
$all = dbFetchAll($qr);

header( "Content-Type: application/force-download" );
header('Content-Disposition: attachment; filename="texts.serial"');
header('Content-Transfer-Encoding: binary');
header('Pragma: public');

echo serialize($all);
exit( 0 );
