<?php

  error_reporting(E_ALL);

  $o = "onlynewer";

  if ( count( $_POST ) ) {

    $o = $_POST['override'];

    if ( is_file( $_FILES['import_file']['tmp_name'] ) ) {
      $raw = file_get_contents( $_FILES['import_file']['tmp_name'] );
      $texts = unserialize($raw);

      //die( print_r($texts,true ) );

      if ( count( $texts ) ) {
        foreach ($texts as $text) {
          $key = dbEscapeString( $text['text_key'] );
          $q = "SELECT * FROM texts WHERE text_key =".$key."";
          $qr = dbQuery($q);
          $old = dbFetch($qr);

          if (! $old) {
            $q = dbInsertFromVals("texts", $text);
            $qr = dbQuery($q);
          }

          if ($old && $o == "all") {
            $q = dbUpdateFromVals("texts", "text_key", $key, $text);
            $qr = dbQuery($q);
          }

          if ($old && $o == "onlynewer" && $old['text_updated'] < $text['text_updated']) {
            $q = dbUpdateFromVals("texts", "text_key", $key, $text);
            $qr = dbQuery($q);
          }

        }
      }
    }

    header( "Location: ?module=texts&l=".$_GET['l'] );

  }
