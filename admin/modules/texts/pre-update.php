<?php

require_once '../inc/languages.php';

$errors = array();

if ( count( $_POST ) ) {
    include_once $modules_directory . './texts/regs.php';

    foreach ($regs as $k => $v) {
        if ( ! preg_match( $v , $_POST[$k] ) ) {
            $errors[$k] = "* invalid";
        }
    }

    if ( count( $errors ) == 0 ) {
        $_POST['text_updated'] = time();
        $q = dbUpdateFromVals("texts" , "text_id" , intval( $_GET['text_id'] ) );
        $qr = dbQuery($q);

        $g['module'] = "texts";
        $g['l'] = $_GET['l'];

        header( "Location: ?".http_build_query($g) );
        exit( 0 );
    }
} else {

    
    if( isset( $_GET['text_key'] ) ) {
        
        $text_id = dbFetchOneValue( "texts" , "text_id", "text_key" , $_GET['text_key'] );
        $_GET['text_id'] = $text_id;
        unset( $_GET['text_key'] );
        
        header( "Location: ?". http_build_query( $_GET ) );
        exit( 0 );
    } 

    $_POST = dbFetchOneRow( "texts" , "text_id" , $_GET['text_id'] );
    
}
