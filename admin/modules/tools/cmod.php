
<h3>
  Create a Module
</h3>



<form role="form" method="post">

  <p>
    Modules are tables in the database... Modules are entities in your project.
    If clients wants a site with "articles" in it. YOU, my good sir needs to have
    an articles tables in your database.
  </p>

  <p>
    Fill in the form below and we will generate a ZIP file with the code you
    need to make a module. Once you have the ".zip" file you then need to
    upload it to your admin directory and BAM you should now have a working
    module that you can start on.
  </p>

  <p class="text-warning">
    The directory that you upload may have some odd permissions. Be sure to
    check those if the module is not working.
  </p>

  <p>
    Entities / Modules are the plural form of what you are working with. So
    your module is not "auto" but instead "autos".
  </p>

  <div class="form-group">
    <label class="control-label" for="module_name">Module Name</label>
    <input class="form-control" type="text" name="module_name" id="module_name" />
  </div>

  <p>
    Now because we I am too lazy to make a super duper singular maker function
    I kindly ask you to please put in the singular name of the entity. Ex,
    You're making a "cats" module thus "cat".
  </p>

  <div class="form-group">
    <label class="control-label" for="singular_name">Singular Name</label>
    <input class="form-control" type="text" name="singular_name" id="singular_name" />
  </div>

  <button type="submit" class="btn btn-primary">Create Zip File</button>



</form>




<script>


jQuery(document).ready(function($) {


  $("input[name='module_name']").keyup(function(e){
    $(this).val( $(this).val().toString().toLowerCase().replace( /[^a-z0-9_]/ , "" ) );
  });

  $("input[name='singular_name']").keyup(function(e){
    $(this).val( $(this).val().toString().toLowerCase().replace( /[^a-z0-9_]/ , "" ) );
  });

});

</script>
