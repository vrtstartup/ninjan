



<div class="row">
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

  <h3>Database</h3>
  <p>
    The data base can be administered using "phpliteadmin" or another sqlite db tool.
  </p>

  <p>
    The DB is located at "../db/database.sqlite" here is an phpliteadmin link:
    <a target="new" href="phpliteadmin.php">phpLiteAdmin</a>.
    This is sQlite which you may not have used before. sQlite is just a file.
    The db file and the directory it is in need to have write access to the
    web server.
  </p>

  <h3>Table Tools</h3>

  <p>
    In my projects each table is an entity. Thus if my project has
    employees in it. You can be 100% certain that I have a db table
    employees.
  </p>

  <p>
    When working with these tables you need to make C.R.U.D. Create, Report,
    Update, Delete modules. To aid and assist in making these modules, you
    can use the following tools.
  </p>

  <p>
    The "form" will generate a copy and pastable generic form for the table
    selected. The fields link will produce a straight PHP associative array
    that you can use to copy and paste with. The "regex" will generate a
    basic generic set of field validation regular expressions.
  </p>

  <table class="table table-striped">
  <thead>
  <tr>
  <th>Table</th>
  <th>Form</th>
  <th>Fields</th>
  <th>Regex</th>
  </tr>
  </thead>

  <tbody>
  <?php foreach ($tables as $t) { ?>
  <tr>
  <td><?php echo $t; ?></td>
  <td><a href="?module=<?php echo $MODULE; ?>&action=formgen&table=<?php echo $t; ?>">form</a></td>
  <td><a href="?module=<?php echo $MODULE; ?>&action=fieldsgen&table=<?php echo $t; ?>">fields</a></td>
  <td><a href="?module=<?php echo $MODULE; ?>&action=regs&table=<?php echo $t; ?>">regex</a></td>
  </tr>
  <?php } ?>
  </tbody>

  </table>

</div>

<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">



  <h3>Modules</h3>

  <p>
    Entities / Modules / Things ... If your project has things that the client
    would like to manage. Then here is where you can create a base module
    to create a table and manage the entity.
  </p>

  <p>
    <a href="?module=<?php echo $MODULE; ?>&action=cmod">Create a Module</a>
  </p>

</div>

</div>




  <h4>Copy and Paste Form Helpers</h4>

  <p>
    When building forms ( front or back -end ) you can copy and paste the
    following to speed things for various types of fields.
  </p>

  <ul class="nav nav-tabs">
    <li><a href="#text" data-toggle="tab">Text</a></li>
    <li><a href="#textarea" data-toggle="tab">TextArea</a></li>
    <li><a href="#date" data-toggle="tab">Date</a></li>
    <li><a href="#country" data-toggle="tab">Country</a></li>
    <li><a href="#fkey" data-toggle="tab">FKey</a></li>
    <li><a href="#boolean" data-toggle="tab">Boolean</a></li>
    <li><a href="#file" data-toggle="tab">File</a></li>
  </ul>

<div class="tab-content">
    
    <div class="tab-pane active" id="text">
<pre>
  &lt;label for=&quot;some_field&quot; class=&quot;control-label &lt;?php echo errors( &#x27;some_field&#x27; ); ?&gt;&quot;&gt;
    &lt;?php echo strNice( &#x27;some_field&#x27; ); ?&gt;
  &lt;/label&gt;
  &lt;input type=&quot;text&quot; class="form-control" id=&quot;some_field&quot; 
         name=&quot;some_field&quot; value=&quot;&lt;?php echo pvar( &#x27;some_field&#x27; ); ?&gt;&quot; required="required" pattern="^.{1,255}$" /&gt;
</pre>
    </div>
    <div class="tab-pane" id="textarea">
<pre>
  &lt;label class="control-label" for="some_field"&gt;&lt;?php echo strNice( "some_field" ); ?&gt;&lt;/label&gt;
  &lt;textarea name="some_field" id="some_field" class="form-control" rows="4" required="required" pattern="^.{0,1000}$" title="&lt;?php echo strNice( "some_field" ); ?&gt;"&gt;&lt;?php echo pvar( "some_field" ); ?&gt;&lt;/textarea&gt;  
</pre>  

    </div>
    <div class="tab-pane" id="date">
<pre>
  &lt;label class="control-label" for="some_field"&gt;&lt;?php echo strNice( "some_field" ); ?&gt;&lt;/label&gt;
  &lt;?php echo getDateSelect( "some_field" ); ?&gt;
</pre>
    </div>
    <div class="tab-pane" id="country">
<pre>
  &lt;label for=&quot;some_field&quot; class=&quot;control-label &lt;?php echo errors( &#x27;some_field&#x27; ); ?&gt;&quot;&gt;
    &lt;?php echo strNice( &#x27;some_field&#x27; ); ?&gt;
  &lt;/label&gt;
  &lt;?php echo getCountrySelect( &quot;some_field&quot; ); ?&gt;
</pre>
    </div>

  <div class="tab-pane" id="fkey">
<pre>
  &lt;label for=&quot;some_field&quot; class=&quot;control-label &lt;?php echo errors( &#x27;some_field&#x27; ); ?&gt;&quot;&gt;
    &lt;?php echo strNice( &#x27;some_field&#x27; ); ?&gt;
  &lt;/label&gt;
  &lt;?php echo getSelectFromTable( &quot;some_field&quot;, &quot;other_table&quot;, &quot;label_field&quot;, &quot;values_field&quot;, 
                                 $where = -1 , $orderby = -1 , $default = null ); ?&gt;
</pre>
  </div>


  <div class="tab-pane" id="boolean">
<pre>
  &lt;label for="some_field-1" class="control-label"&gt;
    &lt;?php echo strNice( 'some_field' ); ?&gt;
  &lt;/label&gt;
  &lt;?php echo getYesNo( "some_field" ); ?&gt;
</pre>
  </div>



  <div class="tab-pane" id="file">
<pre>
  &lt;label class="control-label" for="some_field"&gt;&lt;?php echo strNice( "some_field" ); ?&gt;&lt;/label&gt;

    &lt;?php if ( strlen( pvar( 'some_field' ) ) ) { // Handle the update version ?&gt;
      &lt;br /&gt;
      &lt;strong&gt;Currently:&lt;/strong&gt; &lt;a target="_blank" href="../&lt;?php echo pvar( 'some_field' ); ?&gt;"&gt;&lt;?php echo pvar( 'some_field' ); ?&gt;&lt;/a&gt;
      &lt;div class="checkbox"&gt;
        &lt;label&gt;
          &lt;input type="checkbox" name="delete-some_field" id="delete-some_field" /&gt;Delete ?
        &lt;/label&gt;
      &lt;/div&gt;
      &lt;input type="hidden" name="some_field" value="&lt;?php echo pvar( 'some_field' ); ?&gt;" /&gt;
    &lt;?php } ?&gt;

    
    &lt;input class="form-control" type="file" name="some_field" id="some_field" /&gt;
</pre>
  </div>


</div>
  

