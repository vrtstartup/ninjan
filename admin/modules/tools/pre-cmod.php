<?php

// Did we sumbit
if ( count( $_POST ) ) {

  // Create a temp sip archive that we will put out to the browser
  $zip = new ZipArchive();
  $tmp = tempnam("tmp", "zip");

  // open the zip file.
  $zip->open($tmp, ZipArchive::OVERWRITE);

  // get all the files.
  $templates = glob( $modules_directory . $MODULE . "/templates/*" );

  // Build content for each file.
  foreach ($templates as $template) {
    $k = basename( $template );
    $output[$k] = file_get_contents( $template );
  }

  // replace the fields in post.
  foreach ($output as $file => $content) {
    foreach ($_POST as $p => $q) {
      $content = preg_replace( "/%".$p."%/" , $q , $content );
    }

    // add the file to the zip archive.
    $zip->addFromString( $_POST['module_name'] . "/" . $file , $content );

  }

  // close the zip
  $zip->close();

  // Stream the file to the client
  header("Content-Type: application/zip");
  header("Content-Length: " . filesize($tmp));
  header("Content-Disposition: attachment; filename=\"".$_POST['module_name'].".zip\"");
  readfile($tmp);

  // Delete tmp file.
  unlink($tmp);

}
