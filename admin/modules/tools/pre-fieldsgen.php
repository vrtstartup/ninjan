<?php

$r = "";

if ( ! isset( $_GET['table'] ) ) {
  header("Location: ?module=".$MODULE);
  exit( 0 );
}

$t = $_GET['table'];

if ( ! dbTableExists($t) ) {
  header("Location: ?module=".$MODULE);
  exit( 0 );
}

$base = '  $data[\':field:\']:spaces:= $_POST[\':field:\'];
';


$fields = dbFields( $t );

foreach ($fields as $field) {

  $t = preg_replace( "/:field:/" , $field , $base );
  $t = preg_replace( "/:spaces:/" , str_pad( " " , 25 - strlen( $field ) ) , $t );
  $r .= $t;

}


$r = '  $data = array();
'.$r;
