<?php

$r = "";

if ( ! isset( $_GET['table'] ) ) {
  header("Location: ?module=".$MODULE);
  exit( 0 );
}

$t = $_GET['table'];

if ( ! dbTableExists($t) ) {
  header("Location: ?module=".$MODULE);
  exit( 0 );
}

$base = '
  &lt;div class="form-group&lt;?php if( isset( $errors[\':field:\'] ) ) { ?&gt; has-error&lt;?php } ?&gt;"&gt;
    &lt;label class="control-label" for=":field:"&gt;&lt;?php echo strNice( ":field:" ); ?&gt;&lt;/label&gt;
    &lt;input type="text" name=":field:" id=":field:" class="form-control" value="&lt;?php echo pvar( ":field:" ); ?&gt;" required="required" pattern="^.{1,255}$" title="&lt;?php echo strNice( ":field:" ); ?&gt;"&gt;
  &lt;/div&gt;
';

$fields = dbFields( $t );

foreach ($fields as $field) {
  $t = $base;
  $t = preg_replace( "/:field:/" , $field , $t );
  $r .= $t;
}
