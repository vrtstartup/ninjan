<!-- This is a basic generic form generated from the admin -->

<!-- To generic a full generic form you can use the tool in the admin -->

<form enctype="multipart/form-data" method="post">

  <label for="%singular_name%_key" class="<?php echo errors( '%singular_name%_key' ); ?>">Name</label>
  <input type="text" id="%singular_name%_key" name="%singular_name%_key" value="<?php echo pvar( '%singular_name%_key' ); ?>" />

  <br />
  <button type="submit" class="btn">Save</button>

</form>
