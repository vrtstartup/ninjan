<!-- Standard Link for creating entries -->
<!-- Create Link -->

<p>
  <a href="?module=<?php echo $MODULE; ?>&action=create">Create a new <?php echo $prefix; ?></a>
</p>

<!-- Standard HTML table for showing entries -->
<!-- Certainly Smart people can do better -->
<table class="table table-striped table-bordered">
<thead>
  <tr>
    <th>Id</th>
    <th>Key</th>

    <!-- Add more fields to show here -->

    <!-- End Add -->

    <th>Delete</th>
  </tr>
</thead>

<tbody>

<?php while ( $qrow = dbFetch($qr) ) { ?>
  <tr onMouseOver="this.style.background = '#AAAAAA';" onMouseOut="this.style.background = 'none';">

    <td><a href="?module=<?php echo $MODULE; ?>&action=update&<?php echo $key_field; ?>=<?php echo $qrow[$key_field] ?>"><?php echo $qrow[$key_field]; ?></a></td>
    <td><a href="?module=<?php echo $MODULE; ?>&action=update&<?php echo $key_field; ?>=<?php echo $qrow[$key_field] ?>"><?php echo $qrow[$prefix.'_key']; ?></a></td>

    <!-- Add more fields to here -->

    <!-- End Add -->

    <!-- Delete Field -->
    <td><a href="?module=<?php echo $MODULE; ?>&action=delete&<?php echo $key_field; ?>=<?php echo $qrow[$key_field]; ?>" onClick="return confirm( 'Are you 100% sure that you want to delete?' );">delete</a></td>
  </tr>
<?php } ?>

</tbody>

</table>
