<?php

$errors = array();

if ( count( $_POST ) ) { // If there are post vars then the formw as submitted?
    include_once( $modules_directory . $MODULE . "/regs.php" ); // Include regular expressions

    $errors = validateVals( $regs ); // Generate errors array

    // Here there probably also be some validation.
    // Just populate errors( '']

    // Are there errors?
    if ( count( $errors ) == 0 ) {
      // Ok there are no errors now we want to
      // Set various things that need to be hard coded.

      // This should be change to aliasString( $_POST['title_field'] );
      // However since I can't predict that you will have a title field...
      // We do this instead.
      $_POST[$prefix.'_key'] = uniqid(); // generate the key field

      //$_POST[$prefix.'_timestamp'] = time();
      //$_POST[$prefix.'_ip'] = $_SERVER['REMOTE_ADDR'];

      // Now we build query
      $q = dbInsertFromVals( $table );
      $qr = dbQuery( $q );

      // Get The ID
      $id = dbLastInsertId();

      // Handle File Uplaods
      // This is a very basic way
      // Start with this example and do image resizing/mime checking etc.
      handleUploads( $table , $key_field , $id );

      // dbReorderTable($table, $key_field, "weight_field");
      // dbReorderTableGroup($table, $key_field, "weight_field", "group_field", 'group_value');
      // Well now that the entry is create and files handled... ;)
      // Let's go back to the index
      header( "Location: ?module=".$MODULE );
      exit( 0 );

    }
  } else {

     // Here we are going to put in our form defaults.
     // There was nothing in $_POST so this is the first time we come to the
     // page. Let's load up the $_POST with some defaults?

     //$_POST['field_something'] = 'Default';

  }
