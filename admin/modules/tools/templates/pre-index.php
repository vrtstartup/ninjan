<?php

  // This is a generic pre-index.php
  // This gets included before displaying the index action.

  // 99% of the time we are going to build up a basic query to get the stuff
  // from the table.

  // Handle moving.

  /*
  if ( isset( $_GET['moveup'] ) ) {
    //dbMoveUp($table, $key_field, intval( $_GET['moveup'] ), "weight_field" );
    dbMoveUpGroup($table, $key_field, intval( $_GET['moveup'] ), "weight_field", "group_field" );
    unset( $_GET['moveup'] );
    header( "Location: ?" . http_build_query($_GET) );
    exit( 0 );
  }

  if ( isset( $_GET['movedown'] ) ) {
    //dbMoveDown($table, $key_field, intval( $_GET['movedown'] ), "weight_field" );
    dbMoveDown($table, $key_field, intval( $_GET['movedown'] ), "weight_field", "group_field" );
    unset( $_GET['movedown'] );
    header( "Location: ?" . http_build_query($_GET) );
    exit( 0 );
  }
  */

  // Basic query on the table.
  $q = "SELECT * FROM ".$table." ORDER BY ".$key_field." DESC";

  // You can add a filter, clauses, paging, etc.. go for it!

  $qr = dbQuery( $q );
