<?php

  // This is a generic pre update file
  // Typically here we do the code that needs to be done to update
  // an entry of our modules.

  // our ID
  $id = intval( $_GET[$key_field] );

  if ( count( $_POST ) ) { // If there are post vars then the formw as submitted?
    include_once( $modules_directory . $MODULE . "/regs.php" ); // Include regular expressions

    $errors = validateVals( $regs , $_POST ); // Generate errors array

    // Here there probably also should be some validation.
    // Just populate errors( '']

    // Are there errors?
    if ( count( $errors ) == 0 ) {
      // Ok there are no errors now we want to
      // Set various things that need to be hard coded.
      //$_POST[$prefix.'_key'] = aliasString( pvar( 'field' ) ); // generate the key field
      //$_POST[$prefix.'_timestamp'] = time();
      //$_POST[$prefix.'_ip'] = $_SERVER['REMOTE_ADDR'];

      // Now we build query
      $q = dbUpdateFromVals( $table , $key_field, $id );
      $qr = dbQuery( $q );

      // Handle File Uplaods
      // This is a very basic way
      // Start with this and do image resizing/mime checking etc.
      handleUploads( $table , $key_field , $id );

      // dbReorderTable($table, $key_field, "weight_field");
      // dbReorderTableGroup($table, $key_field, "weight_field", "group_field", 'group_value');
      // Well now that the entry is udpated and files handled... ;)
      // Let's go back to the index
      header( "Location: ?module=".$MODULE );
      exit( 0 );
    }
  } else {

     // Here we are going to put in our form defaults.
     // There was nothing in $_POST so this is the first time we come to the
     // page. Let's load up the $_POST with some defaults?

     // Load the post with the data in the db.
     $_POST = dbFetchOneRow($table, $key_field, $id);

     //$_POST['field_something'] = 'Default';

  }
