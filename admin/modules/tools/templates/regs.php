<?php

  // This contians the regular expression for genral validation on fields.
  // typically every field should be validated...

  // Here are samples

  //$regs['some_field'] = "/^..*$/"; // Basic required check
  //$regs['some_field'] = "/^..*\@..*\...*$/"; // Basic Email
  //$regs['some_field'] = "/^[0-9][0-9]*$/"; // Basic Number
  //$regs['some_password'] = "/^.....*$/"; // Basic password check
  //$regs['password'] = "/^".$_POST['some_password']."$/"; // pass conf check

  // Puppy Example
  /*
  $regs['puppy_name'] = "/^..*$/";
  $regs['puppy_breed'] = "/^..*$/";
  $regs['puppy_adopted'] = "/^..*$/";
  $regs['puppy_description'] = "/^..*$/";

  $regs['puppy_arrived'] = "/^..*$/";
  */

  // To generate you own generic regular expression very quickly you can
  // use the tools in the admin.

  $regs = array();