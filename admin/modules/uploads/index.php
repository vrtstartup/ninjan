<?php

$path = "";

if ( isset( $_GET['path'] ) ) {
  $path = $_GET['path'];
}

$path = str_replace( ".." , "" , $path );
$path = preg_replace( "/^\./" , "" , $path );
$path = preg_replace( "/^\//" , "" , $path );
$path = preg_replace( "/\/$/" , "" , $path );

if ( isset( $_POST['newdir'] ) && preg_match( "/^[a-z][a-z]*$/" , $_POST['newdir'] ) ) {
  mkdir( $BASE . $path . "/" . $_POST['newdir'] );
}

if ( isset($_FILES['upload']) && is_uploaded_file($_FILES['upload']['tmp_name']) ) {
  move_uploaded_file($_FILES['upload']['tmp_name'], $BASE . $path . "/" . $_FILES['upload']['name'] );
}

if ( isset( $_GET['delete'] ) && $_GET['delete'] == 1 ) {

  if ( is_file( $BASE . $path ) ) {
    unlink( $BASE . $path );
  }

  if ( is_dir( $BASE . $path ) ) {
    delTree( $BASE . $path );
  }

  header( "Location: " . $BL . dirname( $path ) );
  exit( 0 );
}

$items = scandir( $BASE . $path );
$cwd = array_shift( $items );
$up = array_shift( $items );

function delTree($dir)
{
  $files = array_diff(scandir($dir), array('.','..'));
  foreach ($files as $file) {
    (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
  }

  return rmdir($dir);
}

?>

<h3 class="pull-left">uploads/<?php echo $path; ?></h3>

<div class="pull-right">
  <a class='btn btn-info' href="<?php echo $BL; ?>">start</a>  
  <a class='btn btn-info' href="<?php echo $BL . dirname( $path ); ?>">parent directory</a>
</div>

<span class="clearfix"></span>
<hr />

<table class="table table-striped table-bordered">
  <?php while ( $f = array_shift( $items ) ) { ?>

    <tr>
    <?php if ( is_dir( $BASE . $path . "/" . $f ) ) { ?>

      <!-- directory -->
      <td> <a href="<?php echo $BL . $path . "/" . $f; ?>"><?php echo $f; ?></a> /</td>

    <?php } else { ?>

      <!-- file -->
      <td> <a target="new" href="<?php echo $BASE . $path . "/" . $f; ?>"><?php echo $f; ?></a> </td>

    <?php } ?>

    <td><?php echo floor( filesize( $BASE . $path . "/" . $f) / 1024 ) ?> kb</td>

    <td><a href="<?php echo $BL . $path . "/" . $f; ?>&delete=1">delete</a></td>

    </tr>

  <?php } ?>

</table>

<hr />

<form role="form" enctype="multipart/form-data" method="post">
  <div class="form-group">
    <label class="control-label" for="upload" >Upload a File</label>
    <input class="form-control" type="file" id="upload" name="upload" />
  </div>
  
  <button type="submit" class="btn btn-primary">Upload</button>
</form>

<hr />

<form role="form" method="post">
  <div class="form-group">
    <label class="control-label">Create New Directory</label>
    <input class="form-control" type="text" name="newdir" id="newdir" />
  </div>

  <button type="submit" class="btn btn-primary">Create</button>
</form>

<hr />

<p class="text-warning">
  This is a just a basic file manager with delete and upload abilities.
  It is not meant to replace a real FTP/SFTP client like Transmit or WinSCP.
  Please contact the system developer / web administrator for further details.
</p>
