    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="./"><?php echo $_SERVER['SERVER_NAME']; ?></a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <?php if( isset( $_SESSION['admin_user'] ) ) { ?>
            
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Content<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <?php foreach ($modules as $module) { ?>
                  <?php if( ! in_array( $module , $noshows ) && ( ! $require_login || $module_levels[$module] >= intval( $_SESSION['admin_user_level'] ) ) ) { ?>
                    <li><a href="?module=<?php echo $module; ?>"><span><?php echo ucwords( $module ) ?></span></a></li>
                  <?php } ?>
                <?php } ?>
              </ul>
            </li>

            

            

            

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Utilities<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="?module=tools"><span>Tools</span></a></li>
                <li><a href="?module=uploads"><span>Uploads</span></a></li>
              </ul>
            </li>

            <?php if( isset( $_SESSION['admin_user'] ) ) { ?>
              <li><a href="logout.php">Logout</a></li>
            <?php } ?>

            

            <?php } ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
