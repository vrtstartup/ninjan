
<!-- CUSTOM FOOTER -->
<div id="footer">
	<div class="container">
		<p class="text-muted credit">
			Rue Egide Van Ophem 40 A | B-1180 Bruxelles | Tel: +32 2 344 2044 | Fax: +32 2 344 50 44 | <a href="mailto:office@mw-a.net">office@mw-a.net</a><br>
			<a href="http://www.mostwanted-agency.net/" target="_blank">www.mostwanted-agency.net</a> | TVA BE 0826.289.451 | 			&copy; MostWanted - Wild Digital Agency <?php echo date('Y'); ?>
		</p>
	</div>
</div>