    <div class="navbar navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="./"><?php echo $_SERVER['SERVER_NAME']; ?></a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <?php if( isset( $_SESSION['admin_user'] ) ) { ?>
            
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Content<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <?php foreach ($modules as $module) { ?>
                  <?php if( ! in_array( $module , $noshows ) && ( ! $require_login || $module_levels[$module] >= intval( $_SESSION['admin_user_level'] ) ) ) { ?>
                    <li><a href="?module=<?php echo $module; ?>"><span><?php echo ucwords( $module ) ?></span></a></li>
                  <?php } ?>
                <?php } ?>
              </ul>
            </li>

            

            

            

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Utilities<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="?module=tools"><span>Tools</span></a></li>
                <li><a href="?module=uploads"><span>Uploads</span></a></li>
              </ul>
            </li>

            <?php if( isset( $_SESSION['admin_user'] ) ) { ?>
              <li><a href="logout.php">Logout</a></li>
            <?php } ?>

            

            <?php } ?>
          </ul>
          
          <!-- MWA LOGO -->
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#" class="mwa-logo" data-toggle="modal" data-target="#mwainfo">
                <span class="desktop"></span>
                <span class="mobile">MostWanted</span>
              </a></li>
          </ul>
          
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    <!-- MODAL -->
    <div class="modal fade" id="mwainfo">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">MostWanted - Wild Digital Agency</h4>
          </div>
          <div class="modal-body">
              <address class="address">
              <p><strong>Address</strong></p>
              <p>RUE EGIDE VAN OPHEM, 40 A<br/>
              1180 BRUXELLES<br/>
              TÉL +32 2 344 20 44<br/>
              FAX +32 2 344 50 44<br/>
              MAIL <a href="mailto:info@mw-a.net">INFO@MW-A.NET</a></p>
            </address>
            <ul class="networks">
              <li class="title">Follow us on:</li>
              <li><a target="_blank" href="http://www.linkedin.com/company/mostwanted">LinkedIn</a></li>
              <li><a target="_blank" href="http://www.facebook.com/mostwanted.digital">Facebook</a></li>
              <li><a target="_blank" href="http://www.pinterest.com/MostWantedDigit/">Pinterest</a></li>
              <li><a target="_blank" href="http://twitter.com/MostWantedDigit">Twitter</a></li>
            </ul>
          </div>
          <div class="modal-footer">
            
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
