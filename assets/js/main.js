function s4() {
  return Math.floor((1 + Math.random()) * 0x10000)
             .toString(16)
             .substring(1);
};

function guid() {
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
         s4() + '-' + s4() + s4() + s4();
}


$(function () {

  var input     = $(".fileElem");
  var label     = $(".fileSelect");

  $('.fileUpload').fileupload({
    
    dropZone: $('.drag-drop'),

    add: function (e, data) {
      input.attr("disabled", "disabled");
      label.toggleClass("disabled");
      label.find('span.txt').toggleClass("hide");
      label.find('i.fa-spinner').toggleClass("hide");

      var jqXHR = data.submit();
    },

    done: function(e, data) {
      if( data.result == "OK" ) {
        
        var despan = label.find('span.txt');

        if( $(despan).hasClass( 'step2' ) ) {
          
          label.find('span.txt').toggleClass("hide");
          label.find('span.txt').text('Finished uploading');
          label.find('i.fa-spinner').toggleClass("hide");

          $(".step2").find('i.fa-spinner').toggleClass("hide");
          $.get( document.location.href + "&step2=1" , function( response ) {

            if( response == "OK" ) {
              $('form.fileUpload').fadeOut(300, function() {
                $('.loading_video_steps').parent().fadeIn(300);
              });


              $(".step2").find('i.fa-spinner').toggleClass("hide");
              $(".step2").find('i.fa-check-circle-o').toggleClass("hide");
              $(".step3").find('i.fa-spinner').toggleClass("hide");
              $('.step3').removeClass('inactive');

              $.get( document.location.href + "&step3=1" , function( response ) {

                if( response == "OK" ) {

                  $(".step3").find('i.fa-spinner').toggleClass("hide");
                  $(".step3").find('i.fa-check-circle-o').toggleClass("hide");
                  $(".step4").find('i.fa-spinner').toggleClass("hide");
                  $('.step4').removeClass('inactive');

                  $.get( document.location.href + "&step4=1" , function( response ) {

                    if( response == "OK" ) {
                      $(".step4").find('i.fa-spinner').toggleClass("hide");
                      $(".step4").find('i.fa-check-circle-o').toggleClass("hide");

                      $("#verb").text( "Next" );
                      $('.link_next').removeClass("hide");
                    }
                  });
                }
              });
            }
          });
        } 
        else if( $(despan).hasClass( 'step1' ) ) {

          label.find('span.txt').toggleClass("hide");
          label.find('span.txt').text('Finished uploading');
          label.find('i.fa-spinner').toggleClass("hide");

          $(".step2").find('i.fa-spinner').toggleClass("hide");
          
          $.get( document.location.href + "&step2=1" , function( response ) {

            if( response == "OK" ) {

              $('form.fileUpload').fadeOut(300, function() {
                $('.loading_audio_steps').parent().fadeIn(300);
              });

              $(".step2").find('i.fa-spinner').toggleClass("hide");
              $(".step2").find('i.fa-check-circle-o').toggleClass("hide");
                              
              $("#verb").text( "Next" );
              $('.link_next').removeClass("hide");
            }
          });
        } 
        else {
          label.find('span.txt').toggleClass("hide");
          label.find('span.txt').text('Finished uploading');
          label.find('i.fa-spinner').toggleClass("hide");

          $('form.fileUpload').fadeOut(750, function() {
            $("#verb").text( "Next" );
          });
        }
      } 
      else {
        label.toggleClass("disabled");
        label.find('span.txt').toggleClass("hide");
        label.find('i.fa-spinner').toggleClass("hide");
        input.attr("disabled", "disabled");

        var error_wrapper = $('.error_message');

        error_wrapper.find('p').html(data.result);
        error_wrapper.slideDown(750, function() {
          error_wrapper.delay(3000).slideUp(500);
        });
      }
    }
  });
});


$.fn.serializeObject = function(){
  var o = {};
  var a = this.serializeArray();
  $.each(a, function() {
    if (o[this.name]) {
      if (!o[this.name].push) {
        o[this.name] = [o[this.name]];
      }
      o[this.name].push(this.value || '');
    } 
    else {
      o[this.name] = this.value || '';
    }
  });
  return o;
};


function UrlExists(url) {
  var http = new XMLHttpRequest();

  http.open('HEAD', url, false);
  http.send();

  return http.status!=404;
}

function normalizeNinja( ninja ) {

  for( var k in ninja.overlays ) { }


  for( var k in ninja.images ) {

    delete ninja.images[k].fired;


    if( typeof ninja.images[k].x === "undefined" || ninja.images[k].x === "") {
      ninja.images[k].x = 0;
    }

    if( typeof ninja.images[k].y === "undefined" || ninja.images[k].y === "") {
      ninja.images[k].y = 0;
    }

    if( typeof ninja.images[k].scaleX === "undefined" || ninja.images[k].scaleX === "") {
      ninja.images[k].scaleX = 1;
    }

    if( typeof ninja.images[k].scaleY === "undefined" || ninja.images[k].scaleY === "") {
      ninja.images[k].scaleY = 1;
    }

    

    if( typeof ninja.images[k].width === "undefined" || ninja.images[k].width === "") {
      ninja.images[k].width = 1280;
    }

    if( typeof ninja.images[k].height === "undefined" || ninja.images[k].height === "") {
      ninja.images[k].height = 720;
    }


    if( typeof ninja.images[k].crop_x === "undefined" || ninja.images[k].crop_x === "") {
      ninja.images[k].crop_x = 0;
    }

    if( typeof ninja.images[k].crop_y === "undefined" || ninja.images[k].crop_y === "") {
      ninja.images[k].crop_y = 0;
    }

    if( typeof ninja.images[k].crop_width === "undefined" || ninja.images[k].crop_width === "") {
      ninja.images[k].crop_width = 1280;
    }

    if( typeof ninja.images[k].crop_height === "undefined" || ninja.images[k].crop_height === "") {
      ninja.images[k].crop_height = 720;
    }


    if( typeof ninja.images[k].frame === "undefined" || ninja.images[k].frame === "") {
      ninja.images[k].frame = 0;
    }

    if( typeof ninja.images[k].duration === "undefined" || ninja.images[k].duration === "") {
      ninja.images[k].duration = 5;
    }

    if( typeof ninja.images[k].opacity === "undefined" || ninja.images[k].opacity === "") {
      ninja.images[k].opacity = 1;
    }


    


    if( typeof ninja.images[k].intro_easing === "undefined" || ninja.images[k].intro_easing === "") {
      ninja.images[k].intro_easing = "Linear";
    }

    if( typeof ninja.images[k].intro_opacity === "undefined" || ninja.images[k].intro_opacity === "") {
      ninja.images[k].intro_opacity = 1;
    }


    if( typeof ninja.images[k].intro_duration === "undefined" || ninja.images[k].intro_duration === "") {
      ninja.images[k].intro_duration = 0.1;
    }

    if( typeof ninja.images[k].intro_x === "undefined" || ninja.images[k].intro_x === "") {
      ninja.images[k].intro_x = ninja.images[k].x;
    }

    if( typeof ninja.images[k].intro_y === "undefined" || ninja.images[k].intro_y === "") {
      ninja.images[k].intro_y = ninja.images[k].y;
    }

    if( typeof ninja.images[k].intro_scaleX === "undefined" || ninja.images[k].intro_scaleX === "") {
      ninja.images[k].intro_scaleX = 1;
    }

    if( typeof ninja.images[k].intro_scaleY === "undefined" || ninja.images[k].intro_scaleY === "") {
      ninja.images[k].intro_scaleY = 1;
    }

    if( typeof ninja.images[k].intro_width === "undefined" || ninja.images[k].intro_width === "") {
      ninja.images[k].intro_width = ninja.images[k].width;
    }

    if( typeof ninja.images[k].intro_height === "undefined" || ninja.images[k].intro_height === "") {
      ninja.images[k].intro_height = ninja.images[k].height;
    }

    if( typeof ninja.images[k].intro_crop_x === "undefined" || ninja.images[k].intro_crop_x === "") {
      ninja.images[k].intro_crop_x = ninja.images[k].crop_x;
    }

    if( typeof ninja.images[k].intro_crop_y === "undefined" || ninja.images[k].intro_crop_y === "") {
      ninja.images[k].intro_crop_y = ninja.images[k].crop_y;
    }

    if( typeof ninja.images[k].intro_crop_width === "undefined" || ninja.images[k].intro_crop_width === "") {
      ninja.images[k].intro_crop_width = ninja.images[k].crop_width;
    }

    if( typeof ninja.images[k].intro_crop_height === "undefined" || ninja.images[k].intro_crop_height === "") {
      ninja.images[k].intro_crop_height = ninja.images[k].crop_height;
    }


    if( typeof ninja.images[k].outro_easing === "undefined" || ninja.images[k].outro_easing === "") {
      ninja.images[k].outro_easing = "Linear";
    }

    if( typeof ninja.images[k].outro_opacity === "undefined" || ninja.images[k].outro_opacity === "") {
      ninja.images[k].outro_opacity = 0;
    }


    if( typeof ninja.images[k].outro_duration === "undefined" || ninja.images[k].outro_duration === "") {
      ninja.images[k].outro_duration = 0.1;
    }

    if( typeof ninja.images[k].outro_x === "undefined" || ninja.images[k].outro_x === "") {
      ninja.images[k].outro_x = ninja.images[k].x;
    }

    if( typeof ninja.images[k].outro_y === "undefined" || ninja.images[k].outro_y === "") {
      ninja.images[k].outro_y = ninja.images[k].y;
    }

    if( typeof ninja.images[k].outro_scaleX === "undefined" || ninja.images[k].outro_scaleX === "") {
      ninja.images[k].outro_scaleX = 1;
    }

    if( typeof ninja.images[k].outro_scaleY === "undefined" || ninja.images[k].outro_scaleY === "") {
      ninja.images[k].outro_scaleY = 1;
    }

    if( typeof ninja.images[k].outro_width === "undefined" || ninja.images[k].outro_width === "") {
      ninja.images[k].outro_width = ninja.images[k].width;
    }

    if( typeof ninja.images[k].outro_height === "undefined" || ninja.images[k].outro_height === "") {
      ninja.images[k].outro_height = ninja.images[k].height;
    }

    if( typeof ninja.images[k].outro_crop_x === "undefined" || ninja.images[k].outro_crop_x === "") {
      ninja.images[k].outro_crop_x = ninja.images[k].crop_x;
    }

    if( typeof ninja.images[k].outro_crop_y === "undefined" || ninja.images[k].outro_crop_y === "") {
      ninja.images[k].outro_crop_y = ninja.images[k].crop_y;
    }

    if( typeof ninja.images[k].outro_crop_width === "undefined" || ninja.images[k].outro_crop_width === "") {
      ninja.images[k].outro_crop_width = ninja.images[k].crop_width;
    }

    if( typeof ninja.images[k].outro_crop_height === "undefined" || ninja.images[k].outro_crop_height === "") {
      ninja.images[k].outro_crop_height = ninja.images[k].crop_height;
    }

    ninja.images[k].frame = parseInt( ninja.images[k].frame );

    ninja.images[k].x = parseInt( ninja.images[k].x );
    ninja.images[k].y = parseInt( ninja.images[k].y );

    ninja.images[k].intro_x = parseInt( ninja.images[k].intro_x );
    ninja.images[k].intro_y = parseInt( ninja.images[k].intro_y );

    ninja.images[k].outro_x = parseInt( ninja.images[k].outro_x );
    ninja.images[k].outro_y = parseInt( ninja.images[k].outro_y );

    ninja.images[k].width = parseInt( ninja.images[k].width );
    ninja.images[k].height = parseInt( ninja.images[k].height );

    ninja.images[k].intro_width = parseInt( ninja.images[k].intro_width );
    ninja.images[k].intro_height = parseInt( ninja.images[k].intro_height );

    ninja.images[k].outro_width = parseInt( ninja.images[k].outro_width );
    ninja.images[k].outro_height = parseInt( ninja.images[k].outro_height );

    ninja.images[k].duration = parseFloat( ninja.images[k].duration );
    ninja.images[k].opacity = parseFloat( ninja.images[k].opacity );

    ninja.images[k].intro_duration = parseFloat( ninja.images[k].intro_duration );
    ninja.images[k].intro_opacity = parseFloat( ninja.images[k].intro_opacity );

    ninja.images[k].crop_x = parseInt( ninja.images[k].crop_x );
    ninja.images[k].crop_y = parseInt( ninja.images[k].crop_y );

    ninja.images[k].crop_width = parseInt( ninja.images[k].crop_width );
    ninja.images[k].crop_height = parseInt( ninja.images[k].crop_height );


    ninja.images[k].scaleX = parseFloat( ninja.images[k].scaleX );
    ninja.images[k].scaleY = parseFloat( ninja.images[k].scaleY );

    ninja.images[k].intro_scaleX = parseFloat( ninja.images[k].intro_scaleX );
    ninja.images[k].intro_scaleY = parseFloat( ninja.images[k].intro_scaleY );

    ninja.images[k].outro_scaleX = parseFloat( ninja.images[k].outro_scaleX );
    ninja.images[k].outro_scaleY = parseFloat( ninja.images[k].outro_scaleY );
    

  }

  for( var k in ninja.texts ) {

    delete ninja.texts[k].fired;

    // normalize the x and y.
    // set the actor in the center of the screen if not defined.
    if( typeof ninja.texts[k].x === "undefined" || ninja.texts[k].x === "") {
      ninja.texts[k].x = 640;
    }

    if( typeof ninja.texts[k].y === "undefined" || ninja.texts[k].y === "") {
      ninja.texts[k].y = 360;
    }

    if( typeof ninja.texts[k].center === "undefined" || ninja.texts[k].center == true) {
      ninja.texts[k].center = "true";
    }
    else if(ninja.texts[k].center === "" || ninja.texts[k].center == false) {
      ninja.texts[k].center = "false";
    }


    if( typeof ninja.texts[k].align === "undefined" || ninja.texts[k].align === "") {
      ninja.texts[k].align = "center";
    }

    // set the intro and outro x an y to default.
    if( typeof ninja.texts[k].intro_x === "undefined" || ninja.texts[k].intro_x === "") {
      ninja.texts[k].intro_x = ninja.texts[k].x;
    }
    if( typeof ninja.texts[k].intro_y === "undefined" || ninja.texts[k].intro_y === "") {
      ninja.texts[k].intro_y = ninja.texts[k].y;
    }
    if( typeof ninja.texts[k].outro_x === "undefined" || ninja.texts[k].outro_x === "") {
      ninja.texts[k].outro_x = ninja.texts[k].x;
    }
    if( typeof ninja.texts[k].outro_y === "undefined" || ninja.texts[k].outro_y === "") {
      ninja.texts[k].outro_y = ninja.texts[k].y;
    }

    // set some default intro default things.
    if( typeof ninja.texts[k].intro_duration === "undefined" || ninja.texts[k].intro_duration === "") {
      ninja.texts[k].intro_duration = 0.5;
    }
    if( typeof ninja.texts[k].intro_easing === "undefined" || ninja.texts[k].intro_easing === "") {
      ninja.texts[k].intro_easing = "Linear";
    }
    if( typeof ninja.texts[k].intro_opacity === "undefined" || ninja.texts[k].intro_opacity === "") {
      ninja.texts[k].intro_opacity = 1;
    }

    // set some default outro default things.
    if( typeof ninja.texts[k].outro_duration === "undefined" || ninja.texts[k].outro_duration === "") {
      ninja.texts[k].outro_duration = 0.5;
    }
    if( typeof ninja.texts[k].outro_easing === "undefined" || ninja.texts[k].outro_easing === "") {
      ninja.texts[k].outro_easing = "Linear";
    }
    if( typeof ninja.texts[k].outro_opacity === "undefined" || ninja.texts[k].outro_opacity === "") {
      ninja.texts[k].outro_opacity = 0;
    }

    // set some default things.
    if( typeof ninja.texts[k].duration === "undefined" || ninja.texts[k].duration === "") {
      ninja.texts[k].duration = 1.0; // show for one second.
    }
    if( typeof ninja.texts[k].opacity === "undefined" || ninja.texts[k].opacity === "") {
      ninja.texts[k].opacity = 1;
    }

    if( typeof ninja.texts[k].fontFamily === "undefined" || ninja.texts[k].fontFamily === "") {
      ninja.texts[k].fontFamily = "Futura LT"; // show for one second.
    }

    if( typeof ninja.texts[k].fontSize === "undefined" || ninja.texts[k].fontSize === "") {
      ninja.texts[k].fontSize = 80; // show for one second.
    }

    if( typeof ninja.texts[k].intro_fontSize === "undefined" || ninja.texts[k].intro_fontSize === "") {
      ninja.texts[k].intro_fontSize = ninja.texts[k].fontSize; // show for one second.
    }

    if( typeof ninja.texts[k].outro_fontSize === "undefined" || ninja.texts[k].outro_fontSize === "") {
      ninja.texts[k].outro_fontSize = ninja.texts[k].fontSize; // show for one second.
    }

    if( typeof ninja.texts[k].fontStyle === "undefined" || ninja.texts[k].fontStyle === "") {
      ninja.texts[k].fontStyle = "normal"; // show for one second.
    }

    if( typeof ninja.texts[k].fill === "undefined" || ninja.texts[k].fill === "") {
      ninja.texts[k].fill = "white"; // show for one second.
    }

    ninja.texts[k].frame = parseInt( ninja.texts[k].frame );

    ninja.texts[k].x = parseInt( ninja.texts[k].x );
    ninja.texts[k].y = parseInt( ninja.texts[k].y );

    ninja.texts[k].intro_x = parseInt( ninja.texts[k].intro_x );
    ninja.texts[k].intro_y = parseInt( ninja.texts[k].intro_y );

    ninja.texts[k].outro_x = parseInt( ninja.texts[k].outro_x );
    ninja.texts[k].outro_y = parseInt( ninja.texts[k].outro_y );

    ninja.texts[k].outro_opacity = parseFloat( ninja.texts[k].outro_opacity );
    ninja.texts[k].opacity = parseFloat( ninja.texts[k].opacity );
    ninja.texts[k].intro_opacity = parseFloat( ninja.texts[k].intro_opacity );
    ninja.texts[k].duration = parseFloat( ninja.texts[k].duration );
    ninja.texts[k].intro_duration = parseFloat( ninja.texts[k].intro_duration );
    ninja.texts[k].outro_duration = parseFloat( ninja.texts[k].outro_duration );

    ninja.texts[k].fontSize = parseFloat( ninja.texts[k].fontSize );
    ninja.texts[k].intro_fontSize = parseFloat( ninja.texts[k].intro_fontSize );
    ninja.texts[k].outro_fontSize = parseFloat( ninja.texts[k].outro_fontSize );
  }
}

function convertToSlug(txt) {
  var slug = txt.toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,'');

  return slug
}


function secondsToHms(d) {
  d = Number(d);

  var h = Math.floor(d / 3600);
  var m = Math.floor(d % 3600 / 60);
  var s = Math.floor(d % 3600 % 60);

  var time = ((h > 0 ? h + ":" : "") + (m > 0 ? (h > 0 && m < 10 ? "0" : "") + m + ":" : "0:") + (s < 10 ? "0" : "") + s);

  return time; 
}
