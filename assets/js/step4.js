/*

      dP                                                       dP          
      88                                                       88          
.d888b88 .d8888b. .d8888b.    88d888b. .d8888b. .d8888b. .d888b88 dP    dP 
88'  `88 88'  `88 88'  `""    88'  `88 88ooood8 88'  `88 88'  `88 88    88 
88.  .88 88.  .88 88.  ...    88       88.  ... 88.  .88 88.  .88 88.  .88 
`88888P8 `88888P' `88888P'    dP       `88888P' `88888P8 `88888P8 `8888P88 
                                                                       .88 
                                                                   d8888P
*/

$(document).ready(function() {
  
  // lets setup and clean up all the texts.
  // everything needs to be half size, half x and half y.    
  var source    = $("#texts-template").html(); 
  var template  = Handlebars.compile(source);
  var texts     = { textslist: [] };

  var source_3    = $("#images-template").html(); 
  var template_3  = Handlebars.compile(source_3);
  var images      = { imageslist: [] };

  var source_2    = $("#overlays-template").html(); 
  var template_2  = Handlebars.compile(source_2);
  var overlays    = { overlayslist: [] };

  $('#texts').find('ul').append(template(texts));
  $('#images').find('ul').append(template_3(images));
  $('#overlays').find('ul').append(template_2(overlays));


  normalizeNinja( ninja );

  // load up texts
  for( var k in ninja.texts ) {
    ninja.texts[k].id = k;
    texts.textslist.push( ninja.texts[k] );
  }

  // load up images
  for( var k in ninja.images ) {
    ninja.images[k].id = k;
    images.imageslist.push( ninja.images[k] );

    var imageUrl = CWD + '/' + ninja.images[k].src;

    if (!UrlExists(imageUrl)) {
      ninja.images[k].error = 'error';
    }
  }

  // load up overlays
  for ( var k in ninja.overlays ) {
    ninja.overlays[k].id = k;
    overlays.overlayslist.push(ninja.overlays[k]);
  }

  var meta_form = $('#add_meta_form');

  meta_form.find('input#title').val(ninja.meta.title);
  meta_form.find('input#meta_desc').val(ninja.meta.description);

  var source = $("#texts-template").html(); 
  var template = Handlebars.compile(source);
  $('#texts').find('ul').append(template(texts));


  var source_3 = $("#images-template").html(); 
  var template_3 = Handlebars.compile(source_3);
  $('#images').find('ul').append(template_3(images));


  var source_2 = $("#overlays-template").html(); 
  var template_2 = Handlebars.compile(source_2);     
  $('#overlays').find('ul').append(template_2(overlays));

  

  /* Show details form */
  $('ul#texts-ul').on('click','.texts-row .btn_details', function(e) {

    var $this = $(this).closest('.texts-row');
    
    e.preventDefault();

    if(!$this.hasClass('active_details')) {
      $('.texts-row.active_details').find('.details_wrapper').slideUp('fast', function() {
        $(this).parent().removeClass('active_details');
      });

      $('.texts-row.active_effects').find('.effects_wrapper').slideUp('fast', function() {
        $(this).parent().removeClass('active_effects');
      });

      $this.addClass('active_details');
      $this.find('.effects_wrapper').slideUp('fast');
      $this.find('.details_wrapper').slideDown('fast');
    }
    else {
      $this.find('.details_wrapper').slideUp('fast');
      $this.removeClass('active_details');
    }
  });


  $('ul#images-ul').on('click','.images-row .btn_details', function(e) {

    e.preventDefault();

    var $this = $(this).closest('.images-row');

    if(!$this.hasClass('active_details')) {
      $('.images-row.active_details').find('.details_wrapper').slideUp('fast', function() {
        $(this).parent().removeClass('active_details');
      });

      $('.images-row.active_effects').find('.effects_wrapper').slideUp('fast', function() {
        $(this).parent().removeClass('active_effects');
      });

      $this.addClass('active_details');
      $this.find('.effects_wrapper').slideUp('fast');
      $this.find('.details_wrapper').slideDown('fast');
    }
    else {
      $this.find('.details_wrapper').slideUp('fast');
      $this.removeClass('active_details');
    }
  });


  /* Show effects form */
  $('ul#texts-ul').on('click','.texts-row .btn_effects', function(e) {

    e.preventDefault();

    var $this = $(this).closest('.texts-row');

    if(!$this.hasClass('active_effects')) {
      $('.texts-row.active_effects').find('.effects_wrapper').slideUp('fast', function() {
        $(this).parent().removeClass('active_effects');
      });

      $('.texts-row.active_details').find('.details_wrapper').slideUp('fast', function() {
        $(this).parent().removeClass('active_details');
      });

      $this.addClass('active_effects');
      $this.find('.details_wrapper').slideUp('fast');
      $this.find('.effects_wrapper').slideDown('fast');
    }
    else {
      $this.find('.effects_wrapper').slideUp('fast');
      $this.removeClass('active_effects');
    }
  });

  $('ul#images-ul').on('click','.images-row .btn_upload', function(e) {

    e.preventDefault();

    var $this = $(this).closest('.images-row');
    $this.find('input#deimage').trigger("click");
  });


  $('ul#images-ul').on('click','.images-row .btn_effects', function(e) {

    e.preventDefault();

    var $this = $(this).closest('.images-row');

    if(!$this.hasClass('active_effects')) {
      $('.images-row.active_effects').find('.effects_wrapper').slideUp('fast', function() {
        $(this).parent().removeClass('active_effects');
      });

      $('.images-row.active_details').find('.details_wrapper').slideUp('fast', function() {
        $(this).parent().removeClass('active_details');
      });

      $this.addClass('active_effects');
      $this.find('.details_wrapper').slideUp('fast');
      $this.find('.effects_wrapper').slideDown('fast');
    }
    else {
      $this.find('.effects_wrapper').slideUp('fast');
      $this.removeClass('active_effects');
    }
  });

  

  /* Add new actor  */
  $('#add_text_form').on('submit', function(e) {

    e.preventDefault();

    var default_effect  = effects.text_effects[0];
    var frame           = $('input#frame_rate').val();
    var text            = $('input#text').val();
    var id              = guid();

    //console.log( id , Object.keys( ninja.texts ).length );


    var new_data = { textslist: [
      {
        id: id,
        frame: frame,
        text: text,

        duration: default_effect.duration,
        opacity: default_effect.opacity,
        x: default_effect.x,
        y: default_effect.y,

        fontSize: default_effect.fontSize,
        center: default_effect.center,

        intro_fontSize: default_effect.intro_fontSize,
        intro_duration: default_effect.intro_duration,
        intro_easing: default_effect.intro_easing,
        intro_opacity: default_effect.intro_opacity,
        intro_x: default_effect.intro_x,
        intro_y: default_effect.intro_y,

        outro_fontSize: default_effect.outro_fontSize,
        outro_duration: default_effect.outro_duration,
        outro_easing: default_effect.outro_easing,
        outro_opacity: default_effect.outro_opacity,
        outro_x: default_effect.outro_x,
        outro_y: default_effect.outro_y
      }
    ]};

    $('#texts').find('ul#texts-ul').append(template(new_data));

    $('ul#texts-ul').find('.texts-row[data-id=' + id + '] .effect-button[data-id=0]').addClass('active');

    $('input#frame_rate').val('');
    $('input#text').val('');
    
    ninja.texts[id] = {
      "frame" : frame,
      "text" : text
    }
  });


  /* Add new actor  */
  $('#add_overlay_form').on('submit', function(e) {

    e.preventDefault();

    var id            = guid();
    var startFrame    = $('input#frame_from').val();
    var endFrame      = $('input#frame_to').val();
    var color         = $('select#color').val();
    var transparency  = $('input#transparency').val();

    var new_data = { overlayslist: [
      {
        id: id,
        startFrame: startFrame,
        endFrame: endFrame,
        color: color,
        transparency: transparency
      }
    ]};

    $('#overlays').find('ul#overlays-ul').append(template_2(new_data));

    $('input#frame_from').val('');
    $('input#frame_to').val('');
    $('input#transparency').val('');
    
    ninja.overlays[id] = {
      "startFrame" : startFrame,
      "endFrame" : endFrame,
      "color" : color,
      "transparency" : transparency
    }
  });


  /* Remove actor */
  $('ul#texts-ul').on('click', '.texts-row .btn_remove', function(e) {
    var $this = $(this);
    var row   = $this.closest('.texts-row');
    var id    = row.data("id");

    row.fadeOut(300, function() {
      row.parent().remove(); 
      delete ninja.texts[id];
    });
  });


  /* Remove overlay */
  $('ul#overlays-ul').on('click', '.overlay-row .btn_remove', function(e) {
    var $this = $(this);
    var row   = $this.closest('.overlay-row');
    var id    = row.data("id");

    row.fadeOut(300, function() {
      row.parent().remove(); 
      delete ninja.overlays[id];
    });
  });


  /* Remove image */
  $('ul#images-ul').on('click', '.images-row .btn_remove', function(e) {
    var $this = $(this);
    var row   = $this.closest('.images-row');
    var id    = row.data("id");

    row.fadeOut(300, function() {
      row.parent().remove(); 
      delete ninja.images[id];
    });
  });

  $('#calculate_frame_form').submit(function(e) {

    e.preventDefault();

    var $this     = $(this);
    var framerate = 25;
    var input_fr  = $this.find('input#calc_frames');
    var input_sec = $this.find('input#calc_sec');
    var frames    = input_fr.val();
    var seconds   = input_sec.val();

    $this.find('input#calc_frames').focus(function() {
      input_sec.val('');
    });

    input_sec.focus(function() {
      input_fr.val('');
    });

    if(frames.length > 0) {
      new_seconds = frames / 25;

      input_sec.val(new_seconds);
    }

    else if(seconds.length > 0) {
      new_frames = seconds * 25;

      input_fr.val(new_frames);
    }
  });


  $('ul#texts-ul').on('click','.effect-button', function(e) {

    e.preventDefault();

    var $this       = $(this);
    var effects_id  = $this.data('id');
    var row         = $this.closest('.texts-row');
    var effect      = effects.text_effects[effects_id];

    $('.effect-button.active').removeClass('active');
    $this.addClass('active');

    row.find('input#duration').val(effect.duration);
    row.find('input#opacity').val(effect.opacity);
    row.find('input#x').val(effect.x);
    row.find('input#y').val(effect.y);
    row.find('select#center').val(effect.center.toString());

    row.find('input#intro_duration').val(effect.intro_duration);
    row.find('input#intro_opacity').val(effect.intro_opacity);
    row.find('select#intro_easing').val(effect.intro_easing);
    row.find('input#intro_x').val(effect.intro_x);
    row.find('input#intro_y').val(effect.intro_y);

    row.find('input#outro_duration').val(effect.outro_duration);
    row.find('input#outro_opacity').val(effect.outro_opacity);
    row.find('select#outro_easing').val(effect.outro_easing);
    row.find('input#outro_x').val(effect.outro_x);
    row.find('input#outro_y').val(effect.outro_y);
  });

  $('ul#images-ul').on('click','.effect-button', function(e) {

    e.preventDefault();

    var $this       = $(this);
    var effects_id  = $this.data('id');
    var row         = $this.closest('.images-row');
    var effect      = effects.image_effects[effects_id];

    $('.effect-button.active').removeClass('active');
    $this.addClass('active');


    // Reset
    if( effects_id == 49 ) {

      row.find('input#opacity').val( 1 );
      row.find('input#intro_opacity').val( 1 );
      row.find('input#outro_opacity').val( 1 );

      row.find('input#duration').val( 3 );
      row.find('input#intro_duration').val( 0.5 );
      row.find('input#outro_duration').val( 0.5 );

      return;
    }

    // auto burns
    if( effects_id == 50 ) {

      var xdir = Math.floor( Math.random() * ( 1 - -2 + 1 ) + -2 );
      var ydir = Math.floor( Math.random() * ( 1 - -2 + 1 ) + -2 );

      row.find('input#outro_x').val( parseInt( row.find('input#x').val() ) + 50 * xdir );
      row.find('input#outro_y').val( parseInt( row.find('input#y').val() ) + 50 * ydir );

      row.find('input#outro_width').val( parseInt( row.find('input#width').val() ) + 100 );
      row.find('input#outro_height').val( parseInt( row.find('input#height').val() ) + 100 );

      row.find('input#duration').val( 0.01 );
      row.find('input#intro_duration').val( 0.01 );
      row.find('input#outro_duration').val( 4 );

      return;
    }


    // UP Burns
    if( effects_id == 52 ) {

      row.find('input#outro_x').val( parseInt( row.find('input#x').val() ) - 50 );
      row.find('input#outro_y').val( parseInt( row.find('input#y').val() ) - 150 );

      row.find('input#outro_width').val( parseInt( row.find('input#width').val() ) + 100 );
      row.find('input#outro_height').val( parseInt( row.find('input#height').val() ) + 100 );

      row.find('input#duration').val( 0.01 );
      row.find('input#intro_duration').val( 0.01 );
      row.find('input#outro_duration').val( 4 );

      return;
    }

    // UP Right Burns
    if( effects_id == 53 ) {

      row.find('input#outro_x').val( parseInt( row.find('input#x').val() ) + 50 );
      row.find('input#outro_y').val( parseInt( row.find('input#y').val() ) - 150 );

      row.find('input#outro_width').val( parseInt( row.find('input#width').val() ) + 100 );
      row.find('input#outro_height').val( parseInt( row.find('input#height').val() ) + 100 );

      row.find('input#duration').val( 0.01 );
      row.find('input#intro_duration').val( 0.01 );
      row.find('input#outro_duration').val( 4 );

      return;
    }

    // Right Burns
    if( effects_id == 54 ) {

      row.find('input#outro_x').val( parseInt( row.find('input#x').val() ) + 50 );
      row.find('input#outro_y').val( parseInt( row.find('input#y').val() ) - 50 );

      row.find('input#outro_width').val( parseInt( row.find('input#width').val() ) + 100 );
      row.find('input#outro_height').val( parseInt( row.find('input#height').val() ) + 100 );

      row.find('input#duration').val( 0.01 );
      row.find('input#intro_duration').val( 0.01 );
      row.find('input#outro_duration').val( 4 );

      return;
    }


    // Right Down Burns
    if( effects_id == 55 ) {

      row.find('input#outro_x').val( parseInt( row.find('input#x').val() ) + 50 );
      row.find('input#outro_y').val( parseInt( row.find('input#y').val() ) + 50 );

      row.find('input#outro_width').val( parseInt( row.find('input#width').val() ) + 100 );
      row.find('input#outro_height').val( parseInt( row.find('input#height').val() ) + 100 );

      row.find('input#duration').val( 0.01 );
      row.find('input#intro_duration').val( 0.01 );
      row.find('input#outro_duration').val( 4 );

      return;
    }

    // Down Burns
    if( effects_id == 56 ) {

      row.find('input#outro_x').val( parseInt( row.find('input#x').val() ) - 50 );
      row.find('input#outro_y').val( parseInt( row.find('input#y').val() ) + 50 );

      row.find('input#outro_width').val( parseInt( row.find('input#width').val() ) + 100 );
      row.find('input#outro_height').val( parseInt( row.find('input#height').val() ) + 100 );

      row.find('input#duration').val( 0.01 );
      row.find('input#intro_duration').val( 0.01 );
      row.find('input#outro_duration').val( 4 );

      return;
    }

    // Down Left Burns
    if( effects_id == 57 ) {

      row.find('input#outro_x').val( parseInt( row.find('input#x').val() ) - 150 );
      row.find('input#outro_y').val( parseInt( row.find('input#y').val() ) + 50 );

      row.find('input#outro_width').val( parseInt( row.find('input#width').val() ) + 100 );
      row.find('input#outro_height').val( parseInt( row.find('input#height').val() ) + 100 );

      row.find('input#duration').val( 0.01 );
      row.find('input#intro_duration').val( 0.01 );
      row.find('input#outro_duration').val( 4 );

      return;
    }


    // Left Burns
    if( effects_id == 58 ) {

      row.find('input#outro_x').val( parseInt( row.find('input#x').val() ) - 150 );
      row.find('input#outro_y').val( parseInt( row.find('input#y').val() ) - 50 );

      row.find('input#outro_width').val( parseInt( row.find('input#width').val() ) + 100 );
      row.find('input#outro_height').val( parseInt( row.find('input#height').val() ) + 100 );

      row.find('input#duration').val( 0.01 );
      row.find('input#intro_duration').val( 0.01 );
      row.find('input#outro_duration').val( 4 );

      return;
    }


    // Left UP Burns
    if( effects_id == 59 ) {

      row.find('input#outro_x').val( parseInt( row.find('input#x').val() ) - 150 );
      row.find('input#outro_y').val( parseInt( row.find('input#y').val() ) - 150 );

      row.find('input#outro_width').val( parseInt( row.find('input#width').val() ) + 100 );
      row.find('input#outro_height').val( parseInt( row.find('input#height').val() ) + 100 );

      row.find('input#duration').val( 0.01 );
      row.find('input#intro_duration').val( 0.01 );
      row.find('input#outro_duration').val( 4 );

      return;
    }


    if( effects_id == 51 ) {

      row.find('input#opacity').val( 0 );
      row.find('input#intro_opacity').val( 1 );
      row.find('input#outro_opacity').val( 0 );

      row.find('input#duration').val( 3 );
      row.find('input#intro_duration').val( 0.5 );
      row.find('input#outro_duration').val( 0.5 );

      return;
    }

    // Move up 10
    if( effects_id == 70 ) {

      row.find('input#y').val( parseInt( row.find('input#y').val() ) - 10 );
      row.find('input#intro_y').val( parseInt( row.find('input#intro_y').val() ) - 10 );
      row.find('input#outro_y').val( parseInt( row.find('input#outro_y').val() ) - 10 );

      return;
    }

    // Move down 10
    if( effects_id == 71 ) {

      row.find('input#y').val( parseInt( row.find('input#y').val() ) + 10 );
      row.find('input#intro_y').val( parseInt( row.find('input#intro_y').val() ) + 10 );
      row.find('input#outro_y').val( parseInt( row.find('input#outro_y').val() ) + 10 );

      return;
    }

    // Move left 10
    if( effects_id == 72 ) {

      row.find('input#x').val( parseInt( row.find('input#x').val() ) - 10 );
      row.find('input#intro_x').val( parseInt( row.find('input#intro_x').val() ) - 10 );
      row.find('input#outro_x').val( parseInt( row.find('input#outro_x').val() ) - 10 );

      return;
    }

    // Move right 10
    if( effects_id == 73 ) {

      row.find('input#x').val( parseInt( row.find('input#x').val() ) + 10 );
      row.find('input#intro_x').val( parseInt( row.find('input#intro_x').val() ) + 10 );
      row.find('input#outro_x').val( parseInt( row.find('input#outro_x').val() ) + 10 );

      return;
    }

    // zoom in 10
    if( effects_id == 74 ) {

      var ratio = parseInt( row.find('input#height').val() ) / parseInt( row.find('input#width').val() );

      row.find('input#x').val( parseInt( row.find('input#x').val() ) - 10 );
      row.find('input#intro_x').val( parseInt( row.find('input#intro_x').val() ) - 10 );
      row.find('input#outro_x').val( parseInt( row.find('input#outro_x').val() ) - 10 );

      row.find('input#y').val( parseInt( row.find('input#y').val() ) - 10 * ratio );
      row.find('input#intro_y').val( parseInt( row.find('input#intro_y').val() ) - 10 * ratio );
      row.find('input#outro_y').val( parseInt( row.find('input#outro_y').val() ) - 10 * ratio );

      row.find('input#width').val( parseInt( row.find('input#width').val() ) + 20 );
      row.find('input#height').val( parseInt( row.find('input#height').val() ) + 20 * ratio );

      row.find('input#intro_width').val( parseInt( row.find('input#intro_width').val() ) + 20 );
      row.find('input#intro_height').val( parseInt( row.find('input#intro_height').val() ) + 20 * ratio );

      row.find('input#outro_width').val( parseInt( row.find('input#outro_width').val() ) + 20 );
      row.find('input#outro_height').val( parseInt( row.find('input#outro_height').val() ) + 20 * ratio );

      return;
    }

    // zoom out 10
    if( effects_id == 75 ) {

      var ratio = parseInt( row.find('input#height').val() ) / parseInt( row.find('input#width').val() );

      row.find('input#x').val( parseInt( row.find('input#x').val() ) + 10 );
      row.find('input#intro_x').val( parseInt( row.find('input#intro_x').val() ) + 10 );
      row.find('input#outro_x').val( parseInt( row.find('input#outro_x').val() ) + 10 );

      row.find('input#y').val( parseInt( row.find('input#y').val() ) + 10 * ratio );
      row.find('input#intro_y').val( parseInt( row.find('input#intro_y').val() ) + 10 * ratio );
      row.find('input#outro_y').val( parseInt( row.find('input#outro_y').val() ) + 10 * ratio );

      row.find('input#width').val( parseInt( row.find('input#width').val() ) - 20 );
      row.find('input#height').val( parseInt( row.find('input#height').val() ) - 20 * ratio );

      row.find('input#intro_width').val( parseInt( row.find('input#intro_width').val() ) - 20 );
      row.find('input#intro_height').val( parseInt( row.find('input#intro_height').val() ) - 20 * ratio );

      row.find('input#outro_width').val( parseInt( row.find('input#outro_width').val() ) - 20 );
      row.find('input#outro_height').val( parseInt( row.find('input#outro_height').val() ) - 20 * ratio );

      return;
    }


    row.find('input#duration').val(effect.duration);
    row.find('input#opacity').val(effect.opacity);
    row.find('input#x').val(effect.x);
    row.find('input#y').val(effect.y);
    row.find('input#width').val(effect.width);
    row.find('input#height').val(effect.height);

    row.find('input#intro_duration').val(effect.intro_duration);
    row.find('input#intro_opacity').val(effect.intro_opacity);
    row.find('input#intro_x').val(effect.intro_x);
    row.find('input#intro_y').val(effect.intro_y);
    row.find('input#intro_width').val(effect.intro_width);
    row.find('input#intro_height').val(effect.intro_height)

    row.find('input#outro_duration').val(effect.outro_duration);
    row.find('input#outro_opacity').val(effect.outro_opacity);
    row.find('input#outro_x').val(effect.outro_x);
    row.find('input#outro_y').val(effect.outro_y);
    row.find('input#outro_width').val(effect.outro_width);
    row.find('input#outro_height').val(effect.outro_height)
  });
  

  $("#download").click( function( e ) {

    e.preventDefault();

    // SAVE ALL THE FORMS TO THE JSON CUES.....
    ninja = buildNinjaFromForms();
    normalizeNinja( ninja );


    var data = {};
    data.ajax = "saveNinja";
    data.ninja = JSON.stringify( ninja );

    $.post( document.location.href , data , function( response ) {

      if( response == "OK" ) {
        document.location.href = "?page=downloadjson";
      }
    });
  });


  $("#render").click( function( e ) {

    // SAVE ALL THE FORMS TO THE JSON CUES.....
    pause();
    ninja = buildNinjaFromForms();
    normalizeNinja( ninja );

    var data = {};
    data.ajax = "saveNinja";
    data.ninja = JSON.stringify( ninja );

    var meta_form   = $('#add_meta_form');
    var meta_title  = meta_form.find('input#title').val();

    if(meta_title.length > 0) {

      $.post( document.location.href , data , function( response ) {

        if( response == "OK" ) {
          document.location.href = "?page=render";
        }
      });
    } else {
      var error_wrapper = $('.error_message');

      error_wrapper.find('p').html("Please provide a meta title.");
      error_wrapper.slideDown(750, function() {
        error_wrapper.delay(3000).slideUp(500);
      });
    }
  });

  $("#pnp").click( function(e) {
    var $this = $(this);

    $this.toggleClass("play");
    
    if($this.hasClass("play")) {
      $this.html('Pause');
      play();
    } 
    else {
      $this.html('Play');
      pause();
    }
  });

  $("#restart").click( function(e) {
    restart();
  });

  $('.kineticjs-content').mouseenter(function(e) {
    $('#coord_tooltip').css('display', 'block');
  });

  $('.kineticjs-content').mouseout(function(e) {
    $('#coord_tooltip').css('display', 'none');
  });

  $('.kineticjs-content').mousemove(function(e) {

    var tooltip   = $('#coord_tooltip');
    var offset    = $(this).offset();
    var cursor_x  = e.pageX + 15;
    var cursor_y  = e.pageY + 15;
    var x_coord   = (e.pageX - offset.left) * 2;
    var y_coord   = (e.pageY - offset.top) * 2;

    tooltip.find('span.x').html(Math.floor(x_coord))
    tooltip.find('span.y').html(Math.floor(y_coord));

    tooltip.css('top', cursor_y);
    tooltip.css('left', cursor_x);
  });

  $('.kineticjs-content').on('click', function(e) {
      
    var offset      = $(this).offset();
    var x_coord     = (e.pageX - offset.left) * 2;
    var y_coord     = (e.pageY - offset.top) * 2;

    $('#current-x span').html(Math.floor(x_coord));
    $('#current-y span').html(Math.floor(y_coord));

    $('#current-x').addClass('active');
    $('#current-y').addClass('active');
    $('#video_info a').addClass('active');

  });

  $('.fileUpload2').fileupload({

    add : function( e , data ) {

      var frameregex = new RegExp( /^[0-9]+$/ );

      if( frameregex.test( $('#add_image_form input#frame').val() ) ) {
        var jqXHR = data.submit();
      }
    },


    done : function( e , data ) {

      if( data.result != "NOK" ) {

        var r               = $.parseJSON( data.result );
        var default_effect  = effects.image_effects[0];
        var frame           = $('#add_image_form input#frame').val();
        var id              = guid();
        var description     = $('#add_image_form input#id').val();
        
        var new_data = { imageslist: [{
          "frame" : frame,
          "id" : id,
          "description" : description,
          "src" : r[4],

          "x" : -1 * ( ( r[0] - 1280 ) / 2 ),
          "y" : -1 * ( ( r[1] - 720 ) / 2 ),
          "width" : r[0],
          "height" : r[1],
          "duration" : default_effect.duration,
          "opacity" : default_effect.opacity,

          "intro_x" : -1 * ( ( r[0] - 1280 ) / 2 ),
          "intro_y" : -1 * ( ( r[1] - 720 ) / 2 ),
          "intro_width" : r[0],
          "intro_height" : r[1],
          "intro_duration" : default_effect.intro_duration,
          "intro_opacity" : default_effect.intro_opacity,
          "intro_easing" : default_effect.intro_easing,

          "outro_x" : -1 * ( ( r[0] - 1280 ) / 2 ),
          "outro_y" : -1 * ( ( r[1] - 720 ) / 2 ),
          "outro_width" : r[0],
          "outro_height" : r[1],
          "outro_duration" : default_effect.outro_duration,
          "outro_opacity" : default_effect.outro_opacity,
          "outro_easing" : default_effect.outro_easing
        }]};


        $('#images').find('ul#images-ul').append(template_3( new_data ));

        $('ul#images-ul').find('.images-row[data-id=' + id + '] .effect-button[data-id=0]').addClass('active');

        $('#add_image_form input#frame').val('');
        $('#add_image_form input#id').val('');
        
        var ti = new Image();
        ti.onload = function() {
          videoimages[id] = ti;
          videoimagesready++;

          ninja.images[id] = {
            "frame" : frame,
            "id" : id,
            "description" : description,
            "src" : r[4],
            "x" : default_effect.x,
            "y" : default_effect.y,
            "width" : default_effect.width,
            "height" : default_effect.height,
            "duration" : default_effect.duration,
            "opacity" : default_effect.opacity,

            "intro_x" : default_effect.intro_x,
            "intro_y" : default_effect.intro_y,
            "intro_width" : default_effect.intro_width,
            "intro_height" : default_effect.intro_height,
            "intro_duration" : default_effect.intro_duration,
            "intro_opacity" : default_effect.intro_opacity,
            "intro_easing" : default_effect.intro_easing,

            "outro_x" : default_effect.outro_x,
            "outro_y" : default_effect.outro_y,
            "outro_width" : default_effect.outro_width,
            "outro_height" : default_effect.outro_height,
            "outro_duration" : default_effect.outro_duration,
            "outro_opacity" : default_effect.outro_opacity,
            "outro_easing" : default_effect.outro_easing
          }

          normalizeNinja( ninja );
        };

        ti.src = CWD + "/" + r[4];
      }
    }
  });



  $('.fileUpload3').fileupload({

    add : function( e , data ) {

      var frameregex = new RegExp( /^[0-9]+$/ );

      if( frameregex.test( $('.images_details_form input#frame').val() ) ) {
        var jqXHR = data.submit();
      }
    },


    done : function( e , data ) {

      if( data.result != "NOK" ) {

        var r = $.parseJSON( data.result );
        
        var ti = new Image();
        ti.onload = function() {
         
          
          var tid = data.id;

          //console.log( tid );

          $('.images_details_form[data-id="'+tid+'"]').find('span.source').html('['+r[4]+']');

          $('.images_details_form[data-id="'+tid+'"]').find('li').removeClass('error');

          audioLoaded();
          
        };

        ti.src = CWD + "/" + r[4];
      }
    }
  });


  $('#video_info a').on('click', function(e) {

    e.preventDefault();

    var x_coord     = $('#current-x span').html();
    var y_coord     = $('#current-y span').html();
    var active_row  = $("ul#texts-ul li.texts-row.active_details");

    if(active_row.find('.initial_tab').hasClass('active')) {
      active_row.find('input#x').val(x_coord);
      active_row.find('input#y').val(y_coord);
    }
    else if(active_row.find('.intro_tab').hasClass('active')) {
      active_row.find('input#intro_x').val(x_coord);
      active_row.find('input#intro_y').val(y_coord);
    }
    else if(active_row.find('.outro_tab').hasClass('active')) {
      active_row.find('input#outro_x').val(x_coord);
      active_row.find('input#outro_y').val(y_coord);
    }
  });
});


function play() {
  audio.play();
  anim.start();
  $('#current-frame').removeClass('active');
}


function pause() {
  audio.pause();
  anim.stop();
  $('#current-frame span').html(Math.round(currentFrame));
  $('#current-frame').addClass('active');

}


function restart() {
  
  anim.stop();
  audio.pause();

  currentFrame = 0;
  frameNum = 0;
  currentTime = 0;
  audio.currentTime = 0;
  
  ninja = buildNinjaFromForms();
  normalizeNinja( ninja );

  $("#pnp").addClass( "play" );
  $("#pnp").html('Pause');
  $('#current-frame span').html(0);
  $('#current-frame').removeClass('active');

  play();

}


function buildNinjaFromForms() {

  var data = {};
  
  data.meta     = $( "#add_meta_form" ).serializeObject();
  data.texts    = {};
  data.overlays = {};
  data.images   = {};

  $("form.texts_details_form").each( function( index ) {
    var $this       = $(this);
    var id          = $this.attr( 'id' ).replace( "texts_details_form_" , "" );
    
    data.texts[id]  = $this.serializeObject();
  });

  $("form.images_details_form").each( function( index ) {
    var $this       = $(this);
    var id          = $this.attr( 'id' ).replace( "images_details_form_" , "" );

    data.images[id] = $this.serializeObject();
  });

  $("form.overlays_details_form").each( function( index ) { 
    var $this         = $(this);
    var id            = $this.attr( 'id' ).replace( "overlays_details_form_" , "" );
    
    data.overlays[id] = $this.serializeObject();
  });

  return data;
}
