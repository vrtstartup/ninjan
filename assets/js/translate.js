$.event.special.tripleclick = {
 
      setup: function(data, namespaces) {
          var elem = this, $elem = jQuery(elem);
          $elem.bind('click', jQuery.event.special.tripleclick.handler);
      },
   
      teardown: function(namespaces) {
          var elem = this, $elem = jQuery(elem);
          $elem.unbind('click', jQuery.event.special.tripleclick.handler)
      },
   
      handler: function(event) {
          var elem = this, $elem = jQuery(elem), clicks = $elem.data('clicks') || 0;
          clicks += 1;
          
          setTimeout( function() {
            clicks=0;
            $elem.data('clicks',0);
          } , 500 );

          if ( clicks === 3 ) {
              clicks = 0;
   
              // set event type to "tripleclick"
              event.type = "tripleclick";
   
              // let jQuery handle the triggering of "tripleclick" event handlers
              $elem.trigger('tripleclick');
          }
          $elem.data('clicks', clicks);
      }
   
  };



$(document).ready(function(){


  



  $("translate").bind("tripleclick", function() {
    // do something

    window.open( "./admin/?module=texts&action=update&text_key=" + $(this).attr( 'data-key' ) + "&l=" + $(this).attr( 'data-language' ) , 'admin' );

  });

  


});