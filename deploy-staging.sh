#!/bin/bash

./getdb-staging.sh

export dir=`dirname $0`
cd $dir
echo `pwd`

git config git-ftp.user webftp
git config git-ftp.password vrt.startup
git config git-ftp.url 54.194.194.175/ninjamaker
git config git-ftp.syncroot ./

git config --global push.default simple
git config --global user.name "VRT"
git config --global user.email "noreply@vrt.be"

git commit -a -m "Deployment Commit"
git push

git pull
git ftp push
