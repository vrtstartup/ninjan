<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../assets/ico/favicon.png">

    <title>404 Not Found</title>

    <!-- Bootstrap core CSS -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../assets/css/sticky-footer.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript
    ================================================== -->
    
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>

    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->

     <style>
      body {
        padding-top: 50px;
      }
      
    </style>

  </head>

  <body>

    <div id="wrap">

      <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="http://www.github.com/brownrl/skeleton">Skeleton</a>
          </div>
          <div class="collapse navbar-collapse">

            <ul class="nav navbar-nav">
              <li>
                <a href="../">Go To Main Site</a>
            </ul>

          </div><!--/.nav-collapse -->

        </div>
      </div>

      <div class="container">
      
        <h1>Not Found</h1>
        
        <p>
          Oh brother, this is embarrasing. It appears that you are looking for something that is not found.
          Even worse it could be a link that was misspelled or no longer exists.
        </p>

        <p>
          1000030487 Apologies.
        </p>

        <p class="text-warning">
          Please contact the "makers" of this website and let them know how you got here. Let them know
          what you did or what you clicked before you got here. A screenshot of this page is completely
          uneccessary. Just let them know that you got a "404" and the page or site that you came from.
        </p>

        <p>
          It looks like you came from: ( not always known )
        </p>
        <pre>
          
Referred here by: 

<script>
document.write(  document.referrer );
</script>
        </pre>

        <p>
          <a href="javascript:history.back();" class="btn btn-primary">Go Back In Your Browser's History?</a>
        </p>

      </div><!-- /.container -->
    </div>

    <div id="footer">
      <div class="container">
        <p class="text-muted credit">Website by <a href="#">Designer</a> and <a href="#">Developer</a>.</p>
      </div>
    </div>


  </body>
</html>