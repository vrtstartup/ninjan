<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../assets/ico/favicon.png">

    <title>Permissions</title>

    <!-- Bootstrap core CSS -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../assets/css/sticky-footer.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript
    ================================================== -->
    
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>

    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->

     <style>
      body {
        padding-top: 50px;
      }
      
    </style>

  </head>

  <body>

    <div id="wrap">

      <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="http://www.github.com/brownrl/skeleton">Skeleton</a>
          </div>
          <div class="collapse navbar-collapse">

            <ul class="nav navbar-nav">
              <li>
                <a href="../">Try Again</a>
            </ul>

          </div><!--/.nav-collapse -->

        </div>
      </div>

      <div class="container">
      
        <h1>Permissions Error</h1>
        
        <p>
          In order for this website/project to run some files/directories on the server need to have correct permissions. 
        </p>

        <p>
          Case in point,<br />
          <br />
          If you had a website that allowed people to upload a picture, you would need a directory that the uploaded
          pictures could go...
        </p>

        <p>
          For skeleton to function properly the following need to be writable to the web server on this machine:
        </p>

        <pre>

          inc/smarty/cache
          inc/smarty/templates_c
          db/
          db/database.sqlite
          uploads/
          uploads/*
        </pre>

        <p>
          You can manually set the permissions on these directories and files yourself or 
          you can easily make all of these directories and files writable by running the bash shell script:
        </p>

        <pre>

          ./skeleton perms
        </pre>

        <p>
          While inside the main skeleton directory.
        </p>

        <p class="text-warning">
          If anything in the above does not make sense to you, that's ok. Please find an appropriate
          tech person and let them know this is the "screen" you are seeing. In my experience it is
          always been best if you copy and paste the URL above from the browser and send that in an 
          email message. In general the tech person you contact should actually be paid to help you.
        </p>

        <p>Reason</p>
        <pre>
          <?php echo ( isset( $_GET['reason'] ) ) ? $_GET['reason'] : "unknown"; ?>
        </pre>

      </div><!-- /.container -->
    </div>

    <div id="footer">
      <div class="container">
        <p class="text-muted credit">Website by <a href="#">Designer</a> and <a href="#">Developer</a>.</p>
      </div>
    </div>


  </body>
</html>