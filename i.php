<?php

  if ($_GET['i']) {
    require_once 'inc/functions/function.imageResize.php';
    
    $_GET['i'] = str_replace("..", "", $_GET['i']);
    $_GET['w'] = intval( $_GET['w'] );
    $_GET['h'] = intval( $_GET['h'] );
    

    $crop = true;
    if( isset( $_GET['crop'] ) && $_GET['crop'] == 'false') {
      $crop = false;
    }
    
    if ( file_exists( $_GET['i'] ) ) {
      imageResize( $_GET['i'] , $_GET['w'] , $_GET['h'], 70, $crop );
    } else {
      imageResize( "assets/img/1x1-pixel.png" , $_GET['w'] , $_GET['h'], 70, $crop );
    }
  }
