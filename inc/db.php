<?php


/**

  This is not a true Database abstraction at all. It is a database wrapping.
  The functions here are simply for making life easier and this is not a text
  book excercise in theoretical database abstraction.

  Put data in the DB get data out of the DB.

  Make wrapper functions to help speed things along.

  Here we use PDO which is a DB abstraction and yes your DB should be able to plug in here.

**/

/**
         _ _ _                  _            _   _ _       _           
 ___ ___| |_| |_ ___    ___ _ _| |___ ___   | |_|_| |_ ___| |_ ___ ___ 
|_ -| . | | |  _| -_|  |  _| | | | -_|_ -|  | . | |  _|  _|   | -_|_ -|
|___|_  |_|_|_| |___|  |_| |___|_|___|___|  |___|_|_| |___|_|_|___|___|
      |_|
**/


// Get the DB either here or one dir up.
// This means we can use the db in the front site
// or in the "admin/" which might be useful.



$DB = 'db/database.sqlite';
if ( strstr( $_SERVER['PHP_SELF'] , "admin/" ) ) {
  $DB = '../db/database.sqlite';
}


if ( ! is_writeable( dirname($DB) ) ) {
  if( ADMIN ) {
    header( "Location: ../errors/permissions.php?reason=".dirname($DB) );
    exit( 0 );
  }
  header( "Location: errors/permissions.php?reason=".dirname($DB) );
  exit( 0 );
}

if ( file_exists( $DB ) && ! is_writeable($DB) ) {
  
  if( ADMIN ) {
    header( "Location: ../errors/permissions.php?reason=".$DB );
    exit( 0 );
  }

  header( "Location: errors/permissions.php?reason=".$DB );
  exit( 0 );
}

$dbhandle = new PDO('sqlite:'.$DB);
if (! $dbhandle) { die( "CAN'T GET OR MAKE THE DB FOR PDO, This is a major problem." ); }


// YES WE GOT A DB!

// Here we go....

$sqlite_data_types = array();
$sqlite_data_types[] = "NULL";
$sqlite_data_types[] = "INTEGER";
$sqlite_data_types[] = "TEXT";
$sqlite_data_types[] = "REAL";
$sqlite_data_types[] = "BLOB";

$sqlite_nulls = array();
$sqlite_nulls[] = "NULL";
$sqlite_nulls[] = "NOT NULL";





// Ok this point we have a DB.
// Now we create some base funcitons
// We probably could use some pre built API and be fancy
// or just a few helpful functions to get us through the day.

// Create a query function
function dbQuery( $q )
{
  global $DB;
  global $dbhandle;

  try {
    $r = $dbhandle->query( $q );
  } catch ( PDOException $e ) {
    die( $e->getMessage() );
  }

  if (! $r) {
    $mesg = "";
    $mesg .= "<pre>";
    $mesg .= "Database Query Error: $DB\n\n";
    $mesg .= "Query: " . $q . "\n\n";
    $mesg .= "Debug BackTrace: " . print_r( debug_backtrace() , true ) . "\n\n";
    $mesg .= "Result: " . print_r( $r , true ) . "\n\n";
    $mesg .= "</pre>";
    die( $mesg );

  }

  return $r;

}

/**
 * Return a blank array based on a table.
 * Helpful if you need and prefilled array
 */
function dbBlankArray($table)
{

  // We fill $_POST with blank values for all the fields in this DB
  $r = array();
  $fields = dbFields($table);
  foreach ($fields as $field) {
    $r[$field] = "";
  }

}

/**
 * Get the last inserted ID
 */
function dbLastInsertId()
{
  global $dbhandle;

  return $dbhandle->lastInsertId();
}

/**
 * Escape a String
 */
function dbEscapeString($str)
{
  global $dbhandle;

  return $dbhandle->quote( $str );
}

// Function: Insert From Vals
// Take an associative array and build an insert statement
//
// $table -> the table you want to fill
// $prefix -> the prefix of the fields ( ie, auto_color -> 'auto_' )
// $vals -> the array to insert, default _POST
//
// Please note that this will work with normal database naming and not with
// special names with spaces and accents and odd stuff
//
//
function dbInsertFromVals( $table , $vals = null )
{

  $dbfields = dbFields( $table );

  $fields = array();
  $values = array();

  if ( is_null( $vals ) ) {
    $vals = $_POST;
  }

  foreach ($vals as $k => $v) {
    if ( in_array( $k , $dbfields ) ) {
      $fields[] = dbEscapeString( $k );
      $values[] = dbEscapeString( $v );
    }
  }
  $fields = join( "," , $fields );
  $values = "" . join(  ", " , $values ) ."";

  $q = "INSERT INTO ".$table." (".$fields.") VALUES (".$values.")";

  return $q;
}

/**
 * Create an update statement based the table, prefix, key vals.
 */
function dbUpdateFromVals( $table , $key , $value , $vals = null )
{
  $dbfields = dbFields( $table );
  $fields = array();

  if ( is_null( $vals ) ) {
    $vals = $_POST;
  }

    // loops through the vals and build it up
  foreach ($vals as $k => $v) {
    // do we have the right prefix?
    if ( in_array( $k , $dbfields ) ) {
      $fields[] = dbEscapeString( $k ) . " = ".dbEscapeString( $v )."";
    }
  }

  // remove the last ", "
  $fields = join( ", " , $fields );

    // construct the query in full
  $q = "UPDATE ".$table." SET ".$fields." WHERE ".$key." = '".$value."'";

  return $q;
}

/**
 * Count the number of rows in the table.
 */
function dbTableRowCount( $table )
{
  if ( dbTableExists($table) ) {
    $q = "SELECT COUNT( * ) AS total FROM ".$table;
    $qr = dbQuery($q);
    $qrow = dbFetch($qr);

    return intval( $qrow['total'] );

  } else {
    return 0;
  }
}

/**
* Get the whole table
*/
function dbFetchTable( $table ) {
  if ( dbTableExists($table) ) {
    $q = "SELECT * FROM ".$table;
    $qr = dbQuery( $q );
    return dbFetchAll( $qr );
  } else {
    return 0;
  }
}


/** 
 * Get the whole table JSON
 */
function dbFetchTableJson( $table ) {
  return json_encode( dbFetchTable( $table ) );
}


 /**
  * Get an associative array from a result.
  */
function dbFetch($qr)
{
  return $qr->fetch(PDO::FETCH_ASSOC);
}

/**
 * Get one value for one field in one table based on key val.
 */
function dbFetchOneValue($table, $field, $key, $value)
{

  $value = dbEscapeString($value);

  $q = "SELECT ".$field." AS val FROM ".$table." WHERE ".$key." = ".$value." LIMIT 1";
  $qr = dbQuery($q);
  $qrow = dbFetch($qr);

  //die( print_r( $qrow , true ) );
  return $qrow['val'];
}

/**
 * Get a record from the table.
 */
function dbFetchOneRow($table, $key, $value)
{
  //$table = dbEscapeString($table);
  //$key = dbEscapeString($key);
  $value = dbEscapeString($value);

  $q = "SELECT * FROM ".$table." WHERE ".$key." = ".$value." LIMIT 1";
  $qr = dbQuery($q);
  $qrow = dbFetch($qr);

  return $qrow;

}

/**
 * Get one as a json string
 */
function dbFetchOneRowJson($table, $key, $value) {
  return json_encode( dbFetchOneRow( $table , $key , $value ) );
}



/**
 * Return all the rows in a result set as array.
 */
function dbFetchAll($qr)
{
  $r = array();
  while ($qrow = dbFetch($qr)) {
    $r[] = $qrow;
  }

  return $r;
}

/**
 * Return all the row as a json array.
 */
function dbFetchAllJson( $qr ) {
  return json_encode( dbFetchAll( $qr ) );
}

/**
 * Get an array of all the tables.
 */
function dbTables()
{
  $tables = array();
  $q = "SELECT name FROM sqlite_master WHERE type = 'table'";
  $qr = dbQuery($q);
  $rows = dbFetchAll($qr);
  if (count($rows)) {
    foreach ($rows as $row) {
      $tables[] = $row['name'];
    }
  }

  return $tables;
}

/**
 * Does this table exist?
 */
function dbTableExists($table)
{
  $tables = dbTables();

  return in_array($table, $tables);
}

/**
 * Create a new table with an id and key field.
 */
function dbCreateTable($table, $prefix)
{
  if (!preg_match("/^[a-z][a-z0-9_]+[a-z0-9]$/", $table)) {
    die("DB ERROR: You are trying to create a table with an illegal name: '".$table."'");
  }

  if (!preg_match("/^[a-z][a-z0-9_]*[a-z0-9]$/", $prefix)) {
    die("DB ERROR: You are trying to create a table with an illegal field prefix: '".$prefix."'");
  }

  if (!dbTableExists($table)) {
    $q = "CREATE TABLE ".$table." (".$prefix."_id integer NOT NULL PRIMARY KEY, ".$prefix."_key text NOT NULL".")";
    $qr = dbQuery($q);
  }
}

/**
 * Give an array of all the fields in a table.
 */
function dbFields($table)
{
  $fields = array();
  $q = "PRAGMA table_info(".dbEscapeString($table).")";
  $qr = dbQuery($q);
  while ($qrow = dbFetch($qr)) {
    $fields[] = $qrow['name'];
  }

  return $fields;
}

/**
 * Does this table have this field?
 */
function dbFieldExists($field, $table)
{
  $fields = dbFields($table);

  return in_array($field, $fields);
}

/**
 * Create a new field. $nl is the NULL or NOT NULL part.
 *
 * Because this is sqlite2 this is a CRAZY function.
 *
 * Creating a temp table, copying, merging, deleting.
 * It's here.
 */
function dbCreateField($field, $type, $nl, $prefix, $table)
{

  global $sqlite_data_types;
  global $sqlite_nulls;


  if (!preg_match("/^[a-z][a-z0-9_]*[a-z0-9]$/", $field)) {
    die("DB ERROR: You are trying to create a field with an illegal name prefix: '".$field."'");
  }


  if( ! in_array(strtoupper( $type ), $sqlite_data_types) ) {
    die( "DB ERROR: Trying to create a field of a non existing type." ); 
  }


  if( ! in_array(strtoupper( $nl ), $sqlite_nulls) ) {
    die( "DB ERROR: Trying to create a field: $field and NULL status: $nl not set correctly." ); 
  }


  dbCreateTable($table, $prefix);

  $new_table = $table."_tmp";
  $full_field = $prefix."_".$field;

  if (!dbFieldExists($full_field, $table)) {
    // Get the sql of the current table
    $q = dbFetchOneValue("sqlite_master", "sql", "tbl_name", $table);

    //die( $q );
    // Make a temp table with the new field.
    $q = preg_replace("/TABLE ".$table." \(/", "TABLE ".$new_table." (", $q);
    $q = preg_replace("/\)/",",\n\t\"".$full_field."\" ".$type." ".$nl." \n) ", $q);

    //die( $q );

    dbQuery( $q );

    // Get the old fields
    $old_fields = dbFields($table);

    // Copy over the data
    $q = "INSERT INTO ".$new_table." SELECT ".join(",",$old_fields).",'' FROM ".$table;
    $qr = dbQuery($q);

    // Drop the original table
    $q = "DROP TABLE ".$table;
    $qr = dbQuery($q);

    // Get the sql of the new table.
    $q = dbFetchOneValue("sqlite_master", "sql", "tbl_name", $new_table);

    // Make the old table again.
    $q = preg_replace("/TABLE ".$new_table." \(/", "TABLE ".$table." (", $q);
    $qr = dbQuery($q);

    // Copy over the data
    $q = "INSERT INTO ".$table." SELECT * FROM ".$new_table;
    $qr = dbQuery($q);

    // Drop the temp table
    $q = "DROP TABLE ".$new_table;
    $qr = dbQuery($q);

  }
}

/**
 * Reorder the table by setting the weight to multiples of ten.
 */
function dbReorderTable( $table , $key_field, $weight_field )
{
  $q = "SELECT ".$key_field." FROM ".$table." ORDER BY ".$weight_field." ASC";
  $qr = dbQuery($q);

  $i = 0;
  while ( $qrow = dbFetch($qr) ) {
    $q2 = "UPDATE ".$table." SET ".$weight_field." = ". 10 * $i." WHERE ".$key_field." = ".$qrow[$key_field];
    $qr2 = dbQuery($q2);
    $i++;
  }
}

/**
 * Subtract 15 from the weight and reorder
 */
function dbMoveUp( $table , $key_field , $value , $weight_field )
{
  $q = "UPDATE ".$table." SET ".$weight_field." = ".$weight_field." - 15 WHERE ".$key_field." = ".$value;
  $qr = dbQuery($q);
  dbReorderTable($table, $key_field, $weight_field);
}


/**
 * Add 15 from the weight and reorder
 */
function dbMoveDown( $table , $key_field , $value , $weight_field )
{
  $q = "UPDATE ".$table." SET ".$weight_field." = ".$weight_field." + 15 WHERE ".$key_field." = ".$value;
  $qr = dbQuery($q);
  dbReorderTable($table, $key_field, $weight_field);
}


/**
 * Reorder the table by setting the weight to multiples of ten. Within a grouping
 */
function dbReorderTableGroup( $table , $key_field, $weight_field, $group_field, $group_value )
{
  $q = "SELECT ".$key_field." FROM ".$table." WHERE ".$group_field." = '".$group_value."' ORDER BY ".$weight_field." ASC";
  $qr = dbQuery($q);

  $i = 0;
  while ( $qrow = dbFetch($qr) ) {
    $q2 = "UPDATE ".$table." SET ".$weight_field." = ". 10 * $i." WHERE ".$key_field." = ".$qrow[$key_field];
    $qr2 = dbQuery($q2);
    $i++;
  }
}

/**
 * Subtract 15 from the weight and reorder by grouping
 */
function dbMoveUpGroup( $table , $key_field , $value , $weight_field, $group_field )
{
  $q = "UPDATE ".$table." SET ".$weight_field." = ".$weight_field." - 15 WHERE ".$key_field." = ".$value;
  $qr = dbQuery($q);

  $group_value = dbFetchOneValue($table, $group_field, $key_field, $value);

  dbReorderTableGroup($table, $key_field, $weight_field, $group_field, $group_value);
}


/**
 * Add 15 from the weight and reorder by grouping
 */
function dbMoveDownGroup( $table , $key_field , $value , $weight_field , $group_field )
{
  $q = "UPDATE ".$table." SET ".$weight_field." = ".$weight_field." + 15 WHERE ".$key_field." = ".$value;
  $qr = dbQuery($q);

  $group_value = dbFetchOneValue($table, $group_field, $key_field, $value);

  dbReorderTableGroup($table, $key_field, $weight_field, $group_field, $group_value);
}

/**
 * Says whether or not a value is already used.
 * Handy if you want to make sure that a login is unique
 */
function dbIsFree( $table , $field , $value , $except = null )
{

  $q = "SELECT COUNT( ".$field." ) AS total FROM ".$table." WHERE ".$field." = '".dbEscapeString( $value )."'";
  if ($except != null) {
    $q .= " AND ".$except;
  }

  $qr = dbQuery( $q );
  $qrow = dbFetch($qr);
  $total = $qrow['total'];

  return $total == 0;
}


/**
 * Retrieve a result and put it into a json array
 * Not too tricky just doing a json encode on the return
 */
function dbFetchJson( $qr ) {
  return json_encode( dbFetch( $qr ) );
}

// This function creates a table to which we will make the DKVS ( Document, Key, Value, Store ) on.
function initDKVS() {

  // If we don't have the base of teh DKVS system create the table.
  if( ! dbTableExists( "dkvs" ) ) {
    $q = "CREATE TABLE 'dkvs' ".
         "('document_id' TEXT NOT NULL, 'collection' TEXT NOT NULL , 'timestamp' INTEGER NOT NULL , 'key' TEXT NOT NULL , 'value' BLOB , PRIMARY KEY ('document_id', 'key' , 'collection' ))";
    $qr = dbQuery( $q );
  }

}


// This function will save a document ( PHP Array ), recursively.
function saveDocument( $collection = NULL , &$document ) {

  $time = time();

  // This is a poor man's document store meant to work only with PHP arrays.
  if( ! is_array( $document ) ) {
      die( "DKVS ERROR :: At this time only arrays are supported in the DKVS system." );
  }

  if( $collection === NULL || empty( $collection ) ) {
    die( "DKVS ERROR :: You must specify a collection for the document to be saved in.");
  }


  $collection = strtolower( $collection );
  $collection = preg_replace( "/[^a-z]/" , "" , $collection );
  $collection = trim($collection);


  if( empty( $collection ) || strlen( $collection ) == 0 ) {
    die( "DKVS ERROR :: You must specify a valid collection for the document to be saved in." );
  }


  // Do we even have a document key value table?
  // You can probably take this out if you are sure.
  initDKVS();


  // When we save we don't want to save the docment_id
  $document_id = false;
  if( isset( $document['_document_id'] ) ) {
    $document_id = $document['_document_id'];
    unset( $document['_document_id'] );
  }

  // We don't want to resave the collection
  unset( $document['_collection'] );


  // Did we have a document id?
  if( $document_id ) {
    // Did we already have document in the system with this id?
    // Delete it out be fore resaving.
    deleteDocument( $document_id );

    // This is the hulk smash way. Porbably not so nice.
    // Delete the entire document out before we save again.
    // Then again maybe this is what we want.

  } else{
    // other wise we set a document_id
    // cause this is a new one.

    // It's probably bad to do this too. However, UUID is not
    // What I am concerned about right now.
    $document_id = uniqid( "doc_" , true ); 
  }

  
  // Go through the array and save it to the db.
  foreach( $document as $k => $v ) {
    
    $data = array();
    $data['key'] = $k;

    // Handle sub arrays
    if( is_array( $v ) ) {
      $v = saveDocument( $collection . $k , $v );
    }

    // Set the value and doc id.
    $data['value'] = $v;
    $data['document_id'] = $document_id;
    $data['collection'] = $collection;
    $data['timestamp'] = $time;
    
    // Insert it into the DB.
    $q = dbInsertFromVals( "dkvs" , $data );
    $qr = dbQuery( $q );

  }

  // Reset this to what it was or the new one.
  $document['_document_id'] = $document_id;
  $document['_collection'] = $collection;
  $document['_timestamp'] = $time;

  // return the ID.
  return $document['_document_id'];

}


// This function will recursively load a document and return it.
function loadDocument( $document_id ) {

  // Do we even have a document key value table?
  initDKVS(); // if we don't, we probbably got other troubles.

  $r = array(); // blank array.

  $q = "SELECT * FROM dkvs WHERE document_id = ".dbEscapeString( $document_id );
  $qr = dbQuery( $q );
  while( $qrow = dbFetch( $qr ) ) {

    // set the collection if not already done.
    ( ! isset( $r['_collection'] ) ) ? $r['_collection'] = $qrow['collection'] : NULL;
    // set the collection if not already done.
    ( ! isset( $r['_timestamp'] ) ) ? $r['_timestamp'] = $qrow['timestamp'] : NULL;

    $r[$qrow['key']] = $qrow['value'];

    // Hey whoa, is this a sub document, then let's load it and roll.
    if( strpos( $qrow['value'] , "doc_" ) === 0 ) {
      $r[$qrow['key']] = loadDocument( $qrow['value'] );
    }

  }

  // The document ID.
  $r['_document_id'] = $document_id;
  
  // Give back r or false.
  return ( count( $r ) ) ? $r : FALSE;

}



// This function will recursively delete a document from the database.
function deleteDocument( $document_id ) {

  // Load up the document that we are going to delete
  $document = loadDocument( $document_id ); 

  // Go through and see if there are sub arrays.
  foreach( $document as $k => $v ) {
    if( is_array( $v ) && isset( $v['_document_id'] ) ) {
      deleteDocument( $v['_document_id'] );
    }
  }

  // Delete everything of me from the DB.
  $q = "DELETE FROM dkvs WHERE document_id = ".dbEscapeString( $document_id );
  $qr = dbQuery( $q );

  // Yep we did it!
  return TRUE;

}

function deleteDocuments( $doc_ids ) {
  if( ! is_array( $doc_ids ) || count( $doc_ids ) == 0 ) {
    return FALSE;
  }
  
  foreach( $doc_ids as $doc_id ) {
    deleteDocument( $doc_id );
  }

  // Yep we deleted them.
  return true;

}

// find docuemtments by a value
function findDocuments( $collection , $key = NULL, $value = NULL , $op = "=" , $cast = "TEXT" , $order = "ASC" , $offset = NULL , $limit = NULL ) {

  global $sqlite_data_types;

  $allowed_ops = array();
  $allowed_ops[] = "=";
  $allowed_ops[] = "<";
  $allowed_ops[] = "<=";
  $allowed_ops[] = ">";
  $allowed_ops[] = ">=";

  $allowed_orders = array();
  $allowed_orders[] = "DESC";
  $allowed_orders[] = "ASC";

  if( $collection === NULL || empty( $collection ) ) {
    die( "DKVS ERROR :: You must specify a collection for finding documents" );
  }

  // init the system just incase. still thinking how to avoid this.
  initDKVS();

  // Shortcircuit, if only searching on collection return the doc ids from the collection and stop.
  if( $key === NULL || empty( $key ) ) {
    $q = "SELECT DISTINCT( document_id ) AS document_id FROM dkvs WHERE collection = ".dbEscapeString( $collection )."";
    $qr = dbQuery( $q );
    $r = array();
    while( $qrow = dbFetch( $qr ) ) {
      $r[] = $qrow['document_id'];
    }
    return ( count($r) > 0 ) ? $r : FALSE;
  }

  // OK we are going to do something more interesting.... ;)

  if( ! in_array( strtoupper( $cast ) , $sqlite_data_types ) ) {
    die( "DB ERROR: Casting to bad type: ".htmlentities($cast) );
  }

  if( ! in_array( strtoupper( $order ) , $allowed_orders ) ) {
    die( "DB ERROR: Invalid Ordering: ".htmlentities($order) );
  }


  if( ! in_array( strtoupper( $op ) , $allowed_ops ) ) {
    die( "DB ERROR: Invalid Operation: ".htmlentities($op) );
  }

  $q  = "SELECT DISTINCT( document_id ) AS document_id FROM dkvs WHERE collection = ".dbEscapeString($collection)." AND key = ".dbEscapeString( $key )." AND CAST( value AS ".$cast.") ".$op." ".dbEscapeString( $value );
  $q .= " ORDER BY CAST( value AS ".$cast." ) ".$order;

  if( $limit !== NULL ) {
    $q .= " LIMIT " . intval( $limit );
  }
  if( $offset !== NULL ) {
    $q .= " OFFSET " . abs( intval( $offset ) );
  }

  $qr = dbQuery( $q );
  $r = array();
  while( $qrow = dbFetch( $qr ) ) {
    $r[] = $qrow['document_id'];
  }
  return ( count($r) > 0 ) ? $r : FALSE;

}


// return an array of loaded ocuments from an array.
function loadDocuments( $doc_ids ) {

  if( ! is_array( $doc_ids ) || count( $doc_ids ) == 0 ) {
    return FALSE;
  }

  $r = array();

  foreach( $doc_ids as $doc_id ) {
    $r[] = loadDocument( $doc_id );
  }

  return $r;

}
