<?php
/**
 * Defines constants for sending email via SMTP.
 *
 * @category Category
 * @package  Package
 * @author   Rob Brown <brownrl@gmail.com>
 * @license  http://www.gnu.org/philosophy/free-sw.html GPL Free
 * @link     http://www.itsgotto.be/cv.php
 */

define("FROM", "noreply@somerdomain.be");
define("SMTP", false);
define("SMTPAUTH", true);
define("SMTPUSERNAME", FROM);
define("SMTPPASSWORD", "");
define("SMTPHOST", "");
define("SMTPPORT", "");
