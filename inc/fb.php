<?php

/**
 * @file: define some Facebook globals
 */

  define( "FB_APP_ID" , "680169821998130" );
  define( "FB_APP_SECRET" , "2980b2928164d1379084cd5fdb1dd0ea" );

  require_once 'inc/fb-php-sdk/facebook.php';

  $facebook = new Facebook(array(
    'appId'  => FB_APP_ID,
    'secret' => FB_APP_SECRET,
  ));

  if ( isset( $_COOKIE['fbs_' . FB_APP_ID] ) ) {
    $cookie = preg_replace("/^\"|\"$/i", "", $_COOKIE['fbs_' . FB_APP_ID]);
    parse_str($cookie, $data);
    $facebook->setAccessToken($data['access_token']);
  }
