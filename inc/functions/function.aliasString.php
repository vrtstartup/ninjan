<?php
/**
 * @file
 * This file defines the aliasString function
 */

/**
 * Take a string and return a machine name no nonesense string
 */
function aliasString($t)
{
    $t = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($t, ENT_QUOTES, 'UTF-8'));
    $t = strtolower( $t );
    $t = trim( $t );
    $t = preg_replace( "/[^a-z]/" , "" , $t );

    return $t;
}
