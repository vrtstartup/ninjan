<?php
/**
 * @file
 * This file defines the blob function
 */

/**
 * Generate an HTML from a snippets of smarty located in the blob dir.
 *
 *
 * @param string $blob
 * snippet that you want to use.
 *
 * @param string $lg
 * The language
 *
 * @param array $params
 * Other paramaters
 *
 */
function blob( $blob , $lg = NULL, $params = NULL )
{
  global $languages;
  global $default_language;

  // Ok now we can operate on languages
  // These should be there if we are in the front end.

  // In the front end we should have a session var.
  if (! $lg && isset( $_SESSION['language'] ) ) {
    $lg = $_SESSION['language'];
  }

  // Here we use en?
  if (! $lg) {
    $lg = $default_language;
  }

  // 1 Take the language specific version
  // 2 Or take a language neutral
  // 3 Or take a language neutral on dir higher.

  $blob = basename( $blob, "html" );
  if ( file_exists( "./blobs/" . $blob . "-" . $lg . ".html" ) ) {
    $blob = "./blobs/" . $blob . "-" . $lg . ".html";
  } elseif ( file_exists( "./blobs/" . $blob . ".html" ) ) {
    $blob = "./blobs/" . $blob . ".html";
  } elseif ( file_exists( "../blobs/" . $blob . ".html" ) ) {
    $blob = "../blobs/" . $blob . ".html";
  } else {
    return "";
  }

  return file_get_contents( $blob );

}
