<?php
/**
 * @file
 * This file defines the getDistance function
 */

/**
 * Return the distance in in KM or M from points
 *
 * @param float $lat1
 * The latitude of point 1.
 * @param float $lon1
 * The latitude of point 1.
 * @param float $lat2
 * The latitude of point 2.
 * @param float $lon2
 * The latitude of point 2.
 * @param string $unit
 * K for Kilometers or M for miles or N for nautical
 *
 */

function getDistance($lat1, $lon1, $lat2, $lon2, $unit = "K")
{
  // Algorythm taken from some web page that I found with google.
  // There are probably 1037 pages out there you also can search and use.

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper(trim($unit));

  if ($unit == "K") {
    return ($miles * 1.609344);
  } elseif ($unit == "N") {
    return ($miles * 0.8684);
  } else {
    return $miles;
  }
}
