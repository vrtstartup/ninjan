<?php
/**
 * @file
 * This file defines the errors() function
 *
 */

/**
 * The errors function.
 *
 * This function return the value in the global $errors if it is:
 *
 * @param string $key
 *   The array key to use.
 * @return string
 *   The value in the array there.
 */
function errors( $key )
{
  global $errors;
  if ( is_array( $errors ) && isset( $errors[$key] ) ) {
    return $errors[$key];
  }
}
