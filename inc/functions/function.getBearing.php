<?php
/**
 * @file
 * This file defines the getBearing function
 */

/**
 * Return the compass heading from point 1 to point 2 0-360
 *
 * @param float $lat1
 * The latitude of point 1.
 * @param float $lon1
 * The latitude of point 1.
 * @param float $lat2
 * The latitude of point 2.
 * @param float $lon2
 * The latitude of point 2.
 *
 */

function getBearing($lat1, $lon1, $lat2, $lon2)
{
  // This algorythm was also lifted from google.

  //difference in longitudinal coordinates
  $dLon = deg2rad($lon2) - deg2rad($lon1);

  //difference in the phi of latitudinal coordinates
  $dPhi = log(tan(deg2rad($lat2) / 2 + pi() / 4) / tan(deg2rad($lat1) / 2 + pi() / 4));

  //we need to recalculate $dLon if it is greater than pi
  if (abs($dLon) > pi()) {
    if ($dLon > 0) {
      $dLon = (2 * pi() - $dLon) * -1;
    } else {
      $dLon = 2 * pi() + $dLon;
    }
  }
  //return the angle, normalized
  return (rad2deg(atan2($dLon, $dPhi)) + 360) % 360;
}
