<?php
/**
 * @file
 * This file defines the getDateSelect function
 */

/**
 * Generate a set of HTML drop downs for date.
 *
 * The drop downs will work on a hidden input.
 * The users browser will require basic JS.
 * The date is assumed to be and set for midnight
 * of that date. 00:00:00
 *
 * @param string $name
 *   The name that you want the form element to be.
 *
 * @return string
 *   HTML string of drops downs.
 *
 */
function getDateSelect( $name )
{
  // use today if not set.
  if ( isset( $_POST[$name] ) && $_POST[$name] ) {
    $t = intval( $_POST[$name] );
  } else {
    $t = time();
  }

  // make $t always midnight
  $t = strtotime( date( "Y-m-d 00:00:00" , $t ) );

  // Setup some vars for pre selecting.
  $d = date( "d" , $t );
  $m = date( "m" , $t );  // note javascript wants the month to be -1
  $y = date( "Y" , $t );

  $r = "";

  // make the js function
  // php generating JS YEAH!
  // actually not too bad just take the selector value and make a date out of it
  // then convert it to PHP time()/MYSQL
  $r .= "<script>\n";
  $r .= "function update_".$name."()\n";
  $r .= "{\n";
  $r .= "var d=document.getElementById( 'd-".$name."' ).value;\n";
  $r .= "var m=document.getElementById( 'm-".$name."' ).value-1;\n";
  $r .= "var y=document.getElementById( 'y-".$name."' ).value;\n";
  $r .= "var myDate=new Date(y,m,d);\n";
  $r .= "var val=Math.floor( myDate.getTime() / 1000 );\n";
  $r .= "//alert( val );\n";
  $r .= "document.getElementById( '".$name."' ).value = val;\n";
  $r .= "}\n";
  $r .= "</script>\n\n";

  // make the hidden field that the selector will work on.
  $r .= "<input type=\"hidden\" name=\"".$name."\" id= \"".$name."\" value='".$t."' />\n";

  // now make the day select
  $i = 0;
  $r .= "\n<div class=\"control-group\"><div class=\"row\"><div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\"><select class=\"form-control\" name=\"\" id='d-".$name."' onChange='update_".$name."();' />\n";
  while ($i++ < 31) {
    $sel = "";
    if ($d == $i) {
      $sel = " selected ";
    }
    $r .= "<option value='".$i."'".$sel.">".$i."</option>\n";
  }
  $r .= "</select></div>\n\n";

  // now make the month select
  $i = 0;
  $r .= "\n<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\"><select class=\"form-control\" name=\"\" id='m-".$name."' onChange='update_".$name."();' />\n";
  while ($i++ < 12) {
    $sel = "";
    if ($m == $i) {
      $sel = " selected ";
    }
    $r .= "<option value='".$i."'".$sel.">".$i."</option>\n";
  }
  $r .= "</select></div>\n\n";

  // now make the year select
  $i = 1900;
  $r .= "\n<div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\"><select class=\"form-control\" name=\"\" id='y-".$name."' onChange='update_".$name."();' />\n";
  while ($i++ < 2025) {
    $sel = "";
    if ($y == $i) {
      $sel = " selected ";
    }
    $r .= "<option value='".$i."'".$sel.">".$i."</option>\n";
  }
  $r .= "</select></div></div></div>\n\n";

  return $r;

}
