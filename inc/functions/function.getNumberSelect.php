<?php

// FUNCTION: getNumberSelect
//
// create a select box for selecting number from min - max.
//
// $name -> name of the form element, also the id for css selecting.
// $min -> the starting number
// $max -> the end number
// $default -> the default that you want to be selected when shown.
//
//  ex,   getNumberSelect( "birthdate_day" , 1 , 31 , 1 )
//
function getNumberSelect( $name , $min , $max , $default = null )
{
    $temp = "";

    // if it is not set from the form then show this one as selcted...
    if ( ! isset( $_POST[$name] ) ) {
      $_POST[$name] = intval( $default );
    }

    // the id is set for styling feel free to mod
    $temp .= "<select class=\"form-control\" name=\"".$name."\" id=\"".$name."\" >\n";

    // we go from min to max, yes yes yes min should be < max
    // what fool would call it switched?
    while ($min <= $max) {
        $temp .= "<option value=\"".$min."\"";

        if ( intval( $_POST[$name] ) == intval( $min ) ) {
            $temp .= " SELECTED ";
        }

        // I always put the spaces there cause all the browsers do weird things
        // with select boxes and this seemed to make things ok.
        $temp .= ">".$min."</option>\n";
        $min++;
    }
    $temp .= "</select>\n";

    return $temp;
}
