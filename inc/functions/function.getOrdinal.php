<?php
/**
 * @file
 * This file defines the getOrdinal function
 */

/**
 * You give me 1 I give you 1st
 *
 * You give 24 I give 24th
 *
 * You give me 2 I will not give you 2e Frenchy!
 * or 2de Dutchy!
 *
 */
function getOrdinal($cdnl)
{
    $test_c = abs($cdnl) % 10;
    $ext = ((abs($cdnl) %100 < 21 && abs($cdnl) %100 > 4) ? 'th'
            : (($test_c < 4) ? ($test_c < 3) ? ($test_c < 2) ? ($test_c < 1)
            ? 'th' : 'st' : 'nd' : 'rd' : 'th'));

    return $cdnl." <sup>".$ext."</sup>";
}
