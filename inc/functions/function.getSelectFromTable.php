<?php
/**
 * @file
 * This file defines the getSelectFromTable function
 */

/**
 * Make a select box based on value and labels in a db table.
 *
  *
 * @param string $name
 *   The name that you want the form element to be.
 * @param string $table
 *   The table that data will come from.
 * @param string $labels
 *   The field in the table that has the labels.
 * @param string $values
 *   The field in the table that has the values.
 * @param string $where
 *   Optional Where statement
 * @param string $order
 *   Optional order by on the stement, defaults to label field
 * @param array $default
 *   Optional first value/label pair to appear at the top
 *
 * @return string
 *   HTML string of the drop down.
 *
 */
function getSelectFromTable( $name , $table , $labels , $values , $where = -1 , $orderby = -1 , $default = null )
{

  // if we have a where clause use it if not nevermind
  if ($where == -1 || $where == null) {
    $where = "";
  } else {
    $where = " WHERE ".$where;
  }

  // if we have an order by use it other wise order by label field
  if ($orderby == -1 || $orderby == null) {
    $orderby = " ORDER BY label ASC";
  } else {
    $orderby = " ORDER BY ".$orderby;
  }

  $temp = "";

  // get the labels and the values from the table
  $q = "SELECT ".$labels." as label, ".$values." as value FROM ".$table."".$where.$orderby;
  $qr = dbQuery( $q );

  $m = "";
  $base = "";
  if( strpos( $name , "[]" ) !== false ) {
    $m = "multiple";
    $base = str_replace("[]", "", $name);
  }
  // begin building the select box
  // you can css on the #id if you want
  $temp .= "<select class=\"form-control dropdown\" name=\"".$name."\" id=\"".$name."\" ".$m.">";

  // Build the first option
  if ( is_array($default) && isset($default['value']) && isset($default['value']) ) {
    $temp .= "<option value=\"".$default['value']."\">".$default['value']."</option>\n";
  }

  // roll through the result set building the options
  while ( $qrow = dbFetch( $qr ) ) {
    $temp .= "<option value=\"".$qrow['value']."\"";

    // lets make this one selected if it was selected before
    if ( isset( $_POST[$name] ) && $_POST[$name] == $qrow['value'] || isset( $_GET[$name] ) && $_GET[$name] == $qrow['value'] ) {
      $temp .= " selected";
    }

    if( $m == "multiple" && isset( $_POST[$base] ) && in_array( $qrow['value'] , $_POST[$base] ) ) {
      $temp .= " selected"; 
    }

    // sorry for the entities and such but when you live
    // amongst french speaking folks they tend be anal about
    // their precious special charecters théy rêàlly suck còck!

    $temp .= " >".$qrow['label']."</option>\n";
  }

  $temp .= "</select>\n";

  return $temp;
}
