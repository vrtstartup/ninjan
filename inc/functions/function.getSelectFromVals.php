<?php
/**
 * Defines a function
 *
 * @category Helper
 * @package  Skeleton
 * @author   Rob Brown <brownrl@gmail.com>
 * @license  http://www.gnu.org/philosophy/free-sw.html GPL Free
 * @link     http://www.itsgotto.be/cv.php
 */


/**
 * Get a select drop down from an associative array.
 *
 * Get a select/html from an associative array of values.
 *
 * @param string $name The name/id of the select
 * @param array $vals The array of 'values'=>'labels'
 *
 * @return string HTML/SelectDrop Down.
 */
function getSelectFromVals( $name , $vals ) {
  if( ! is_array( $vals ) || count( $vals ) == 0 ) {
    return false;
  }

  $r = "<select id=\"".$name."\" name=\"".$name."\" class=\"form-control\">\n";

  foreach( $vals as $k => $v ) {
    $s = ( pvar( $name ) == $k ) ? " selected" : "";
    $r .= "<option value=\"".$k."\"".$s.">".$v."</option>\n";
  }

  $r .= "</select>\n\n";

  return $r;

}
