<?php
/**
 * @file
 * This file defines the getTablePager function
 */

/**
 * Make a generic HTML pagination from a table.
 *
 * You probably want to make a copy and paste of this
 * function to do special things that are not covered in this one.
 * for example, you might want a to list the elements as a ul/li
 * or you want to add a where clause..
 *
 * @param string $table
 *   The table that data will come from.
 * @param array $fields
 *   The field sin the table that will be shown.
 * @param array $rewrites
 *   The fields rewrites for individual fields.
 * @param integer $per_page
 *   10 items per page?
 * @param bool $show_total
 *   Show the total fields on the bottom?
 *
 * @return string
 *   HTML string of the table and links.
 *
 * @TODO
 *   Need to add a where clauses
 * @TODO
 *   Need to make the rewrites better
 *
 */
function getTablePager( $table , $fields, $rewrites = NULL, $per_page = 10, $show_total = FALSE )
{
  if ( ! dbTableExists( $table ) ) {
    die("Whoa you are trying to do pagination on a table that doesn't exist! ($table)" );
  }

  if ( ! is_array($fields) || count($fields) == 0 ) {
    die("Whoa you are trying to do pagination with no fields...");
  }

  // Build a base url to go to later.
  $get = $_GET;
  unset($get['offset']);
  unset($get['sort_by']);
  unset($get['order']);
  unset($get['order']);
  $bu = http_build_query($get);

  // Lets get the total in the table
  $q = "SELECT COUNT( ".$fields[0]." ) as total FROM ".$table."";
  $qr = dbQuery( $q );
  $qrow = dbFetch( $qr );
  $total_records = $qrow['total'];

  // Lets get an offset started amd handle if we go off the ends with prev and next button
  $offset = 0;
  if ( isset( $_GET['offset'] ) ) {
    $offset = intval( $_GET['offset'] );
  }

  if ($offset < 0) { // if less than 0 just go to 0
    $offset = 0;
  }

  while ($offset > $total_records) { // if over the total keep subtracting till under
    $offset = $offset - $per_page;
  }

  // lets get a sort by field
  // take field[0] if not in there
  $sort_by = $fields[0];
  if ( isset($_GET['sort_by']) ) {
    $sort_by = $_GET['sort_by'];
    if ( ! in_array( $sort_by , $fields ) ) {
      $sort_by = $fields[0];
    }
  }

  // Now we are going to define the sorting order either ASC or DESC.
  $directions[] = "ASC";
  $directions[] = "DESC";

  $order = "ASC";

  if ( isset($_GET['order']) ) {
    $order = $_GET['order'];
    if ( ! in_array( $order , $directions ) ) {
      $order = "ASC";  // ASC is default
    }
  }

  // Switch directions click on the ascending, get descending vice versa
  $odirections['ASC'] = "DESC";
  $odirections['DESC'] = "ASC";

  // order signals up and down arrows maybe you want to use a picture?
  $osignals['ASC'] = "<span class=\"up_arrow\">&uarr;</span>";
  $osignals['DESC'] = "<span class=\"down_arrow\">&darr;</span>";

  // BUILDING THE QUERY
  // join the fields
  $fs = join( ", " , $fields );
  $q = "SELECT ".$fs." FROM ".$table." "."ORDER BY ".$sort_by." ".$order." LIMIT ".$per_page." OFFSET ".$offset;
  $qr = dbQuery( $q );

  // define some odds and evens
  $i = 1;
  $oe[0] = "even";
  $oe[1] = "odd";
  // BUILD the HTML table
  // This point we go out into html world

  $output = "";

  $output .= '<table class="table table-striped table-bordered">' . "\n";
  $output .= '<!-- first row with headers -->' . "\n";
  $output .= '  <tr class="header_row">' . "\n";
  foreach ($fields as $k => $v) {
    $output .= '    <th>' . "\n";
    $output .= '    <a href="?'.$bu.'&sort_by='. $v .'&order='. $odirections[$order] .'">'. gT( $v . "_label" ) .'</a>' . "\n";
    if ($sort_by == $v) {
      $output .= '    '. $osignals[$order];
    }

    $output .= '  </th>' . "\n";
  }
  $output .= '  </tr>' . "\n";
  $output .= '  <!-- data row -->' . "\n";
  while ( $qrow = dbFetch( $qr ) ) {
    $output .= '    <tr class="data_row '. $oe[$i % 2] .'">' . "\n";
    foreach ($qrow as $k => $v) {
      if ( ! isset( $rewrites[$k] ) ) {
        $output .= '    <td>'. $v .'</td>' . "\n";
      } else {
        if ($v) {
          $output .= '    <td>'. str_replace( ":".$k.":", $v, $rewrites[$k] ) .'</td>' . "\n";
        } else {
          $output .= '    <td></td>' . "\n";
        }

      }
    }
    $output .= '  </tr>' . "\n";
    $i++;
  }
  $output .= '</table><!-- /end table -->' . "\n";

  $output .= '<div class="pagination-container"><ul class="pagination">' . "\n";

  $output .= '<!-- previous button -->' . "\n";
  $output .= '<li><a class="previous_button" href="?'.$bu.'&sort_by='. $sort_by .'&order='. $order .'&offset='. ( $offset - $per_page ) .'">'. gT( "previous" ) .'</a></li>' . "\n";

  $output .= '<!-- pagination links -->' . "\n";
  for ($i = 0 ; $i < $total_records ; $i = $i + $per_page) {
    $output .= '<li><a class="page_button" href="?'.$bu.'&sort_by='. $sort_by .'&order='. $order .'&offset='. $i .'">'. ( ( $i / $per_page ) + 1 ) .'</a></li>' . "\n";
  }

  $output .= '<!-- next button -->' . "\n";
  $output .= '<li><a class="next_button" href="?'.$bu.'&sort_by='. $sort_by .'&order='. $order .'&offset='. ( $offset + $per_page ) .'">'. gT( "next" ) .'</a></li>' . "\n";

  $output .= '</div>' . "\n";

  if ($show_total) {
  $output .= '<!-- total records -->' . "\n";
  $output .= '<p class="total_records">Total Records: '. $total_records .'</p>' . "\n";
  }

  return $output;

}
