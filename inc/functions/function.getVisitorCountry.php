<?php
/**
 * Short Description of File
 *
 * @category Category
 * @package  Package
 * @author   Rob Brown <brownrl@gmail.com>
 * @license  http://www.gnu.org/philosophy/free-sw.html GPL Free
 * @link     http://www.itsgotto.be/cv.php
 */



/**
 * Guess the users's country
 *
 * Use the geoplugin service to guess the visitor's country by IP.
 *
 * @return string the capital ISO 2 letter code for the country.
 */
function getVisitorCountry()
{
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];
    $result  = "Unknown";
    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));

    if($ip_data && $ip_data->geoplugin_countryName != null)
    {
        $result = $ip_data->geoplugin_countryCode;
    }

    return $result;
}
