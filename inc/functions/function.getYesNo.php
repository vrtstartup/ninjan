<?php
/**
 * @file
 * This file defines the getYesNo function
 */

/**
 * Generate a set of HTML radios to represent a bool.
 *
 * The drop downs will work on a hidden input.
 * The users browser will require basic JS.
 * The date is assumed to be and set for midnight
 * of that date. 00:00:00
 *
 * @param string $name
 *   The name that you want the form element to be.
 *
 * @return string
 *   HTML string of radios with yes and no..
 *
 */
function getYesNo( $name )
{
    $temp = "<div class=\"radio\">";
    $temp .= "<label class=\"control-label\">";

    $temp .= "<input type=\"radio\" name=\"".$name."\" id=\"".$name."-1\" value=\"1\" ";
    if ( isset( $_POST[$name] ) && intval( $_POST[$name] ) >= 1 ) {
        $temp .= "checked";
    }

    $temp .= " />" . "Yes" . "\n";
    $temp .= "</label></div>\n<div class=\"radio\"><label class=\"control-label\">";
    $temp .= "<input type=\"radio\" name=\"".$name."\" id=\"".$name."-0\" value=\"0\" ";

    if ( isset( $_POST[$name] ) && intval( $_POST[$name] ) == "0" ) {
        $temp .= "checked";
    }

    $temp .= " />";
    $temp .= "" . "No" . "</label></div>\n";

    return $temp;
}
