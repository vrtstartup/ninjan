<?php
/**
 * @file
 * This file defines the gvar function
 */

/**
 * The gvar function.
 *
 * This function returnt he value in get safely.
 *
 * @param string $key
 *   value you want.
 * @return string
 *   value
 */
function gvar( $key )
{
  if ( count( $_GET ) && isset( $_GET[$key] ) ) {
    return $_GET[$key];
  }
}
