<?php
/**
 * Describes the handle uploads function
 *
 * @category Category
 * @package  Package
 * @author   Rob Brown <brownrl@gmail.com>
 * @license  http://www.gnu.org/philosophy/free-sw.html GPL Free
 * @link     http://www.itsgotto.be/cv.php
 */

/**
 * Handle uploads from the admin.
 *
 * Goes through the table fields, sees if there is a corresponding
 * $_FILE and processes it. Normally, used for the admin.
 * Possible for the front with some modifications.
 *
 * @param string $table The db table to work on.
 * @param string $key_field the key field of the table.
 * @param string the key field value to work on.
 *
 * @return void
 */

function handleUploads( $table , $key_field , $id ) {

  $fields = dbFields( $table );
  

  $data = array();

  // got hrough all fields, if there is _FILES then we go go go go.
  foreach( $fields as $field ) {

    // ok we change things except the key field, that's just crazy!

    if( $field != $key_field ) {

      if( isset( $_POST['delete-'.$field] ) && $_POST['delete-'.$field] == "on" ) {
        $data[$field] = ""; 
      }

      if( isset( $_FILES[$field] ) ) {


        $file = $_FILES[$field];

        $dest_dir = "uploads/" . $table . "/" . $field . "/" . $id;
        // Make the dir recursive
        if ( ! file_exists( "../" . $dest_dir ) ) {
          mkdir( "../" . $dest_dir, 0777, TRUE );
        }

        // handle the file upload if it is there.
        if ( is_uploaded_file( $_FILES[$field]['tmp_name'] ) ) {
          // move the file to the place
          move_uploaded_file( $file['tmp_name'] , "../" . $dest_dir . "/" . $file['name'] );

          chmod( "../" . $dest_dir . "/" . $file['name'], 0777);

          $data[$field] = $dest_dir . "/" . $file['name'];
        }


        

      }

    }
  }

  //die( "<pre>" . print_r( $_FILES , TRUE ) . "</pre>" );

  if( count( $data ) ) {
    $q = dbUpdateFromVals( $table , $key_field , $id , $data );
    $qr = dbQuery( $q );
  }

}
