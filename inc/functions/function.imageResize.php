<?php
/**
 * @file
 * Describes the image resizing technique
 */



/**
 * Resize an Image
 *
 * Output it to the browser.
 *
 * Does not cache, probably should. Maybe version 2.
 */
function imageResize( $source_image, $width = 200, $height = 150, $quality = 70, $crop = true )
{

  // If the source image isn't an image seriously we got issues
  if ( ! $image_data = getimagesize( $source_image ) ) {
          die( "Whoa can't get the image size of the original?" );

          return false;
  }

  $r = $image_data[0] / $image_data[1];



  if( $width == 0 && $height != 0 ) {
    $width = floor( $image_data[0] * ( $height / $image_data[1] ) );
  }

  if( $height == 0 && $width != 0 ) {
    $height = floor( $image_data[1] * ( $width / $image_data[0] ) );
  }

  //die( $width . " :: " . $height );

  // Is it a gif, jpg or png. Sorry anything else probably not worth it.
  switch ($image_data['mime']) {
          case 'image/gif':
                  $get_func = 'imagecreatefromgif';
                  $suffix = ".gif";
          break;
          case 'image/jpeg';
                  $get_func = 'imagecreatefromjpeg';
                  $suffix = ".jpg";
          break;
          case 'image/png':
                  $get_func = 'imagecreatefrompng';
                  $suffix = ".png";
          break;
  }

  // Setup some variables
  $img_original = call_user_func( $get_func, $source_image );
  $old_width = $image_data[0];
  $old_height = $image_data[1];
  $new_width = $width;
  $new_height = $height;
  $src_x = 0;
  $src_y = 0;
  $current_ratio = round( $old_width / $old_height, 2 );
  $desired_ratio_after = round( $width / $height, 2 );
  $desired_ratio_before = round( $height / $width, 2 );

  // Some people don't want to upscale images. I don't care
  // Uncomment if you want crash out and not upscale.
  /*
  if ($old_width < $width || $old_height < $height) {
           // The desired image size is bigger than the original image.
           // Best not to do anything at all really.
          return false;
  }
  */

  /**
   * If the crop option is left on, it will take an image and best fit it
   * so it will always come out the exact specified size.
   */
  if ($crop) {
    /**
     * create empty image of the specified size
     */
    $new_image = imagecreatetruecolor( intval( $width ), intval( $height ) );

    /**
     * Landscape Image
     */
    if ($current_ratio > $desired_ratio_after) {
      $new_width = $old_width * $height / $old_height;
    }

    /**
     * Nearly square ratio image.
     */
    if ($current_ratio > $desired_ratio_before && $current_ratio < $desired_ratio_after) {
      if ($old_width > $old_height) {
        $new_height = max( $width, $height );
        $new_width = $old_width * $new_height / $old_height;
      } else {
        $new_height = $old_height * $width / $old_width;
      }
    }

    /**
     * Portrait sized image
     */
    if ($current_ratio < $desired_ratio_before) {
      $new_height = $old_height * $width / $old_width;
    }

    /**
     * Find out the ratio of the original photo to it's new, thumbnail-based size
     * for both the width and the height. It's used to find out where to crop.
     */
    $width_ratio = $old_width / $new_width;
    $height_ratio = $old_height / $new_height;

    /**
     * Calculate where to crop based on the center of the image
     */
    $src_x = floor( ( ( $new_width - $width ) / 2 ) * $width_ratio );
    $src_y = round( ( ( $new_height - $height ) / 2 ) * $height_ratio );
  }
  /**
   * Don't crop the image, just resize it proportionally
   */
  else {
    if ($old_width > $old_height) {
            $ratio = $old_width / $width;
    } else {
            $ratio = $old_height / $height;
    }



    $new_width = floor( $old_width / $ratio );
    $new_height = floor( $old_height / $ratio );

    //$new_width = $width;
    //$new_height = $height;

    //die( $width . " :: " . $height );

    $new_image = imagecreatetruecolor( $new_width, $new_height );
  }

  /**
   * Where all the real magic happens
   */
  imagecopyresampled( $new_image, $img_original, 0, 0, $src_x, $src_y, $new_width, $new_height, $old_width, $old_height );

  /**
   * Save it as a JPG File with our $destination_filename param.
   */
  header('Content-Type: image/jpeg');
  imagejpeg( $new_image, NULL, $quality  );

  /**
   * Destroy the evidence!
   */
  imagedestroy( $new_image );
  imagedestroy( $img_original );

  /**
   * Return true because it worked and we're happy. Let the dancing commence!
   */

  return true;
}
