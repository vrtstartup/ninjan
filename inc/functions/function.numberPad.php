<?php


function numberPad( $number, $n ) {
  return str_pad( (int) $number, $n, "0", STR_PAD_LEFT );
}