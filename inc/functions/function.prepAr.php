<?php
/**
 * @file
 * This file defines the prepAr function
 */

/**
 * Prepare an Array for Smarty and Normalize the language
 * So Keys of $array['something_field_en'] get their value copied to
 * $array['something_field']
 *
 *
 * @param array $ar
 * The array to work on.
 *
 *
 */
function prepAr( &$ar )
{
  // These should be there if we are in the front end.
  global $languages;
  global $default_language;

  // In the front end we should have a session var.
  if (! $lg && isset( $_SESSION['language'] ) ) {
    $lg = $_SESSION['language'];
  }

  // Here we use en?
  if (! $lg) {
    $lg = $default_language;
  }

  // Use english as absolute fallback or really fail!
  if (! $lg) {
    // better to just fail.
    die('You\'re calling for a language but we can\'t tell which to use.');
  }

  if ( is_array( $ar ) && count( $ar ) ) {
    $keys = array_keys( $ar );

    foreach ($keys as $key) {
      $okey = $key;
      $key = str_replace( "." , "_" , $key );
      $key = preg_replace( "/_".$lg."$/" , "" , $key );
      if ($key != $okey) {
        $ar[$key] = $ar[$okey];
        unset( $ar[$okey] );
      }
      if ( is_array( $ar[$key] ) ) {
        prepAr( $ar[$key] );
      }
    }
  }
}
