<?php
/**
 * @file
 * This file defines the pvar function
 */

/**
 * The pvar function.
 *
 * This function returnt he value in post safely.
 *
 * @param string $key
 *   value you want.
 * @return string
 *   value
 */
function pvar( $key )
{
  if ( count( $_POST ) && isset( $_POST[$key] ) ) {
    return $_POST[$key];
  }
}
