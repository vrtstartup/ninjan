<?php

/**
 * @file
 * This file defines the rediret function
 */

/**
 * Return a header redirect based on page argument.
 *
 * @param string $page
 * The page that you want to go to.
 *
 */
function redirect( $page , $args = null ) {

  $page = basename( $page );


  if( file_exists( "pages/".$page.".html" ) ) {

    if( is_array( $args ) && count( $args ) > 0 ) {
      $page .= "&" . http_build_query( $args );
    }

    header( "Location: ?page=" . $page );
    exit( 0 );

  }
  
  die( "It's seems that you are trying to do a redirect to a page that doesn't exist?" );
}