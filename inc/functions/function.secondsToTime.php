<?php
/**
 * @file
 * This file defines the getSecondsToTime function
 */

/**
 * Give back an array broken up into h, m, s.
 *
 * So 73 gives 0 1 13
 * 173 gives 0 2 53
 * 784568 gives ... get a calculator!
 *
 */

function getSecondsToTime($seconds)
{
    // extract hours
    $hours = floor($seconds / (60 * 60));

    // extract minutes
    $divisor_for_minutes = $seconds % (60 * 60);
    $minutes = floor($divisor_for_minutes / 60);

    // extract the remaining seconds
    $divisor_for_seconds = $divisor_for_minutes % 60;
    $seconds = ceil($divisor_for_seconds);

    // return the final array
    $obj = array(
        "h" => (int) $hours,
        "m" => (int) $minutes,
        "s" => (int) $seconds,
    );

    return $obj;
}
