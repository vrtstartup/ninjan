<?php
/**
 * Short Description of File
 *
 * @category Category
 * @package  Package
 * @author   Rob Brown <brownrl@gmail.com>
 * @license  http://www.gnu.org/philosophy/free-sw.html GPL Free
 * @link     http://www.itsgotto.be/cv.php
 */

/**
 * Function Short Description
 *
 * Function Longer Description
 *
 * @param string $to       variable description
 * @param string $toname   variable description
 * @param string $subject  variable description
 * @param string $template variable description
 * @param array  $vals     variable description
 *
 * @return returntype descriiption
 */
function sendEmail( $to , $toname , $subject , $template , $vals )
{

    $email_config_file = 'inc/email_config.php';
    $php_mailer_file = 'inc/phpmailer/class.phpmailer.php';

    if (is_file($email_config_file)) {
        include_once $email_config_file;
    } elseif (is_file('../' . $email_config_file)) {
        include_once '../' . $email_config_file;
    } else {
        die( "When trying to send an email we couldn't find the email_config_file" );
    }

    if (is_file($php_mailer_file)) {
        include_once $php_mailer_file;
    } elseif (is_file('../' . $php_mailer_file)) {
        include_once '../' . $php_mailer_file;
    } else {
        die( "When trying to send an email we couldn't find the php_mailer_file" );
    }

    $mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
    if (SMTP) {
        $mail->IsSMTP();
        $mail->SMTPAuth    = true;
        $mail->Username    = SMTPUSERNAME;
        $mail->Password    = SMTPPASSWORD;
        $mail->Host        = SMTPHOST;
        $mail->Port        = SMTPPORT;
        $mail->SMTPDebug   = false;
    }

    try {

        $from = FROM;
        $mail->AddReplyTo($from);
        if ($toname) {
            $mail->AddAddress($to, $toname);
        } else {
            $mail->AddAddress($to);
        }

        $mail->SetFrom($from);

        $mail->Subject = $subject;
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically

        // Use a file template or string
        if (file_exists("email_templates/" . $template . ".html")) {
            $mesg = file_get_contents("email_templates/".$template.".html");
        } else {
            // are we in the admin or sub dir?
            if (file_exists("../email_templates/".$template.".html")) {
                $mesg = file_get_contents("../email_templates/".$template.".html");
            } else {
                $mesg = $template;
            }
        }

        // built in tokens
        $vals['server_name'] = $_SERVER['SERVER_NAME'];

        // Token replacement
        foreach ($vals as $k => $v) {
            $mesg = preg_replace("/%".$k."%/", $v, $mesg);
        }

        // Set the final message
        $mail->MsgHTML($mesg);

        // Send or DIE!
        if ( ! $mail->Send() ) {
          die( "<pre>" . print_r( $mail , true ) . "</pre>\n" );

          return false;
        }

        return true;

    } catch (phpmailerException $e) {
    die( "<pre>" . print_r( $e , true ) . "</pre>\n" );
    } catch (Exception $e) {
    return false;
    }
}
