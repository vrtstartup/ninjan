<?php
/**
 * @file print a string nicely.
 */

/**
 * Print a str nicely from machine to something human.
 */
function strNice( $str )
{
  return ucwords( strtolower( str_replace( "_"  , " " , $str ) ) );
}
