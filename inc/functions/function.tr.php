<?php
/**
 * @file
 * This file defines the gT function
 */

/**
 * Get/Create texts based on a keystone and language
 *
 *
 * @param string $key
 *   The keystone
 *
 * @return string
 *   HTML string of of the translated text from the DB.
 *
 */
function tr( $key , $lg = NULL, $params = NULL)
{

    // These should be there if we are in the front end.
    global $languages;
    global $default_language;

    // In the front end we should have a session var.
    if (! $lg && isset( $_SESSION['language'] ) ) {
      $lg = $_SESSION['language'];
    }

    // Here we use en?
    if (! $lg) {
      $lg = $default_language;
    }

    // Use english as absolute fallback or really fail!
    if (! $lg) {
      // better to just fail.
      die('You\'re calling for a language but we can\'t tell which to use.');
    }

    // easier to type l than it is to type key.
    // Previous versions of this function used l.
    $l = $key;

    // Make sure the keystone is machine able.
    if ( preg_match( "/^[a-z][a-z0-9\_-]*[a-z0-9]$/" , $l ) ) {
        // Holder for later.
        $c = "";

        // Check/Get if the keystone is defined.

        $q = "SELECT COUNT( text_".$lg." ) AS total FROM texts WHERE text_key = '".$l."' LIMIT 1";
        $qr = dbQuery( $q );
        $qrow =dbFetch( $qr );
        $total = $qrow['total'];

        // Yes we have it.
        if ($total == 1) {
            $q = "SELECT text_".$lg." AS content FROM texts WHERE text_key = '".$l."' LIMIT 1";
            $qr = dbQuery( $q );
            $qrow = dbFetch( $qr );
            $c = $qrow['content'];
        }
        // No we don't create it.
        else {
          // Create a dummy text array.
          $t['text_key'] = $l;
          foreach ($languages as $lng => $config) {
            $t['text_'.$lng] = ".".$l."-".$lng.".";
          }
          $t['text_'.$lng] = $params['content'];

          // Will come in handy later when we export and import.
          $t['text_updated'] = time();

          // Create a new entry with some default values that should be easy to
          // see .keystone_en.
          $q = dbInsertFromVals( "texts" , $t );
          $qr = dbQuery( $q );

          $c = $t['text_'.$lg];
        }

        // $c is what we are going to return.

        // However?

        // Should we nl2br it?
        if ( isset($params['nl2br']) && $params['nl2br'] == "true") {
          $c = nl2br($c);
        }

        // Should ucwords it?
        if (isset($params['ucwords']) && $params['ucwords'] == "true") {
          $c = ucwords($c);
        }

        // Umm I think this would be a splendid place to add more
        // modifiers.
        return $c;

    }

    // Seriously you are trying to use a crazy keystone.
    return "Error: '".$l."' is not a valid keystone, a-z0-9_";
}
