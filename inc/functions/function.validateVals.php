<?php
/**
 * @file
 * This file defines the validateVals function
 */

/**
 * Loops through an array and compares the values to a regular expression.
 *
 * Gives back an array with key => Error
 *
 * Error will be "error" or "". Very handy for using in the CSS class.
 *
 * You can also set what you want the errors ot be in a corresponding array
 * ferrors.
 *
 * $regs['something'] = "/^..$/";
 * $vals['something'] = "John";
 * $ferrors['something'] = "Something is Required";
 *
 */
function validateVals( $regs , $vals = null , $ferrors = null )
{
  // Assume no errors to start with.
  $errors = FALSE;

  // Use Post if we have to.
  if ($vals == null) {
    $vals = $_POST;
  }

  // Do we have stuff work on?
  if ( count( $vals ) && count( $vals ) ) {
    $errors = array();
    foreach ($regs as $k => $v) {
      $value = '';
      if( isset( $vals[$k] ) ) {
        $value = $vals[$k];
      }
      if ( ! preg_match( $v , $value ) ) {
        if ( ! $ferrors || ! isset( $ferrors[$k] ) ) {
          $errors[$k] = true;
        } else {
          $errors[$k] = $ferrors[$k];
        }
      }
    }
  }

  // Return the errors
  return $errors;
}
