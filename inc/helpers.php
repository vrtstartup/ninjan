<?php

// A very basic includer

unset($files);
$files = array();
$files =  glob("inc/functions/function.*.php");

if ($files) {
  // Get the functions
  foreach ($files as $filename) {
      include_once( $filename );
  }
}

// At some point we probably want to use some auto loader spl super
// duper cool thing.

unset($files);
$files = array();
$files = glob("inc/classes/class.*.php");

if ($files) {
  // Get the classes
  foreach ($files as $filename) {
      include_once( $filename );
  }
}
