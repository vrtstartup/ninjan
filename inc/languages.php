<?php
/**
 * @file
 * This file defines the languages for the site.
 *
 * Edit this file to your needs.
 */

// Set a a default language, this will also be a global
// for use later on.
$default_language = 'nl'; // If all else fails use this.

// Here we define the different languages for our site.
// As you make changes here the programing should/will/try
// change the database and such to handle the new languages
// or absence of languages on the fly.


// Dutch
$languages['nl']['name'] = 'Nederlands';
$languages['nl']['short'] = 'NL';
$languages['nl']['locale'] = "nl_NL";



// Here is how language is selected when a visitor sees your site.

// Is it set on the URL: language=XX
// Is it Set in the session?
// Is it in a cookie?

// OR use default.


// Go through some if and determine the language we are on.
if( isset( $_GET['language'] ) && in_array( $_GET['language'] , array_keys($languages) ) )
{
    $_SESSION['language'] = $_GET['language'];
    setcookie( 'language' , $_GET['language'] , (time() + (5 * 365 * 24 * 60 * 60)) );
}

if( ! isset( $_SESSION['language'] ) && isset( $_COOKIE['language'] ) && in_array( $_COOKIE['language'] , array_keys($languages) ) )
{
    $_SESSION['language'] = $_COOKIE['language'];
}

// We could build in some more tests here.
// Like domain name?  browser signature?


// At the end of it all if there is no session language use default.
if( ! isset( $_SESSION['language'] ) || ! in_array( $_SESSION['language'] , array_keys($languages) ) )
{
    $_SESSION['language'] = $default_language;
    setcookie( 'language' , $default_language , (time() + (5 * 365 * 24 * 60 * 60)) );

    // Attempted french fallback.
    if( isset( $_SERVER['HTTP_ACCEPT_LANGUAGE'] ) && stristr($_SERVER['HTTP_ACCEPT_LANGUAGE'], "fr") && in_array( 'fr' , array_keys($languages) ) ) {
      $_SESSION['language'] = "fr";
      setcookie( 'language' , "fr" , (time() + (5 * 365 * 24 * 60 * 60)) );
    }
}


// We will assign l to the template
$assigns['l'] = $_SESSION['language'];
$assigns['locale'] = $languages[$_SESSION['language']]['locale'];

// Define a global Language will be handy later.
define( 'LANGUAGE', $assigns['l'] );

// Define the "other" language hand for Belgian website FR -> NL NL -> FR
$other['fr'] = "nl";
$other['nl'] = "fr";

if( isset( $other[$assigns['l']] ) ) {
  $assigns['other_l'] = $other[$assigns['l']];
}


// {$l}


// OK Now we need to make sure that the texts table in the DB is Kosher.
// Create the table if it is not already created.
dbCreateTable("texts", "text");
dbCreateField("key","TEXT","NULL","text","texts");
dbCreateField("updated","INTEGER","NOT NULL","text","texts");
foreach($languages as $l => $info)
{
  dbCreateField($l,"TEXT","NULL","text","texts");
}






