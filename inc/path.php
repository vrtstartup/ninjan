<?php

// LET's defin two globals here.
// THey probably will come in handy later in PHP.
// We need to know our URL and System Path.
// PHP changes over time and this probably can be written better too.

// @TODO
// We probably also need to handle https here to...

$protocol = "http://";
if( isset( $_SERVER["HTTPS"] ) && $_SERVER["HTTPS"] == "on" ) {
    $protocol = "https://";
}

define( 'PWD' , dirname( $_SERVER["SCRIPT_FILENAME"] ) . '/' );
if ( dirname( $_SERVER["SCRIPT_NAME"] ) == "/" ) {
  define( 'URL' , $protocol . $_SERVER['SERVER_NAME'] . "/" );
} else {
  define( 'URL' , $protocol . $_SERVER['SERVER_NAME'] . dirname( $_SERVER["SCRIPT_NAME"]  ) . "/" );
}
