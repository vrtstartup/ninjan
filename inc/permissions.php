<?php

  // Here we do some permission checking.
  // However, if you feel confident that things are ok
  // you can skip this file and take it out of the index.php.

  // Smart Templates
  if ( ! is_writeable('inc/smarty/templates_c') ) {
    header( "Location: errors/permissions.php?reason=inc/smarty/templates_c" );
    exit( 0 );
  }

  // Smarty Cache
  if ( ! is_writeable('inc/smarty/cache') ) {
    header( "Location: errors/permissions.php?reasons=inc/smarty/cache" );
    exit( 0 );
  }

  // DB Directory
  if ( ! is_writeable('db') ) {
    header( "Location: errors/permissions.php?reasons=db" );
    exit( 0 );
  }

  // DB File
  if ( ! is_writeable('db/database.sqlite') ) {
    header( "Location: errors/permissions.php?reason=db/database.sqlite" );
    exit( 0 );
  }

  // Uploads Dir
  if ( ! is_writeable('uploads') ) {
    header( "Location: errors/permissions.php?reason=uploads" );
    exit( 0 );
  }

  // Check the uploads dir deeply.
  $path = realpath('uploads');
  $objects = new RecursiveIteratorIterator(
               new RecursiveDirectoryIterator($path),
               RecursiveIteratorIterator::SELF_FIRST);
  foreach ($objects as $name => $object) {
      if ( basename( $object->getPathname() ) != "." && basename( $object->getPathname() ) != ".." && ! is_writable( $object->getPathname() ) ) {
        header( "Location: errors/permissions.php?reason=".$object->getPathname() );
        exit( 0 );
      }
  }
