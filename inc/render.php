<?php

// So now we have all the content we just
// need to figure out which template to use.

// This code will choose the template to use and
// render out the smarty.

// blank=1 on the GET will hard wire to blank template.
// Use the blank template if for ajaxing stuffs...

try {
  // get the page and process the smarty just on the page.
  $content = $smarty->fetch( $full_page_path );
  // Assign the full process content to content
  $smarty->assign( 'content' , $content );

} catch( SmartyCompilerException $e ) {
  die( "<pre>" . print_r( $e->getMessage() , TRUE ) . "</pre>" );
}


if ( ! isset( $_GET['blank'] ) || intval( $_GET['blank'] ) != 1 ) {
  // Get the template... If the page has a special template get
  // that one but for the most post just use index.
  $full_template_path = PWD . "templates/" . $_GET['page'] . ".tpl.php";
  if ( ! file_exists( $full_template_path ) ) {
    $smarty->display( PWD . "templates/default.tpl.php" );
  } else {
    $smarty->display($full_template_path);
  }
} else {
  $smarty->display( PWD  . "inc/blank.tpl.php" );
}
