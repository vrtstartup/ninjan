<?php

  if( isset( $_SESSION['cwd'] ) && is_dir( $_SESSION['cwd'] ) ) {
    $cwd = $_SESSION['cwd'];
    $assigns['cwd'] = $cwd;
  } else {
    header( "Location: ?page=index" );
    exit( 0 );
  }