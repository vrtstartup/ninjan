<?php

// Smarty is a very nice templating / caching system.
// It requires a little bit of setup.

// First thing we have to do is make sure that smarty can cache.

if ( ! is_writeable('inc/smarty/templates_c') ) {
  die('Directory ./inc/smarty/templates_c needs to be writable to the webserver' );
}

if ( ! is_writeable('inc/smarty/cache') ) {
  die('Directory ./inc/smarty/cache needs to be writable to the webserver' );
}

define('SMARTY_DIR', PWD . 'inc/smarty/libs/');
require_once(SMARTY_DIR . 'Smarty.class.php');
$smarty = new Smarty();

$smarty->template_dir = PWD . 'templates/';
$smarty->compile_dir  = SMARTY_DIR . '../templates_c/'; // writable
$smarty->config_dir   = SMARTY_DIR . '../configs/';
$smarty->cache_dir    = SMARTY_DIR . '../cache/'; // writable

// Maybe helpful as you go forward.
//$smarty->debugging = true;
//echo SMARTY_DIR;

// NOTE TO SELF:
//
// The Smarty plugins directory is very cool and you want to check it out.
// You can make custom "plugins" for smarty there.
