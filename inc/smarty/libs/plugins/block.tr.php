<?php



function smarty_block_tr($params, $content, $template, &$repeat)
{
  require_once 'inc/db.php';
  require_once 'inc/functions/function.tr.php';
  require_once 'inc/staging_domains.php';

  if ( is_null( $content ) || strlen( $content ) == 0 ) {
    return;
  }

  $str = '';

  if( ! isset( $params['l'] ) || empty( $params['l'] ) ) {
    return;
  }

  $params['content'] = $content;

  if ( isset( $params['language'] ) ) {
    $str = tr( $params['l'] , $params['language'] , $params);
  } else {
    $str = tr( $params['l'] , false , $params);
  }

  if( STAGING ) {
    $str = "<translate data-key=\"".$params['l']."\" data-language=\"".LANGUAGE."\">" . $str . "</translate>";
  }
  

  if ( isset( $params['assign'] ) && strlen($params['assign']) > 0 ) {
    $template->assign($params['assign'], $str);
  } else {
    return $str;
  }  

}