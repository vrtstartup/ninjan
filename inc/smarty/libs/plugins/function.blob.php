<?php
/**
 * @file
 * This file defines the block smarty plugin
 */

/**
 * The smarty_function_block function.
 *
 * This function return the HTML / Smarty output
 * of a file located in the blocks directory
 *
 *
 */
function smarty_function_blob( $params, $template )
{
  require_once 'inc/functions/function.blob.php';

  $str = "";

  if ( isset( $params['language'] ) ) {
    $str = blob($params['blob'], $params['language'], $params);
  } else {
    $str = blob($params['blob'], '', $params);
  }

  if ( isset( $params['assign'] ) && strlen($params['assign']) > 0 ) {
    $template->assign($params['assign'],$str);
  } else {
    return $str;
  }

}
