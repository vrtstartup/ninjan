<?php

/*
 * This function is basically a caller of the GT function.
 */
function smarty_function_gT( $params, $template )
{
    require_once 'inc/db.php';
    require_once 'inc/functions/function.gT.php';

    if ( ! isset( $params['l'] ) ) {
      $str = '';
    }

    if ( isset( $params['language'] ) ) {
      $str = gT($params['l'], $params['language'], $params);
    } else {
      $str = gT($params['l'], '', $params);
    }

    if ( isset( $params['assign'] ) && strlen($params['assign']) > 0 ) {
      $template->assign($params['assign'],$str);
    } else {
      return $str;
    }
}
