<?php


  // This is a cheap easy way to say to the system that we are in "Staging".
  $staging_domains = array();

  $staging_domains[] = "localdev.be";
  $staging_domains[] = "hiddensaloon.com";
  $staging_domains[] = "ngrok.com";

  // Add you typical dev/staging/work domains here.


  ////// Now we loop through and see.

  $staging = false;

  foreach( $staging_domains as $domain ) {
    if( strpos( $_SERVER['SERVER_NAME'] , $domain ) !== FALSE ) {
      $staging = true; // WE ARE IN STAGING MODE.
    }
  }

  // Make super duper mega global
  define( "STAGING" , $staging );

  // So now

  /*

    if( STAGING ) {
      echo "SWEET WE ARE IN STAGING".
    }

  */