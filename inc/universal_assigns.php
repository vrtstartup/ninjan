<?php

// Put assignments in this file that have to be used every where.

// Example:
$assigns['site_title'] = 'Ninja Maker';

// Some others that typically become useful
$assigns['URL'] = URL;
$assigns['URLNP'] = str_replace("http://", "", URL);
$assigns['URLNP'] = str_replace("https://", "", $assigns['URLNP']);
$assigns['UPLOADS_URL'] = URL . "uploads";
$assigns['ASSETS_URL'] = URL . "assets";
$assigns['spacer'] = '<br />';

// Assign a date to be used in the templates. Youalways need a year, yes?

$assigns['year'] = date( "Y" );

$assigns['factor'] = 5;
$assigns['frame_rate'] = 25;

// FB APP ID
//$assigns['fb_app_id'] = FB_APP_ID;

// We make a base URL with out the language in it.
$g = $_GET;
unset($g['language']);
$assigns['languagelinkbase'] = "?" . http_build_query($g);

$assigns['ABS_URL'] = urlencode( $assigns['languagelinkbase'] . "&language=". LANGUAGE );

$assigns['selected'][$_GET['page']] = "selected";
//die( print_r( $assigns ) );

$assigns['page_title'] = ucwords( strtolower( str_replace( "_" , " " , $_GET['page'] ) ) );

$ffmpeg = "/usr/local/bin/ffmpeg";
if( $_SERVER['SERVER_NAME'] == "beta.ninjanieuws.be" ) {
  $ffmpeg = "./ffmpeg";
}
$assigns['ffmpeg'] = $ffmpeg;


/*
$ogtags = array();

$ogtags['title'] = gT( $_GET['page'] . "_page_title" ) . " - Something";
$ogtags['image'] = URL . "assets/img/panzani-logo.png";
$ogtags['url'] = URL . $assigns['languagelinkbase'] . '&language=' . LANGUAGE;
$ogtags['site_name'] = "Something";
$ogtags['type'] = "website";
$ogtags['description'] = gT( "og_description" );

$assigns['ogtags'] = $ogtags;
$assigns['page_title'] = gT( $_GET['page'] . "_page_title" ) . " - Something";
$assigns['page_keywords'] = gT( $_GET['page'] . "_page_keywords" );
$assigns['page_description'] = gT( $_GET['page'] . "_page_description" );
$assigns['page_abstract'] = gT( $_GET['page'] . "_page_abstract" );
*/

/*
$signed_request = $facebook->getSignedRequest();

if ($signed_request) {
    $_SESSION['signed_request'] = $signed_request;
}

$assigns['signed_request'] = $_SESSION['signed_request'];
$assigns['signed_request_pre'] = "<pre>" . print_r( $_SESSION['signed_request'] , true ) . "</pre>";
*/
