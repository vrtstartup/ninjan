<?php

// Some basic stuff that we need.
// Change this for your time zone.
date_default_timezone_set('Europe/Brussels');

// This may be helpful later to say we are no in the admin?
define( "ADMIN" , FALSE );

// Every website uses a session, every web site that doesn't suck that is.
session_start();

// create an array that we put stuff into for later stuff.
$assigns = array();

require_once 'inc/staging_domains.php'; // Based on the domain name we will be
                                        // In staging mode or not.

require_once 'inc/db.php'; // Include a db conenction in this case sqlite.
require_once 'inc/languages.php'; // Handle languages, edit this for your site.
require_once 'inc/path.php'; // Sets up some URL and PATH variables for using later.
require_once 'inc/helpers.php'; // Includes functions and classes

// Facebook
// require_once( "inc/fb.php" ); // INclude facebook stuff if needed.



require_once 'inc/smarty.php'; // Build the smarty

require_once 'inc/permissions.php'; // You can take this out if you like.


// At this point we should have a page in mind that we want to show.
if ( ! isset( $_GET['page'] ) ) {
  $_GET['page'] = "index";
  header( "Location: ?page=index" );
  exit( 0 );
}

// The full path of the page HTML file.
$full_page_path = PWD . "pages/" . $_GET['page'] . ".html";
if ( ! file_exists( $full_page_path ) ) {
  // Show a NOT FOUND .org page when there is no page.
  header( "Location: ".URL."errors/404.php" );
  exit( 0 );
  //$_GET['page'] = "index";
  //$full_page_path = PWD . "pages/" . $_GET['page'] . ".html";
}


#require_once( "inc/fb.php" ); // Includes Facebook info
// Ok at this point we will be showing something.
// So lets do the universal assigns.
include 'inc/universal_assigns.php'; // Some smarty assigns that are universal


// Get the php pre process file and execute it.
$full_php_path = PWD . "php/" . $_GET['page'] . ".php";
if ( file_exists( $full_php_path ) ) {
  include( $full_php_path );
}


// OK now we assign anything that is in the assigns array to the smarty.
// Anything in the assigns array gets assigned to the smarty
if ( count( $assigns ) ) {
  foreach ($assigns as $k => $v) {
    $smarty->assign( $k , $v );
  }
}




// Compile the smarty tempaltes and send out to the browser.
include 'inc/render.php'; // Take all he stuff and render it.



// Clear the cache if called with cache clear.
if ( isset($_GET['cache']) && $_GET['cache'] == 'clear' ) {
    $smarty->clearAllCache();
    $smarty->clearCompiledTemplate();

}
// This will probably helpful?
#echo "<!-- <pre>\n" . print_r( PWD , true ) . "</pre> -->\n";
#echo "<!-- <pre>\n" . print_r( URL , true ) . "</pre> -->\n";
#echo "<!-- <pre>\n" . print_r( $_SESSION , true ) . "</pre> -->\n";
#echo "<!-- <pre>\n" . print_r( $_COOKIE , true ) . "</pre> -->\n";
