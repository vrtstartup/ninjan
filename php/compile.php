<?php


require_once( "inc/required_cwd.php" );

$ninja = $cwd . "/ninja.json";
$ninja = file_get_contents( $ninja );
$ninja = json_decode( $ninja );

if( ! isset( $_POST['action'] ) ) {
  $_POST['action'] = "";
}

$metatags                = array();
$metatags['title']       = $ninja->meta->title;

$title = $ninja->meta->title;
  
if($title == "") {
  $title = date("d.m.Y");
}

$mtitle = aliasString( $title );


$metatags['description'] = $ninja->meta->description;
$metatags['comment']     = $ninja->meta->description;
$metatags['copyright']   = "VRT Startup - Ninja Nieuws ".date( "Y" );
$metatags['year']        = date( "Y" );
$metatags['date']        = date( "Y" );
$metatags['language']    = "dut";
$metatags['artist']      = "VRT Startup - Ninja Nieuws";

$meta = "";
foreach( $metatags as $k => $v ) {
  $meta .= "-metadata ".$k."=\"".$v."\" ";
}


$bv1 = "assets/bumper/bumper_1280x720.mp4";
$bv2 = "assets/bumper/bumper_720x720.mp4";


if( $_POST['action'] == "compile_step_1" ) {
  
  $vof = $cwd . "/".$mtitle."-1280x720.mp4";
  $af = $cwd . "/audio.mp3";
    
  $command = $ffmpeg . " -i ".$af." -f image2 -i ".$cwd."/simage-%07d.png -r 25 -vcodec libx264 -pix_fmt yuv420p -b 5000k ".$meta." -y ".$vof." 2>&1";
  
  $output = array();
  $return_value = null;
  $last_line = exec( $command , $output , $return_value );
  
  die( "OK" );  

}


if( $_POST['action'] == "compile_step_2" ) {
  
  $ovf = $cwd . "/".$mtitle."-1280x720.mp4";
  $vof = $cwd . "/".$mtitle."-720x720.mp4";
    
  $command = $ffmpeg . " -i ".$ovf." -vf 'crop=720:720:280:0' -vcodec libx264 -pix_fmt yuv420p -b 5000k ".$meta." -y ".$vof." 2>&1";
  
  $output = array();
  $return_value = null;
  $last_line = exec( $command , $output , $return_value );

  die( "OK" );  

}



if( $_POST['action'] == "compile_step_3" ) {

  $ovf = $cwd . "/".$mtitle."-1280x720.mp4";
  $vof = $cwd . "/".$mtitle."-1280x720_bumper.mp4";
    
  $command = $ffmpeg . " -i ".$ovf." -i ".$bv1." -filter_complex \"concat=n=2:v=1:a=1\" -y ".$vof." 2>&1";
  
  $output = array();
  $return_value = null;
  $last_line = exec( $command , $output , $return_value );

  die( "OK" );
  die( "<pre>" . print_r( $output , TRUE ) . "</pre>" );

}


if( $_POST['action'] == "compile_step_4" ) {

  $ovf = $cwd . "/".$mtitle."-720x720.mp4";
  $vof = $cwd . "/".$mtitle."-720x720_bumper.mp4";
    
  $command = $ffmpeg . " -i ".$ovf." -i ".$bv2." -filter_complex \"concat=n=2:v=1:a=1\" -y ".$vof." 2>&1";
  
  $output = array();
  $return_value = null;
  $last_line = exec( $command , $output , $return_value );

  die( "OK" );  

}


$assigns['progress'] = 5;
