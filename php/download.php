<?php
  
  require_once( "inc/required_cwd.php" );

  $assigns['progress'] = 6;

  $ninja = $cwd . "/ninja.json";
  $ninja = file_get_contents( $ninja );
  $ninja = json_decode( $ninja );

  $title = $ninja->meta->title;
  $title = aliasString( $title );
  
  if($title == "") {
    $title = date("d.m.Y");
  }

  $mtitle = aliasString( $title );

  $file1      = $cwd . "/".$mtitle."-1280x720.mp4";
  $file2      = $cwd . "/".$mtitle."-720x720.mp4";
  $file3      = $cwd . "/".$mtitle."-1280x720_bumper.mp4";
  $file4      = $cwd . "/".$mtitle."-720x720_bumper.mp4";
  $ninja_file = $cwd . "/ninja.json";

  $assigns['file1'] = $file1;
  $assigns['file2'] = $file2;
  $assigns['file3'] = $file3;
  $assigns['file4'] = $file4;

  $assigns['file1_size'] = filesize( $file1 );
  $assigns['file2_size'] = filesize( $file2 );
  $assigns['file3_size'] = filesize( $file3 );
  $assigns['file4_size'] = filesize( $file4 );


  $command = "chmod -R 666 ".$cwd."/*";
  $output = array();
  $return_value = null;
  $last_line = exec( $command , $output , $return_value );

  $files_to_zip = array(
    $file1,
    $file2,
    $file3,
    $file4,
    $ninja_file
  );

  $assigns['zip'] = createZip($files_to_zip, $cwd . '/' . $mtitle . '.zip', TRUE);

  //$command = "rm ".$cwd."/simage*";
  //$output = array();
  //$return_value = null;
  //$last_line = exec( $command , $output , $return_value );
