<?php

require_once( "inc/required_cwd.php" );  

$date = date('d-m-Y');

$file = $cwd . "/ninja.json";

header('Content-Type: application/octet-stream');
header("Content-Transfer-Encoding: Binary"); 
header('Content-disposition: attachment; filename="'.$date.'.json"');
readfile($file);

exit( 0 );
