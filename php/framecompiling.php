<?php

  function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
   return $rgb; // returns an array with the rgb values
  }

  $fonts_dir           = "assets/fonts/";
  $fonts               = array();
  $fonts['FuturaLT']     = $fonts_dir . "FuturaLT-Light.ttf";
  $fonts['FuturaLT-ExtraBold'] = $fonts_dir . "FuturaLT-ExtraBold.ttf";
  $fonts['Halflings']  = $fonts_dir . "glyphicons-halflings-regular.ttf";


  $colors          = array();
  $colors['white'] = array( 255 , 255 , 255 );
  $colors['black'] = array( 0 , 0 , 0 );
  $colros['grey']  = array( 127 , 127 , 127 );
  $colors['red']   = array( 255 , 0 , 0 );
  $colors['green'] = array( 0 , 255 , 0 );
  $colors['blue']  = array( 0 , 0 , 255 );

  $assigns['progress'] = 4;


  require_once( "inc/required_cwd.php" );  

  $frametextsinfo = $cwd . "/textframes.json";
  $frametextsinfo = file_get_contents( $frametextsinfo );
  $frametextsinfo = json_decode( $frametextsinfo );

  // decode the individual json of the frames.
  foreach( $frametextsinfo as $k => $v ) {
    $frametextsinfo[$k] = json_decode( $v );
  }

  $frameoverlaysinfo = $cwd . "/overlayframes.json";
  $frameoverlaysinfo = file_get_contents( $frameoverlaysinfo );
  $frameoverlaysinfo = json_decode( $frameoverlaysinfo );

  // decode the individual json of the frames.
  foreach( $frameoverlaysinfo as $k => $v ) {
    $frameoverlaysinfo[$k] = json_decode( $v );
  }


  $frameimagesinfo = $cwd . "/imageframes.json";
  $frameimagesinfo = file_get_contents( $frameimagesinfo );
  $frameimagesinfo = json_decode( $frameimagesinfo );

  // decode the individual json of the frames.
  foreach( $frameimagesinfo as $k => $v ) {
    $frameimagesinfo[$k] = json_decode( $v );
  }

  $total_frames = count( $frametextsinfo );

  // Get the total frames.
  $assigns['total_frames'] = $total_frames;

  if( isset( $_GET['frame'] ) ) {

    $frame = intval( $_GET['frame'] );
    $frameseven = numberPad( $frame , 7 );
    $simage_file = $cwd . "/simage-" . $frameseven .".png";


    
    if( $frame >= 0 && $frame < $total_frames ) {


        $im = imagecreatefrompng( "assets/img/black.png" );

        $images = $frameimagesinfo[$frame]->children;
        $texts = $frametextsinfo[$frame]->children;
        $overlays = $frameoverlaysinfo[$frame]->children;


        // Draw the images.
        foreach( $images as $image ) {
          
          $image = $image->attrs;

          $s = $image->src;
          $ext = strtolower( pathinfo( $cwd . "/" . $s, PATHINFO_EXTENSION) );

          $imageinfo = getimagesize( $cwd . "/" . $s );

          if( $ext == "png" ) {
            $si = imagecreatefrompng( $cwd . "/" . $s );
          } elseif( $ext == "jpg" ) {
            $si = imagecreatefromjpeg( $cwd . "/" . $s );
          } else {
            die( "NOK - Image extension is not recognized: " . $s );
          }

          // now we have a si.
          // we need to make it transparent.

          if( ! isset( $image->x ) ) {
            $image->x = 0;
          }

          if( ! isset( $image->y ) ) {
            $image->y = 0;
          }

          if( ! isset( $image->width ) ) {
            $image->width = $imageinfo[0];
          }

          if( ! isset( $image->height ) ) {
            $image->height = $imageinfo[1];
          }

          if( ! isset( $image->opacity ) ) {
            $image->opacity = 1;
          }

          $pct = round( $image->opacity * 100 );
          $dst_x = round( $image->x );
          $dst_y = round( $image->y );
          $src_x = 0;
          $src_y = 0;
          $dst_w = $image->width;
          $dst_h = $image->height;
          $src_w = $imageinfo[0];
          $src_h = $imageinfo[1];

          // create a new image for alpha blending.
          $tsi = imagecreatetruecolor (  $imageinfo[0] , $imageinfo[1] );
          imagealphablending( $tsi , true );
          imagecopymerge( $tsi , $si , 0 , 0 , 0 , 0 , $imageinfo[0] , $imageinfo[1] , $pct );

          // ok now put the tramparent image on
          imagecopyresampled( $im , $tsi , $dst_x , $dst_y , $src_x , $src_y , $dst_w , $dst_h , $src_w , $src_h );

          imagedestroy( $si );
          imagedestroy( $tsi );
          
        }

        // Do the overlay
        foreach( $overlays as $overlay ) {

          // extract the attributes
          $overlay = $overlay->attrs;

          $o = ( isset( $overlay->opacity ) ) ? intval( 127 - $overlay->opacity * 127 ) : 0;
          $rgb = hex2rgb( $overlay->fill );
          $c = imagecolorallocatealpha( $im, $rgb[0] , $rgb[1] , $rgb[2] , $o );
          imagefilledrectangle( $im, 0, 0, 1280, 720 , $c );

        }

        // Texts on top.
        foreach( $texts as $text ) {

          // extract the attributes
          $text = $text->attrs;

          // normalize and default.
          if( ! isset( $fonts[$text->fontFamily] ) ) {
            $text->fontFamily = "FuturaLT";
          }

          if( isset( $text->fontStyle ) && $text->fontStyle == "bold" ) {
            $text->fontFamily = "FuturaLT-ExtraBold";
          }

          $f = $fonts[$text->fontFamily];
          $o = ( isset( $text->opacity ) ) ? intval( 127 - $text->opacity * 127 ) : 0;
          
          $c = imagecolorallocatealpha( $im, $colors[$text->fill][0] , $colors[$text->fill][1] , $colors[$text->fill][2] , $o );
          $s = floatval( $text->fontSize * (3/4) );
          
          // This should never really happen but....
          if( ! isset( $text->x ) ) {
            $text->x = 0;
          }

          if( ! isset( $text->y ) ) {
            $text->y = 0;
          }

          if( ! isset( $text->offsetX ) ) {
            $text->offsetX = 0;
          }

          if( ! isset( $text->offsetY ) ) {
            $text->offsetY = 0;
          }

          // The text string
          $t = $text->text;


          $textinfo = imagettfbbox( $s , 0 , $f , $t );
          $width = $textinfo[2];
          $height = $textinfo[5];

          // acount for the top left corner instead of the bottom left corner.
          

          
          if( $text->center == "true" ) {
            
            $x = $text->x - ($width / 2);
            $y = $text->y - ($height / 2);

          } else {

            $x = $text->x - $text->offsetX;
            $y = $text->y - $text->offsetY - $height;

          }
          

          

          imagefttext($im, $s, 0 , $x, $y, $c, $f, $t );
          
        }



      imagepng( $im , $simage_file );
      imagedestroy( $im );
      die( "OK" );
    

    }

    die( "NOK - not a valid frame number " . $frame . " :: " . $frameseven );

  }
