<?php

$now = time();

function rrmdir($dir) { 
   if (is_dir($dir)) { 
     $objects = scandir($dir); 
     foreach ($objects as $object) { 
       if ($object != "." && $object != "..") { 
         if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object); 
       } 
     } 
     reset($objects); 
     rmdir($dir); 
   } 
 } 


$projects = glob( "uploads/*" );


foreach( $projects as $project ) {
  if( is_dir( $project ) ) {
    if( is_file( $project . "/timestamp.txt" ) ) {
      $t = file_get_contents( $project . "/timestamp.txt" );
      if( ( $now - $t ) > ( 1 * 60 * 60 ) ) {
        rrmdir( $project );
      }
    }
  }
}




if( count( $_FILES ) > 0 ) {

  if( ! isset( $_FILES['deaudio'] ) ) {
    die( "Please upload a file." );
  }

  $types = array();
  $types[] = "audio/mp3";
  $types[] = "audio/mpeg";
  $types[] = "audio/wav";

  if( ! in_array( $_FILES['deaudio']['type'] , $types ) ) {
    die( "Please only upload a .mp3 or .wav file.");
  }

  $uid = uniqid();

  $cwd = "uploads/" . $uid;
  $assigns['cwd'] = $cwd;

  $_SESSION['cwd'] = $cwd;

  $af = $cwd . "/audio.mp3";
  
  mkdir( $cwd );
  chmod( $cwd , 0777 );

  $tsf = $cwd . "/timestamp.txt";
 
  file_put_contents( $tsf , time() );
  chmod( $tsf , 0666 );
 
  $ninjajson = $cwd . "/ninja.json";
  file_put_contents( $ninjajson , file_get_contents( "assets/json/default.json" ) );

  if( is_uploaded_file( $_FILES['deaudio']['tmp_name'] ) ) {

    if( stripos( $_FILES['deaudio']['name'], ".wav" ) !== false ) {
      $af = $cwd . "/audio.wav";
    }

    move_uploaded_file( $_FILES['deaudio']['tmp_name'], $af );
    chmod( $af , 0666 );

    if( stripos( $_FILES['deaudio']['name'], ".wav" ) !== false ) {
      //ffmpeg -i noise.wav -acodec mp3 noise.mp3

      $taf = $cwd . "/audio.mp3";

      $command = $ffmpeg . " -i ".$af." -acodec mp3 -y ".$taf." 2>&1";
  
      $output = array();
      $return_value = null;
      $last_line = exec( $command , $output , $return_value );

      chmod( $taf , 0666 );

    }

    die( "OK" );
  }

}

if( isset( $_GET['step2'] ) && $_GET['step2'] == 1 && isset( $_SESSION['cwd'] ) && is_dir( $_SESSION['cwd'] ) ) {

  //ffmpeg -i input1.mp3 -i input2.mp3 -filter_complex amerge -c:a libmp3lame -q:a 4 output.mp3

  $cwd = $_SESSION['cwd'];
  $af = $cwd . "/audio.mp3";
  $taf = $cwd . "/taudio.mp3";


  $command = $ffmpeg . " -i ".$af." -i assets/bumper/silence.mp3 -filter_complex \"concat=n=2:v=0:a=1\" -y ".$taf." 2>&1";
  
  $output = array();
  $return_value = null;
  $last_line = exec( $command , $output , $return_value );

  unlink( $af );
  rename($taf, $af);

  //die( "OK" );

  $command = $ffmpeg . ' -i '.$af.' -t 14 -acodec copy -y '.$taf." 2>&1";
  
  $output = array();
  $return_value = null;
  $last_line = exec( $command , $output , $return_value );

  unlink( $af );
  rename($taf, $af);
  
  chmod( $af , 0666 );
  
  die( "OK" );

}


$assigns['progress'] = 0;
