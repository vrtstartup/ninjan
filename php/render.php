<?php

require_once( "inc/required_cwd.php" );  



/*
 .d888888  oo                      dP     dP                          dP dP                            
d8'    88                          88     88                          88 88                            
88aaaaa88a dP .d8888b. dP.  .dP    88aaaaa88a .d8888b. 88d888b. .d888b88 88 .d8888b. 88d888b. .d8888b. 
88     88  88 88'  `88  `8bd8'     88     88  88'  `88 88'  `88 88'  `88 88 88ooood8 88'  `88 Y8ooooo. 
88     88  88 88.  .88  .d88b.     88     88  88.  .88 88    88 88.  .88 88 88.  ... 88             88 
88     88  88 `88888P8 dP'  `dP    dP     dP  `88888P8 dP    dP `88888P8 dP `88888P' dP       `88888P' 
           88                                                                                          
           dP
*/
if( isset( $_POST['ajax'] ) && ! empty( $_POST['ajax'] ) ) {
  switch( $_POST['ajax'] ) {
    case 'save':
      //something
      save();
      break;
    case 'other':
      //something
      break;
  }
  die( "NOK" );
}


function save() {

  global $cwd;

  if( isset( $_POST['textframes'] ) && ! empty( $_POST['textframes'] ) ) {

    $file = $cwd . "/textframes.json";

    file_put_contents( $file , $_POST['textframes'] );
    chmod( $file , 0666 );

    
  } else {
  
    die( "NOK" );
  
  }


  if( isset( $_POST['overlayframes'] ) && ! empty( $_POST['overlayframes'] ) ) {

    $file = $cwd . "/overlayframes.json";

    file_put_contents( $file , $_POST['overlayframes'] );
    chmod( $file , 0666 );

    

  } else {
    
    die( "NOK" );

  }

  if( isset( $_POST['imageframes'] ) && ! empty( $_POST['imageframes'] ) ) {

    $file = $cwd . "/imageframes.json";

    file_put_contents( $file , $_POST['imageframes'] );
    chmod( $file , 0666 );

    

  } else {

    die( "NOK" );

  }

  die( "OK" );

}



$json_file = $cwd . "/ninja.json";
$json = file_get_contents( $json_file );


if( ! is_file( $json_file ) || strlen( $json ) == 0 ) {
  $json = file_get_contents( "assets/json/default.json" );
}


$assigns['ninja'] = $json;

$assigns['progress'] = 3;


  