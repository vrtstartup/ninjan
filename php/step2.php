<?php

require_once( "inc/required_cwd.php" );

  $vf = $cwd . "/uploaded-video.mov";
  $tvf = $cwd . "/tuploaded-video.mov";
  $bv = "assets/bumper/bumper.mov";
  $cf = $cwd . "/preview.mp4";


if( count( $_FILES ) > 0 ) {

  if( ! isset( $_FILES['devideo'] ) ) {
    die( "Please upload a file." );
  }
  
  if( ! $_FILES['devideo']['type'] == "video/quicktime" ) {
    die( "Please only upload a .mov file." );
  }

  if( is_uploaded_file( $_FILES['devideo']['tmp_name'] ) ) {
    
    move_uploaded_file( $_FILES['devideo']['tmp_name'], $vf );
    chmod( $vf , 0666 );

    die( "OK" );

  }

}

if( isset( $_GET['step2'] ) && $_GET['step2'] == 1 ) {

    $command = $ffmpeg . ' -i '.$vf.' -t 14 -vcodec copy -y '.$tvf." 2>&1";

    $output = array();
    $return_value = null;
    $last_line = exec( $command , $output , $return_value );

    unlink( $vf );
    rename($tvf, $vf);
    
    chmod( $vf , 0666 );
    
    die( "OK" );
}

if( isset( $_GET['step3'] ) && $_GET['step3'] == 1 ) {

    $command = $ffmpeg . " -i ".$vf." -i ".$bv." -filter_complex \"concat=n=2:v=1:a=0\" -r 25 -y -vcodec libx264 -pix_fmt yuv420p -an ".$cf. " 2>&1";

    $output = array();
    $return_value = null;
    $last_line = exec( $command , $output , $return_value );

    chmod( $cf , 0666 );

    die( "OK" );
}

if( isset( $_GET['step4'] ) && $_GET['step4'] == 1 ) {

    $command = $ffmpeg . " -i ".$cf." -y -f image2 ".$cwd."/image-%07d.png 2>&1";

    $output2 = array();
    $return_value = null;
    $last_line = exec( $command , $output2 , $return_value );

    $command = "chmod 666 ".$cwd."/*";
       
    $output3 = array();
    $return_value = null;
    $last_line = exec( $command , $output3 , $return_value );

    die( "OK" );
}


$assigns['progress'] = 1;
