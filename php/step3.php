<?php

require_once( "inc/required_cwd.php" );


if( count( $_FILES ) > 0 ) {

  if( ! isset( $_FILES['detext'] ) ) {
    die( "Please upload a file." );
  }

  if( ! $_FILES['detext'] == "application/json" ) {
    die( "Please only upload a .json file." );
  }

  $test = json_decode( file_get_contents( $_FILES['detext']['tmp_name'] ) );
  if( $test === NULL ) {
    die( "The file doesn't appear to be JSON formatted" );
  }

  $af = $cwd . "/ninja.json";
  if( is_uploaded_file( $_FILES['detext']['tmp_name'] ) ) {
    
    move_uploaded_file( $_FILES['detext']['tmp_name'], $af );
    chmod( $af , 0666 );
    
    die( "OK" );
    
  }
}


$assigns['progress'] = 1;
