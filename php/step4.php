<?php

require_once( "inc/required_cwd.php" );

$json_file = $cwd . "/" . "ninja.json";


function saveNinja() {
  global $json_file;
  file_put_contents( $json_file, $_POST['ninja'] );
  die( "OK" );
  chmod( $json_file, 0666 );
}


/*
 .d888888  oo                      dP     dP                          dP dP                            
d8'    88                          88     88                          88 88                            
88aaaaa88a dP .d8888b. dP.  .dP    88aaaaa88a .d8888b. 88d888b. .d888b88 88 .d8888b. 88d888b. .d8888b. 
88     88  88 88'  `88  `8bd8'     88     88  88'  `88 88'  `88 88'  `88 88 88ooood8 88'  `88 Y8ooooo. 
88     88  88 88.  .88  .d88b.     88     88  88.  .88 88    88 88.  .88 88 88.  ... 88             88 
88     88  88 `88888P8 dP'  `dP    dP     dP  `88888P8 dP    dP `88888P8 dP `88888P' dP       `88888P' 
           88                                                                                          
           dP
*/
if( isset( $_POST['ajax'] ) && ! empty( $_POST['ajax'] ) ) {
  switch( $_POST['ajax'] ) {
    case 'saveNinja':
      //something
      saveNinja();
      break;
    case 'other':
      //something
      break;
  }
  die( "NOK" );
}


$imagesmimes = array();
$imagesmimes[] = "image/jpeg";
$imagesmimes[] = "image/png";
$imagesmimes[] = "image/jpg";

if( count( $_FILES ) > 0 ) {

  if( isset( $_FILES['deimage'] ) ) {
    if( in_array( $_FILES['deimage']['type'], $imagesmimes) ) {

      if( is_uploaded_file( $_FILES['deimage']['tmp_name'] ) ) {

        move_uploaded_file( $_FILES['deimage']['tmp_name'], $cwd . "/" . $_FILES['deimage']['name'] );
        
        $info = getimagesize( $cwd . "/" . $_FILES['deimage']['name'] );
        $info[] = $_FILES['deimage']['name'];

        die( json_encode( $info ) );

      }
    }
  }

  die( "NOK" );
}


$json_file = $cwd . "/" . "ninja.json";
$effects = file_get_contents("assets/json/effects.json");

if( is_file( $json_file ) ) {
  $json = file_get_contents( $json_file );
} else {
  $json = file_get_contents( "assets/json/default.json" );
}

$assigns['ninja'] = $json;
$assigns['effects_json'] = $effects;
$assigns['effects'] = json_decode($effects);

$assigns['progress'] = 2;
