<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <meta name="description" content="{gT l="meta_description"}">
    <meta name="keywords" content="{gT l="meta_keywords"}">

    <meta name="twitter:card" content="{gT l="twitter_card"}">
    <meta name="twitter:site" content="{gT l="twitter_site"}">
    <meta name="twitter:title" content="{gT l="twitter_title"}">
    <meta name="twitter:description" content="{gT l="twitter_description"}">
    <meta name="twitter:url" content="{gT l="twitter_url"}">
    <!-- Facebook -->
    <meta property="og:title" content="{gT l="facebook_title"}">
    <meta property="og:description" content="{gT l="facebook_description"}">
    <meta property="og:url" content="{$URL}">
    <meta property="og:image" content="{$URL}assets/img/logo.png">

    <link rel="apple-touch-icon" href="assets/ico/favicon.ico">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">

    <link rel="stylesheet" href="assets/css/main.css" media="screen">
    <link rel="stylesheet" href="assets/css/print.css" media="print">

    <title>{$site_title}</title>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/kinetic.js"></script>
    <script src="assets/js/jquery.ui.widget.js"></script>
    <script src="assets/js/jquery.iframe-transport.js"></script>
    <script src="assets/js/jquery.fileupload.js"></script>
    <script src="assets/js/handlebars.js"></script>
    <script src="assets/js/helpers.js"></script>
    <script type="text/javascript" src="https://www.dropbox.com/static/api/2/dropins.js" id="dropboxjs" data-app-key="98q9yznyx5un1ic"></script>

    <script src="assets/js/main.js"></script>
    
    {if $smarty.get.page == "step4"}
    <script src="assets/js/step4.js"></script>
    {/if}

    {if isset( $smarty.session.cwd )}
    <script type="text/javascript">
            var CWD = "{$smarty.session.cwd}";
        </script>
    {/if}

    <style>

    @font-face {
        font-family: 'FuturaLT-ExtraBold';
        src: url('assets/fonts/FuturaLT-ExtraBold.ttf');
    }

    @font-face {
        font-family: 'FuturaLT';
        src: url('assets/fonts/FuturaLT-Light.ttf');
    }
    

    @font-face {
        font-family: 'Glyphs';
        src: url('assets/fonts/glyphicons-halflings-regular.ttf');
    }

    </style>

    <!-- Fonts -->
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->

  </head>

  <body id="{$smarty.get.page}">

    <div id="wrap">

      <header id="page-header">
        <h1><a href="?page=index"><img src="assets/img/logo.png" height="40px" alt="Logo Ninja Maker beta" /></a></h1>
      </header>

      <div class="container">
        


        {$content}

      </div><!-- /.container -->
    </div>

    <div style="font-family:'FuturaLT'">&nbsp;</div>
    <div style="font-family:'FuturaLT-ExtraBold'">&nbsp;</div>
    <div style="font-family:'Glyphs'">&nbsp;</div>

  </body>
</html>
