<?php

  $json_file = "proj/textcues.json";
  $json = file_get_contents( $json_file );

  $obj = json_decode( $json );

  $framesraw = glob( "proj/image*.png" );
  $total = count( $framesraw );


  ?>
  <html>
  <head>
    <title>Ninja Sample</title>
    <script type="text/javascript"  src="../assets/js/jquery.js"></script>
    <script type="text/javascript"  src="kinectic.js"></script>
  </head>
  <body>
  
    <div id="stage"></div>


    <script defer="defer">

    var rendered_frames = [];
    var stage = new Kinetic.Stage({
        container: 'stage',
        width: 720,
        height: 720
      });

    var videolayer = new Kinetic.Layer();
    var textlayer = new Kinetic.Layer();

    stage.add( videolayer );
    stage.add( textlayer );


    var fps          = 25;
    var factor       = 50;
    var timeout      = ( 1000 / fps ) * factor;

    var currentFrame = 0;
    var totalFrames  = <?php echo $total; ?>;
        
    var textcues = <?php echo $json; ?>;

    var framesraw = <?php echo json_encode( $framesraw ); ?>;
    var frames_loaded = 0;
    var frames = [];

    function render( ) {

      
     

      // Setup the image.
      var fr = new Kinetic.Image({

        x: 0,
        y: 0,
        image: frames[currentFrame],
        width: 720,
        height: 720

      });

      videolayer.clear();
      videolayer.add( fr );

      
      if (currentFrame >= totalFrames) {
        return false;
      }

      

      for( var k in textcues.cues ) {
        // Passed a cue fire text cue.
        if( parseInt( k ) == currentFrame && typeof textcues.cues[k].fired === "undefined" ) {
          for( var i = 0; i < textcues.cues[k].length; i++ ) {


            var actor = textcues.actors[textcues.cues[k][i]];

            // set the actor in the center of the screen if not defined.
            if( typeof actor.x === "undefined" ) {
              actor.x = 360;
            }

            if( typeof actor.y === "undefined" ) {
              actor.y = 360;
            }

            // set the intro and outro x an y to default.
            if( typeof actor.intro_x === "undefined" ) {
              actor.intro_x = actor.x;
            }
            if( typeof actor.intro_y === "undefined" ) {
              actor.intro_y = actor.y;
            }
            if( typeof actor.outro_x === "undefined" ) {
              actor.outro_x = actor.x;
            }
            if( typeof actor.outro_y === "undefined" ) {
              actor.outro_y = actor.y;
            }

            // set some defalut intro things
            if( typeof actor.intro_duration === "undefined" ) {
              actor.intro_duration = 0.5;
            }
            if( typeof actor.intro_easing === "undefined" ) {
              actor.intro_easing = "Linear";
            }
            if( typeof actor.intro_opacity === "undefined" ) {
              actor.intro_opacity = 1;
            }


            // set some default outro default things.
            if( typeof actor.outro_duration === "undefined" ) {
              actor.outro_duration = 0.5;
            }
            if( typeof actor.outro_easing === "undefined" ) {
              actor.outro_easing = "Linear";
            }
            if( typeof actor.outro_opacity === "undefined" ) {
              actor.outro_opacity = 0;
            }

            // set some default actor things.
            if( typeof actor.duration === "undefined" ) {
              actor.duration = 1.0; // show for one second.
            }
            if( typeof actor.opacity === "undefined" ) {
              actor.opacity = 1;
            }

            var text = new Kinetic.Text( actor );
            textlayer.add( text );

            var intro_tween = {};

            intro_tween.node = text;
            intro_tween.duration = actor.intro_duration * factor;
            intro_tween.easing = Kinetic.Easings[actor.intro_easing];
            intro_tween.x = actor.intro_x;
            intro_tween.y = actor.intro_y;
            intro_tween.opacity = actor.intro_opacity;

            // after intro tween call main tween.
            intro_tween.onFinish = function() {

              var main_tween = {};
              main_tween.node = text;
              main_tween.duration = actor.duration * factor;

              // after the maintween now do the outro
              main_tween.onFinish = function() {

                var outro_tween = {};

                outro_tween.node = text;
                outro_tween.duration = actor.outro_duration * factor;
                outro_tween.easing = Kinetic.Easings[actor.outro_easing]
                outro_tween.x = actor.outro_x;
                outro_tween.y = actor.outro_y;
                outro_tween.opacity = actor.outro_opacity;

                outro_tween = new Kinetic.Tween( outro_tween );
                outro_tween.play();

              }
              
              main_tween = new Kinetic.Tween( main_tween );
              main_tween.play();
            
            }

            intro_tween = new Kinetic.Tween( intro_tween );
            intro_tween.play();
          }

          textcues.cues[k].fired = false; 
        }
      
      }

      
    
      videolayer.draw();
      textlayer.draw();

     
      


      setTimeout( render , timeout );

      stage.toDataURL({
        callback: function(dataUrl) {

          
          data = {};
          data.data = dataUrl;
          data.frame = currentFrame;
          $.post( "saveImage.php" , data , function( response ){
            if( response == "OK" ) {
              currentFrame++;
            }
            else
            {
              alert( "Problem with the frame!" );
              currentFrame = 9999999;
            }
          });
          

        }
      });

    } 

   



    // Load video frames and go.
    var i = 0;
    while( frames.length < totalFrames ) {

      frames[i] = new Image();
      frames[i].src = framesraw[i];

      frames[i].onload = function() {
        frames_loaded++;

        if( frames_loaded >= totalFrames ) {
        
          render();
          
        }
      }

      i++;

    }


    </script>


  </body>
  </html>